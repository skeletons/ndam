(* TODO: This file has no documentation attached and it is very difficult to understand how it works.
 * This file (as any other files in the project) definitely needs a documented mli file.
 * Question: Should it be furthermore merged with the Printer module? *)

type 'a cat_list = Base of 'a list | Cat of 'a cat_list list
type printitem = Str of string | Newline | IndentChange of int
let str x = Str x
let nl = Newline
let stdindent = 2
let indent = IndentChange stdindent
let dedent = IndentChange (-stdindent)
let empty = Base []

let flatten_cat_list l =
	let rec aux l acc =
		match l with
		| Base x -> x @ acc
		| Cat l -> List.fold_right aux l acc
	in
	aux l []

let rec pp_print_items ff indent l =
	match l with
	| [] -> ()
	| Str x :: l -> Format.fprintf ff "%s" x; pp_print_items ff indent l
	| Newline :: l -> Format.fprintf ff "@,%s" (String.make indent ' '); pp_print_items ff indent l
	| IndentChange c :: l -> pp_print_items ff (indent + c) l

let pp_print_cl ff l =
	Format.fprintf ff "@[<v>"; pp_print_items ff 0 (flatten_cat_list l); Format.fprintf ff "@]"

let make_list ?(first = Base []) ?(last = Base []) ?(sep = Base []) ?(zero = Cat [first; last]) l =
	let rec aux l =
		match l with
		| [] -> [last]
		| x :: rest -> sep :: x :: aux rest
	in
	match l with
	| [] -> zero
	| x :: rest -> Cat (first :: x :: aux rest)

let cs x = Base [str x]
let nlc = Base [nl]

let make_list_cs ?first ?last ?sep ?zero l =
	make_list ?first:(optmap cs first) ?last:(optmap cs last) ?sep:(optmap cs sep) ?zero:(optmap cs zero) l
