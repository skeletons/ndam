
module SMap = Map.Make(String)
module SSet = Set.Make (String)

let rec sset_list_union = function
	| [] -> SSet.empty
	| x :: q -> SSet.union x (sset_list_union q)

let rec list_compare compare l1 l2 =
	match l1, l2 with
	| [], [] -> 0
	| [], _::_ -> -1
	| _::_, [] -> 1
	| a1 :: q1, a2 :: q2 ->
			let r = compare a1 a2 in
			if r <> 0 then r else
				list_compare compare q1 q2

let id x = x

let comp f g x = g (f x)

(* Give the product of lists *)
let rec product: 'a list list -> 'a list list = function
	| [] -> [[]]
	| x :: q -> let q' = product q in
		List.flatten (List.map (fun a -> List.map (fun l -> a :: l) q') x)

let smap_domain m = List.map fst (SMap.bindings m)

let list_iter3 f la lb lc =
	let rec aux = function
	| [], [], [] -> ()
	| a :: la, b :: lb, c :: lc -> f a b c ; aux (la, lb, lc)
	| _, _, _ -> failwith "list_iter3: different list sizes." in
	aux (la, lb, lc)

let list_map3 f la lb lc =
	let rec aux = function
	| [], [], [] -> []
	| a :: la, b :: lb, c :: lc -> f a b c :: aux (la, lb, lc)
	| _, _, _ -> failwith "list_map3: different list sizes." in
	aux (la, lb, lc)

let list_square l =
	let rec aux stack = function
		| [] -> List.concat stack
		| b :: lb ->
			aux (List.map (fun a -> (a, b)) l :: stack) lb
	in aux []

let if_option = function
	| None -> fun _ -> None
	| Some x -> fun f -> f x

let list_map_option f l =
	Option.map List.rev (List.fold_left (fun r e ->
		if_option r (fun l -> if_option (f e) (fun v -> Some (v :: l)))) (Some []) l)

let fresh ok n =
	let rec aux i =
		let n = n ^ "_" ^ string_of_int i in
		if ok n then n else aux (1 + i) in
	if ok n then n else aux 0

let uniq l =
	let rec aux res = function
		| [] -> List.rev res
		| a :: l ->
			aux (if List.mem a res then res else a :: res) l in
	aux [] l

let rec find_duplicate l =
	let l = List.sort compare l in
	let rec find_dup_aux =
		begin function
		| [] -> None
		| a :: [] -> None
		| a :: b :: l when a = b -> Some a
		| a :: l -> find_dup_aux l
		end
	in
	find_dup_aux l

let rec find_overlap l1 = function
	| [] -> None
	| a :: _ when List.mem a l1 -> Some a
	| _ :: q -> find_overlap l1 q

let rec find_index_opt a = function
	| [] -> None
	| b :: l ->
		if a = b then Some 0 else Option.map succ (find_index_opt a l)

let write_nth i e l =
	if List.length l <= i then
		None
	else
		Some (List.mapi (fun j v -> if i = j then e else v) l)

let seq i =
	List.init i id

(* Printing a list using Format *)
let rec print_list ?(sep="") ?(empty=fun ff -> ()) print ff =
	begin function
	| [] -> empty ff
	| l -> Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "%s" sep) print ff l
	end
