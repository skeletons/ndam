type 'a with_loc = 'a Pskeltypes.with_loc = {
	contents : 'a ;
	loc : Lexing.position * Lexing.position ;
}

type typedesc =
	(* (t1, t2) map *)
	| MapType of typedesc * typedesc
	(* t set *)
	| SetType of typedesc
	(* t list *)
	| ListType of typedesc
	(* type alias or base type *)
	| BaseType of string
	(* t1 * t2 * ... * tn *)
	| TupleType of typedesc list
	(* t program_point or program_point for TopType program_point *)
	| Program_point of typedesc
	(* only used for program points *)
	| TopType
	(* for empty lists, sets, etc *)
	| BottomType

(* Quark : inputs, output ; output may be a tuple *)
type quark_decl = {
	q_name : string ;
	q_inputs : typedesc list ;
	q_output : typedesc ;
}

(* Functions arguments and body *)
type 'a lambda = string list * 'a
(* Function annotated with location information for the arguments *)
type 'a lambda_loc = string list with_loc * 'a

(* Values (and lvalues) for constraints, with location information *)
type pvalue_desc =
	(* V *)
	| PNamedVar of string with_loc
	(* v1[v2] *)
	| PMapGet of pvalue * pvalue
	(* f(v1, ..., vn) *)
	| PFunc of string with_loc * pvalue list
	(* let x = v1 in v2 *)
	| PVLet of pvalue * pvalue lambda_loc
	(* c /\ v *)
	| PVWith of pconstr * pvalue
	(* (v1, ..., vn) *)
	| PVTuple of pvalue list
	(* {v1; ...; vn} *)
	| PVSet of pvalue list
	(* [v1; ...; vn] *)
	| PVList of pvalue list
	(* join args in v1, v2 *)
	| PVJoin of pvalue * pvalue lambda_loc
	(* () *)
	| PVUnit
and pvalue = pvalue_desc with_loc

(* Constraints, with location information *)
and pconstr_desc =
	(* True *)
	| PTrue
	(* c1 /\ c2 *)
	| PConj of pconstr * pconstr
	(* let x = v in c *)
	| PCLet of pvalue * pconstr lambda_loc
	(* v1 <= v2 *)
	| PSub of pvalue * pvalue
	(* forall args in v, c *)
	| PForall of pvalue * pconstr lambda_loc
and pconstr = pconstr_desc with_loc

(* Filter constraint definition *)
type pfilter_constr_decl = {
	pfc_name : string ;
	pfc_value : pvalue lambda_loc ;
}

(* Constraint definition for hook inputs *)
type phookin_constr_decl = {
	phi_name : string ;
	phi_annot : string ;
	phi_value : pvalue lambda_loc ;
}

(* Constraint definition for hook outputs *)
type phookout_constr_decl = {
	pho_name : string ;
	pho_annot : string ;
	pho_value : pvalue lambda_loc ;
}

(* Constraint definition for the beginning of a rule *)
type prulein_constr_decl = {
	pri_name : string ;
	pri_value : pvalue lambda_loc ;
}

(* Constraint definition for the end of a rule *)
type pruleout_constr_decl = {
	pro_name : string ;
	pro_value : pvalue lambda_loc ;
}

(* Declaration of a variable to solve for *)
type var_constr_decl = {
	vc_name : string with_loc ;
	vc_type : typedesc with_loc ;
}

(* All declarations in a constraint file *)
type pcdecls = {
	(* Type declarations *)
	pc_typedecls : (string with_loc * (string with_loc * bool * bool) list) list;
	(* Type aliases *)
	pc_types : (string with_loc * typedesc with_loc) list ;
	pc_vars : var_constr_decl list ;
	(* Input and output types for each hook type *)
	pc_rulein_vars : (string with_loc * typedesc with_loc) list ;
	pc_ruleout_vars : (string with_loc * typedesc with_loc) list ;
	pc_quarks : quark_decl list ;
	pc_filters : pfilter_constr_decl list ;
	pc_hookins : phookin_constr_decl list ;
	pc_hookouts : phookout_constr_decl list ;
	pc_ruleins : prulein_constr_decl list ;
	pc_ruleouts : pruleout_constr_decl list ;
}
