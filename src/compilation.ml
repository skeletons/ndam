open Util
open Skeltypes
open Interpretation
open Skeleton

(*****************************************************************************)
(*                                 simplelet                                 *)
(*****************************************************************************)

(* An interpretation for simplelet. Given a term/skeleton, the resulting
 * functions return the map of constructors that have been destructed to
 * their type and the new term/skeleton *)
let simplelet_interpretation destr :
	(necro_type SMap.t * term, necro_type SMap.t * skeleton) interpretation =
		let merge_maps =
			List.fold_left (SMap.union (fun _ _ x -> Some x)) SMap.empty
		in
		let destr' (c, base_type) p =
			{tv_name = destr c; tv_typ = Arrow (base_type, pattern_typ p) }
		in
		let letin map a p s1 s2 =
			let av = all_variables_skel s2 in
			let rec is_tuple_type p =
				begin match p with
				| PWild _ | PVar _ -> true
				| PConstr _ -> false
				| PTuple p -> List.for_all is_tuple_type p
				end
			in
			begin match p with
			| _ when is_tuple_type p -> map, LetIn (a, p, s1, s2)
			| PConstr (c, ta, p') ->
					let x = fresh (fun x -> not (av x)) "x" in
					let tv_x = {tv_name=x; tv_typ=snd c} in
					SMap.add (fst c) (snd c) map,
					LetIn (a, PVar tv_x, s1,
						LetIn (None, p', Apply (destr' c p', ta, [TVar (tv_x, [])]), s2)
					)
			| PTuple ps ->
					let _, xs = List.fold_left (fun (f, l) _ ->
						let new_x = fresh (fun x -> not (f x)) "x" in
						((fun x -> f x || x = new_x),new_x :: l)) (av, []) ps
					in
					let almost = List.fold_left2 (fun s xn pn ->
						LetIn (None, pn, Return (TVar ({tv_name=xn;tv_typ=pattern_typ pn}, [])) ,s)) s2 xs ps
					in
					let xs' = List.map2 (fun x p -> PVar {tv_typ=pattern_typ p;tv_name=x}) xs ps in
					map, LetIn (a, PTuple xs', s1, almost)
			| _ -> assert false (* Can't happen *)
			end
		in
	{
		var_interp = (fun v ta ->
			SMap.empty, TVar (v, ta)) ;
		constr_interp = (fun c ta (m, t) ->
			m, TConstr (c, ta, t)) ;
		tuple_interp = (fun ct ->
			let c, t = List.split ct in merge_maps c, TTuple t) ;
		function_interp = (fun x ty (c, s) ->
			c, TFunc (x, ty, s)) ;
		letin_interp = (fun a t (c1, s1) (c2, s2) ->
			let c = SMap.union (fun _ _ x -> Some x) c1 c2 in
			letin c a t s1 s2);
		apply_interp = (fun f ta ct ->
			let c, t = List.split ct in merge_maps c,
			Apply (f, ta, t)) ;
		merge_interp = (fun s cb loc ->
			let c, b = List.split cb in
			merge_maps c, Branching (s, b, loc)) ;
		return_interp = (fun (c, t) ->
			c, Return t)
	}

let simplelet ss =
	let destr c =
		let used h = SMap.mem h ss.ss_terms in
		let std_name = "_destr_" ^ c in
		let ok h = not (used h) in
		fresh ok std_name
	in
	let simplelet_aux ss =
		let open Interpretation in
		let simplelet_term t =
			interpret_term (simplelet_interpretation destr) t
		in
		let simplelet_term_decl (ta, ty, term) =
			begin match term with
			| None -> (SMap.empty, (ta, ty, None))
			| Some t -> let (c', t') = simplelet_term t in (c', (ta, ty, Some t'))
			end
		in
		let simplelet_ss_terms s =
			let union m1 m2 = SMap.union (fun _ _ y -> Some y) m1 m2 in
			SMap.fold (fun s term (constr, map) ->
				let constr', term' = simplelet_term_decl term in
				(union constr constr', SMap.add s term' map))
			s (SMap.empty, SMap.empty)
		in
		let constr, ss_terms = simplelet_ss_terms ss.ss_terms in
		let destructor c s =
			let s = begin match s with | Base (s, spec, _) -> let () = assert spec in s | _ -> assert false end in
			let (s, cl) = SMap.find s ss.ss_types in
			let csig = List.find (fun cs -> cs.cs_name = c) (Option.get cl) in
			(csig.cs_type_args, Arrow (csig.cs_output_type, csig.cs_input_type), None)
		in
		let add_destructors ss_terms =
			SMap.fold (fun c s m -> SMap.add (destr c) (destructor c s) m)
			constr ss_terms
		in
		{ ss with ss_terms = add_destructors ss_terms }
	in
	let e = ref ss in
	let flag = ref false in
	let () =
		while not !flag do
			let new_e = simplelet_aux !e in
			let () = flag := same_ss new_e !e in
			let () = e := new_e in
			()
		done
	in
	!e



(*****************************************************************************)
(*                               delete monads                               *)
(*****************************************************************************)

let delete_monads_interp tmp =
	{
		var_interp = (fun v ta -> TVar (v, ta)) ;
		constr_interp = (fun c ta t -> TConstr (c, ta, t)) ;
		tuple_interp = (fun t -> TTuple t) ;
		function_interp = (fun x ty s -> TFunc (x, ty, s)) ;
		letin_interp = (fun a p s1 s2 ->
			begin match a with
			| None -> LetIn (a, p, s1, s2)
			| Some (b, ta) ->
					(* let tmp1 = s1 in
					 * b<ta> tmp1 (λ tmp2 -> let p = tmp2 in s2) *)
					let a = pattern_typ p in
					let ma = skeleton_type s1 in
					let mb = skeleton_type s2 in
					let tmp2_ty = Arrow(Arrow(a, mb), mb) in
					let b_ty = Arrow(ma, tmp2_ty) in
					let tmp1 = {tv_name=tmp;tv_typ=ma} in
					let tmp2 = {tv_name=tmp;tv_typ=pattern_typ p} in
					LetIn (None, PVar tmp1, s1,
						Apply({tv_name=b;tv_typ=b_ty}, ta,
							TVar (tmp1,[]) ::
							TFunc (tmp2.tv_name, tmp2.tv_typ,
								LetIn(None, p, Return (TVar (tmp2, [])), s2)) ::
							[]
						)
					)
			end);
		apply_interp = (fun f ta t -> Apply (f, ta, t)) ;
		merge_interp = (fun s b loc -> Branching (s, b, loc)) ;
		return_interp = (fun t -> Return t)
	}

let delete_monads ss =
	let ok x = not (all_variables_ss ss x) && not (Lexer.is_keyword x) in
	let tmp = fresh ok "_tmp" in
	let ss_terms =
		SMap.mapi (fun name (ta, ty, t) ->
			begin match t with
			| None -> (ta, ty, None)
			| Some t -> (ta, ty, Some (interpret_term (delete_monads_interp tmp) t))
			end) ss.ss_terms
	in
	{ss with ss_terms}



(*****************************************************************************)
(*                                  explode                                  *)
(*****************************************************************************)

let exploding_interpretation =
	{
		var_interp = (fun v ta -> TVar (v, ta)) ;
		constr_interp = (fun c ta t -> TConstr (c, ta, t)) ;
		tuple_interp = (fun t -> TTuple t) ;
		function_interp = (fun x ty sl ->
			begin match sl with
			| s :: [] -> TFunc (x, ty, s)
			| s :: _ ->
					TFunc (x, ty, Branching (skeleton_type s, sl, None))
			| [] -> assert false (* No empty branching *)
			end) ;
		letin_interp = (fun a p sl1 sl2 ->
			List.map (fun (s1, s2) -> LetIn(a, p, s1, s2)) (list_square sl1 sl2)) ;
		apply_interp = (fun f ta t -> Apply (f, ta, t) :: []) ;
		merge_interp = (fun ty l loc ->
			let l = List.flatten l in
			(** Flattening all branchings **)
			let l =
				let rec flatten acc =
					begin function
					| [] -> acc
					| s :: l ->
							begin match s with
							| Branching (_, l', loc) ->
									flatten acc (l' @ l)
							| s -> flatten (s :: acc) l
							end
					end
				in
				flatten [] l
			in
			List.sort_uniq compare l );
		return_interp = (fun t -> Return t :: [])
	}


let explode ss =
	let explode_term = interpret_term exploding_interpretation in
	let ss_terms = SMap.map (fun (ta, ty, t) ->
		(ta, ty, Option.map explode_term t)) ss.ss_terms
	in
	{ss with ss_terms}



(*****************************************************************************)
(*                     rename redefinition of variables                      *)
(*****************************************************************************)

let remove_redef_interp =
	let rec remove_redef_pattern p map t_all t_now =
		begin match p with
		| PWild _ -> (p, map, t_all, t_now)
		| PVar tv ->
				begin match SSet.mem tv.tv_name t_now with
				| false ->
						(p, map, SSet.add tv.tv_name t_all, SSet.add tv.tv_name t_now)
				| true ->
						let ok x = not (SSet.mem x t_all) in
						let new_name = fresh ok tv.tv_name in
						(PVar {tv with tv_name = new_name},
							SMap.add tv.tv_name new_name map,
							SSet.add new_name t_all,
							SSet.add new_name t_now)
				end
		| PConstr (c, ta, p) ->
				let (p, map, t_all, t_now) = remove_redef_pattern p map t_all t_now in
				(PConstr (c, ta, p), map, t_all, t_now)
		| PTuple [] ->
				(p, map, t_all, t_now)
		| PTuple (p :: pq) ->
				let (p, map, t_all, t_now) = remove_redef_pattern p map t_all t_now in
				let (pq, map, t_all, t_now) = remove_redef_pattern (PTuple pq) map t_all t_now in
				begin match pq with
				| PTuple pq -> (PTuple (p :: pq), map, t_all, t_now)
				| _ -> assert false
				end
		end
	in
	{
		var_interp = (fun v ta map taken_all taken_now ->
			begin match SMap.find_opt v.tv_name map with
			| Some v' -> TVar ({v with tv_name = v'}, ta)
			| None -> TVar (v, ta)
			end) ;
		constr_interp = (fun c ta t map taken_all taken_now ->
			TConstr (c, ta, t map taken_all taken_now)) ;
		tuple_interp = (fun tl map taken_all taken_now ->
			TTuple (List.map (fun t -> t map taken_all taken_now) tl) );
		function_interp = (fun x ty sk map taken_all taken_now ->
			let map = SMap.remove x map in
			let taken_all = SSet.add x taken_all in
			let taken_now = SSet.add x taken_now in
			TFunc (x, ty, sk map taken_all taken_now) ) ;
		letin_interp = (fun bind p s1 s2 map taken_all taken_now ->
			let (p', map', taken_all', taken_now') =
				remove_redef_pattern p map taken_all taken_now
			in
			LetIn(bind, p', s1 map taken_all taken_now, s2 map' taken_all' taken_now')) ;
		apply_interp = (fun f ta tl map taken_all taken_now ->
			let f = 
				begin match SMap.find_opt f.tv_name map with
				| Some f' -> {f with tv_name = f'}
				| None -> f
				end
			in
			Apply (f, ta, List.map (fun t -> t map taken_all taken_now) tl)) ;
		merge_interp = (fun ty sl loc map taken_all taken_now ->
			Branching (ty, List.map (fun t -> t map taken_all taken_now) sl, loc)) ;
		return_interp = (fun t map taken_all taken_now ->
			Return (t map taken_all taken_now))
	}

let remove_redef ss =
	let remove_redef_in_term = interpret_term remove_redef_interp in
	let ss_terms = SMap.map (fun (ta, ty, t) ->
		begin match t with
		| None -> (ta, ty, None)
		| Some t ->
				let map = SMap.empty in
				let taken_all =
					SSet.union (all_variables_set_term t)
					(SMap.fold (fun k _ s -> SSet.add k s) ss.ss_terms SSet.empty)
				in
				let taken_now = SSet.empty in
				(ta, ty, Some (remove_redef_in_term t map taken_all taken_now))
		end) ss.ss_terms
	in
	{ ss with ss_terms }



(*****************************************************************************)
(*                            extract inner letin                            *)
(*****************************************************************************)

let extract_letin_interp =
	{
		var_interp = (fun v ta -> TVar (v, ta)) ;
		constr_interp = (fun c ta t -> TConstr (c, ta, t)) ;
		tuple_interp = (fun t -> TTuple t) ;
		function_interp = (fun x ty s -> TFunc (x, ty, s)) ;
		letin_interp = (fun a p s1 s2 ->
			begin match s1 with
			| LetIn (a', p', s1', s2')->
					(* let %a p =
					 * 	let%a' p' = s1' in s2'
					 * in s2 *)
					begin match a' with
					| None ->
							LetIn (None, p', s1', LetIn(a, p, s2', s2))
					| Some _ ->
							LetIn (a, p, s1, s2) (* cannot extract an inner monadic letin *)
					end
			| _ -> LetIn (a, p, s1, s2)
			end) ;
		apply_interp = (fun f ta t -> Apply (f, ta, t)) ;
		merge_interp = (fun ty l loc -> Branching (ty, l, loc)) ;
		return_interp = (fun t -> Return t)
	}

let extract_letin ss =
	let ss = remove_redef ss in
	let extract_in_term = interpret_term extract_letin_interp in
	let ss_terms = SMap.map (fun (ta, ty, t) ->
		(ta, ty, Option.map extract_in_term t)) ss.ss_terms
	in
	{ss with ss_terms}

