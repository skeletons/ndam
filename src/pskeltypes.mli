(** Representation of skeletons before typing, still contains location information **)

type location = Lexing.position * Lexing.position

(** Location information, for error reporting **)
type 'a with_loc = {
	contents : 'a ;
	loc : location ;
}

type var = Skeltypes.var

type poly_ptype =
	| PType of string * poly_ptype with_loc list
	| Prod of poly_ptype with_loc list
	| PArrow of poly_ptype with_loc * poly_ptype with_loc

type monadic_annotation = (string with_loc * poly_ptype with_loc list) option

type type_arg = Skeltypes.type_arg

type ppattern =
	| PPWild
	| PPVar of var
	| PPConstr of string with_loc * ppattern with_loc
	| PPTuple of ppattern with_loc list

and pterm =
	| PTVar of var * poly_ptype with_loc list
	| PTConstr of string with_loc * poly_ptype with_loc list * pterm with_loc
	| PTType of poly_ptype with_loc * pterm with_loc
	| PTTuple of pterm with_loc list
	| PTFunc of string * poly_ptype with_loc * pskeleton with_loc

and pskeleton =
	| PBranching of pskeleton with_loc list
	| PReturn of pterm with_loc
	| PLetIn of monadic_annotation * ppattern with_loc * pskeleton with_loc * pskeleton with_loc
	| PApply of var with_loc * poly_ptype with_loc list * pterm with_loc list


type pdecl =
	| PAbstractType of string * int
	| PVariantType of string * type_arg list *
		(string with_loc * poly_ptype with_loc) list
	| PTerm of string * type_arg list * poly_ptype with_loc * pterm with_loc option
	| PAlias of string * type_arg list * poly_ptype with_loc

type pdecls = {
		pabs_type : (string * int) list ;
		pvar_type : (string * type_arg list * (string with_loc * poly_ptype with_loc) list) list ;
		pterms : (string * type_arg list * poly_ptype with_loc * pterm with_loc option) list ;
		palias : (string * type_arg list * poly_ptype with_loc) list
	}

