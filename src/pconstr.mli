(* TODO: This file has been automatically generated and needs clean-up and documentation. *)

type 'a with_loc =
  'a Pskeltypes.with_loc = {
  contents : 'a;
  loc : Lexing.position * Lexing.position;
}
type typedesc =
    MapType of typedesc * typedesc
  | SetType of typedesc
  | ListType of typedesc
  | BaseType of string
  | TupleType of typedesc list
  | Program_point of typedesc
  | TopType
  | BottomType
type quark_decl = {
  q_name : string;
  q_inputs : typedesc list;
  q_output : typedesc;
}
type 'a lambda = string list * 'a
type 'a lambda_loc = string list with_loc * 'a
type pvalue_desc =
    PNamedVar of string with_loc
  | PMapGet of pvalue * pvalue
  | PFunc of string with_loc * pvalue list
  | PVLet of pvalue * pvalue lambda_loc
  | PVWith of pconstr * pvalue
  | PVTuple of pvalue list
  | PVSet of pvalue list
  | PVList of pvalue list
  | PVJoin of pvalue * pvalue lambda_loc
  | PVUnit
and pvalue = pvalue_desc with_loc
and pconstr_desc =
    PTrue
  | PConj of pconstr * pconstr
  | PCLet of pvalue * pconstr lambda_loc
  | PSub of pvalue * pvalue
  | PForall of pvalue * pconstr lambda_loc
and pconstr = pconstr_desc with_loc
type pfilter_constr_decl = {
  pfc_name : string;
  pfc_value : pvalue lambda_loc;
}
type phookin_constr_decl = {
  phi_name : string;
  phi_annot : string;
  phi_value : pvalue lambda_loc;
}
type phookout_constr_decl = {
  pho_name : string;
  pho_annot : string;
  pho_value : pvalue lambda_loc;
}
type prulein_constr_decl = {
  pri_name : string;
  pri_value : pvalue lambda_loc;
}
type pruleout_constr_decl = {
  pro_name : string;
  pro_value : pvalue lambda_loc;
}
type var_constr_decl = {
  vc_name : string with_loc;
  vc_type : typedesc with_loc;
}
type pcdecls = {
  pc_typedecls : (string with_loc * (string with_loc * bool * bool) list) list;
  pc_types : (string with_loc * typedesc with_loc) list;
  pc_vars : var_constr_decl list;
  pc_rulein_vars : (string with_loc * typedesc with_loc) list;
  pc_ruleout_vars : (string with_loc * typedesc with_loc) list;
  pc_quarks : quark_decl list;
  pc_filters : pfilter_constr_decl list;
  pc_hookins : phookin_constr_decl list;
  pc_hookouts : phookout_constr_decl list;
  pc_ruleins : prulein_constr_decl list;
  pc_ruleouts : pruleout_constr_decl list;
}

