open Skeltypes

type ('termout, 'skelout) interpretation = {
		var_interp : typed_var -> necro_type list -> 'termout ;
		constr_interp : constructor -> necro_type list -> 'termout -> 'termout ;
		tuple_interp : 'termout list -> 'termout ;
		function_interp : var -> necro_type -> 'skelout -> 'termout ;
		letin_interp : bind -> pattern -> 'skelout -> 'skelout -> 'skelout ;
		apply_interp : typed_var -> necro_type list -> 'termout list -> 'skelout ;
		merge_interp : necro_type -> 'skelout list -> (location * string) option -> 'skelout ;
		return_interp : 'termout -> 'skelout
	}


let rec interpret_skeleton interp = function
	| Branching (ty, branches, loc) ->
		interp.merge_interp ty (List.map (interpret_skeleton interp) branches) loc
	| LetIn (a, p, b, s) ->
		interp.letin_interp a p (interpret_skeleton interp b) (interpret_skeleton interp s)
	| Return t -> interp.return_interp (interpret_term interp t)
	| Apply (f, ta, t) ->
			let t_out = List.map (interpret_term interp) t in
			interp.apply_interp f ta t_out

and interpret_term interp = function
	| TVar (tv, ta) ->
			interp.var_interp tv ta
	| TConstr (c, s, t') ->
			interp.constr_interp c s (interpret_term interp t')
	| TTuple tl ->
			interp.tuple_interp (List.map (fun t -> interpret_term interp t) tl)
	| TFunc (x, ty, s) ->
			interp.function_interp x ty (interpret_skeleton interp s)



let constant_interpretation t s = {
	var_interp = (fun _ _ -> t) ;
	constr_interp = (fun _ _ _ -> t) ;
	tuple_interp = (fun _ -> t) ;
	function_interp = (fun _ _ _ -> t) ;
	letin_interp = (fun _ _ _ _ -> s) ;
	apply_interp = (fun _ _ _ -> s) ;
	merge_interp = (fun _ _ _ -> s) ;
	return_interp = (fun _ -> s)
}


(* TODO: update
 *
 * type ('skelin, 'skelout) finterpretation = {
		before_letin_finterp : 'skelin -> 'skelin ;
		letin_finterp : monadic_annotation -> term -> 'skelin -> 'skelout -> 'skelin ;
		hook_finterp : call -> 'skelin -> 'skelout ;
		filter_finterp : call -> 'skelin -> 'skelout ;
		before_merge_finterp : 'skelin -> 'skelin ;
		merge_finterp : base_type -> 'skelin -> 'skelout list -> 'skelout ;
		return_finterp : monadic_annotation -> term -> 'skelin -> 'skelout
	}

let finterpretation_to_interpretation
	(interp : ('skelin, 'skelout) finterpretation) = {
		letin_interp = (fun a xs interp_bone interp_skel input ->
			let b = interp_bone (interp.before_letin_finterp input) in
			let input = interp.letin_finterp a xs input b in
			interp_skel input) ;
		hook_interp = interp.hook_finterp ;
		filter_interp = interp.filter_finterp ;
		merge_interp = (fun types interps input ->
			let inner_input = interp.before_merge_finterp input in
			let rs = List.map (fun interp -> interp inner_input) interps in
			interp.merge_finterp types input rs) ;
		return_interp = interp.return_finterp
	}

let finterpret_skeleton i =
	interpret_skeleton (finterpretation_to_interpretation i)
*)

(* TODO: Update
let add_interpretation interp ainterp =
	{
		comput_interp = (fun (st, st1) -> (interp.comput_interp st, ainterp.comput_interp (st, st1))) ;
		filter_interp = (fun f (st, st1) -> (interp.filter_interp f st, ainterp.filter_interp f (st, st1))) ;
		merge_interp = (fun vars outs (st, st1) ->
				(interp.merge_interp vars (List.map fst outs) st), ainterp.merge_interp vars outs (st, st1)) ;
		before_merge_interp = (fun (st, st1) -> (interp.before_merge_interp st, ainterp.before_merge_interp (st, st1))) ;
	}

let combine_interpretation interp1 interp2 =
	{
		comput_interp = (fun st -> interp1.comput_interp st, interp2.comput_interp st) ;
		filter_interp = (fun f st -> (interp1.filter_interp f st, interp2.filter_interp f st)) ;
		merge_interp = (fun vars outs st ->
				(interp1.merge_interp vars outs st, interp2.merge_interp vars outs st)) ;
		before_merge_interp = (fun st -> (interp1.before_merge_interp st, interp2.before_merge_interp st))
	}

let combine_interpretation_inout interp1 interp2 =
	{
		comput_interp = (fun st -> let st1 = interp1.comput_interp st in (st1, interp2.comput_interp (st, st1))) ;
		filter_interp = (fun f st -> let st1 = interp1.filter_interp f st in (st1, interp2.filter_interp f (st, st1))) ;
		merge_interp = (fun vars outs st ->
				let st1 = interp1.merge_interp vars outs st in
				(st1, interp2.merge_interp vars outs (st, st1))) ;
		before_merge_interp = (fun st ->
				let st1 = interp1.before_merge_interp st in
				(st1, interp2.before_merge_interp (st, st1))) ;
	}


let interpretation_product interp1 interp2 =
	{
		comput_interp = (fun (st1, st2) -> interp1.comput_interp st1, interp2.comput_interp st2) ;
		filter_interp = (fun f (st1, st2) -> interp1.filter_interp f st1, interp2.filter_interp f st2) ;
		merge_interp = (fun vars outs (st1, st2) ->
				(interp1.merge_interp vars (List.map fst outs) st1, interp2.merge_interp vars (List.map snd outs) st2)) ;
		before_merge_interp = (fun (st1, st2) -> interp1.before_merge_interp st1, interp2.before_merge_interp st2) ;
	}

(*
let constant_interpretation =
	{
		comput_interp = (fun x -> x) ;
		filter_interp = (fun f x -> x) ;
		merge_interp = (fun _ _ x -> x) ;
		before_merge_interp = (fun x -> x) ;
	}
*)

let rec take n = function
	| [] -> []
	| x :: l -> if n = 0 then [] else x :: take (n - 1) l

let accumulating_interpretation interp =
	{
		comput_interp = (fun (st, rest) ->
				let (st, add) = interp.comput_interp st in (st, add @ rest)) ;
		filter_interp = (fun f (st, rest) ->
				let (st, add) = interp.filter_interp f st in (st, add @ rest)) ;
		merge_interp = (fun vars outs (st, rest) ->
				let (st, add) = interp.merge_interp vars (List.map fst outs) st in
				let souts = List.map snd outs in
				(st, add @ List.concat souts @ rest)) ;
		before_merge_interp = (fun (st, _) -> (st, [])) ;
	}

let get_accumulated result = snd result

module Var = struct type t = typed_var let compare x y = compare x.tv_name y.tv_name end
module VMap = Map.Make(Var)
*)

