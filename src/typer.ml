open Util
open Pskeltypes
open Skeltypes
open Skeleton
open Printer


(** * Useful auxiliary base definitions **)

exception TypeError
exception UnificationError

let check_duplicates typ l =
	begin match find_duplicate l with
	| None -> ()
	| Some x -> failwith ("The " ^ typ ^ " " ^ x ^ " has been defined twice.")
	end

type typing_env = {
	te_types : necro_type SMap.t ;
	te_constr_signatures : constructor_signature SMap.t ;
	te_terms : (type_arg list * necro_type) SMap.t ;
	te_aliases : (type_arg list * necro_type) SMap.t
}
type variable_env = necro_type SMap.t


let file_name = ref "???"

(* Return an error at loc [(lstart, lend)] with message [fmt] *)
let type_error (lstart, lend) fmt =
	Format.kasprintf (fun s ->
		Parserutil.report_error !file_name lstart lend ;
		print_endline s ;
		raise TypeError) fmt

(* Try to find [contents] in the map [m] *)
let try_map_find typ m {contents;loc}: 'a =
	begin match SMap.find_opt contents m with
	| Some s -> s
	| None -> type_error loc "Unknown %s %s." typ contents
	end

(* Check the equality of types got and expected *)
let check_type loc got expected =
	if type_compare got expected <> 0 then
		type_error loc "This expression has type %s but was expected of type %s"
			(string_of_type got) (string_of_type expected)

(* Return a variable environment equal to [ve] extended with
 * the associations of [new_ve] *)
let rec extend_ve ve new_ve =
	let f x t1 t2 =
		begin match x, t2 with
		| _, None -> t1
		| _, _ -> t2
		end
	in
	SMap.merge f ve new_ve

(** Given a list of types outputed from a branching, return a merged type **)
let merge_types loc =
	let rec print_list ?(beg="[") =
		begin function
		| [] -> beg ^ "]"
		| a :: [] -> beg ^ Printer.string_of_type a ^ "]"
		| a :: q -> beg ^ Printer.string_of_type a ^ print_list ~beg:";" q
		end
	in
	begin function
	| [] ->
		type_error loc "Empty branching is not permitted."
	| l :: ls ->
		if List.for_all (fun l' -> type_compare l l' = 0) ls then l
		else type_error loc ("Different branches of a branching return different \
	types.\n" ^^ "Types returned : %s") (print_list (l::ls))
	end

(* Caution, this function is not symmetrical.
 * [unify t1 t2] returns [Some m], if [m] is the minimal mapping of type
 * variables to types, such that every variable from [t1] has a mapping in [m],
 * and [t1] with the substitutions from [m] is equal
 * to [t2].
 * [unify t1 t2] returns [None] if there is no such mapping. *)
let rec unify (t1:necro_type) (t2: necro_type): necro_type SMap.t option =
	let union _ t t' = if t = t' then Some t else raise UnificationError in
	begin match t1, t2 with
	| Alias (_ , _, t1), t2 | t1, Alias (_ , _, t2) ->
			unify t1 t2
	| Variable a1, _ ->
			Some (SMap.singleton a1 t2)
	| Product l1, Product l2 ->
			if List.length l1 <> List.length l2 then None else
			let merge m1 t1 t2 =
				let m2 = unify t1 t2 in
				match m1, m2 with
				| Some m1, Some m2 ->
						begin
							try Some (SMap.union union m1 m2)
							with _ -> None
						end
				| _, _ -> None
			in
			List.fold_left2 merge (Some SMap.empty) l1 l2
	| Arrow (s1, s'1), Arrow (s2, s'2) ->
			begin match unify s1 s2 with
			| Some m1 ->
					begin match unify s'1 s'2 with
					| Some m2 -> Some (SMap.union union m1 m2)
					| None -> None
					end
			| None -> None
			end
	| Base (s1, spec1, a1), Base (s2, spec2, a2) when s1 = s2 && spec1 = spec2->
			unify (Product a1) (Product a2)
	| _, _ -> None
	end





(** * Typing functions **)


(* Get a type from a parsed type, given the list tae of type arguments in
 * effect, the typing environment te, and a loc from which to return an error *)
let rec type_type types aliases (tae:type_arg list) (p:poly_ptype with_loc):
	necro_type =
	begin match p.contents with
	| Prod l -> Product (List.map (type_type types aliases tae) l)
	| PType (str, l) when List.mem str tae ->
			if l = [] then Variable str
			else type_error p.loc "The type variable %s is not polymorphic" str
	| PArrow (s1, s2) -> Arrow (type_type types aliases tae s1, type_type types
	aliases tae s2)
	| PType (str, l) ->
			begin match SMap.find_opt str types with
			| None ->
					begin match SMap.find_opt str aliases with
					| Some (ta, ty) -> 
						let given = List.length l in
						let required = List.length ta in
						if given <> required then
							type_error p.loc
							"The type %s expects %d arguments but was given %d"
							str required given
						else
							let args = List.map (type_type types aliases tae) l in
							Alias(str, args, subst_mult ta args ty)
					| None -> type_error p.loc "Unbound type %s" str
					end
			| Some (Product _) | Some (Variable _) | Some (Alias _) | Some (Arrow _) -> assert false
			| Some (Base (s, spec, args)) ->
					let given = List.length l in
					let required = List.length args in
					if given <> required then
						type_error p.loc
						"The type %s expects %d arguments but was given %d"
						str required given
					else
						Base (s, spec, List.map (type_type types aliases tae) l)
			end
	end


(* Given a type [ty], set [pat] to be some element of type [ty], return the
 * corresponding typed pattern and the variable environment that sets variables accordingly *)
let rec assign ty tae te (pat:ppattern with_loc): variable_env * pattern =
	let aux ty t = assign ty tae te t in
	begin match pat.contents with
	| PPWild ->
			let ve = SMap.empty in
			let var = PVar {tv_name = "_" ; tv_typ = ty } in
			(ve, var)
	| PPVar x ->
			let ve = SMap.singleton x ty in
			let var = PVar {tv_name = x ; tv_typ = ty } in
			(ve, var)
	| PPConstr (c, t) ->
			let cdef = try_map_find "constructor" te.te_constr_signatures c in
			let cdef_input = cdef.cs_input_type in
			let cdef_output = cdef.cs_output_type in
			let cdef_output_type_args =
				match cdef_output with
				| Base (s, spec, args) ->
						List.map (function Variable s -> s | _ -> assert false) args
				| _ ->
						assert false
			in
			let ty, actual_type_args =
				let fail () =
					type_error pat.loc "The constructor %s has type %s \
					but an expression of type %s was expected" c.contents
					(string_of_type cdef_output) (string_of_type ty)
				in
				begin match ty with
				| Base (s, _, _) when s <> type_name cdef_output ->
						fail ()
				| Alias (_, _, (Base (_, _, a) as ty)) -> ty, a
				| Base (_, _, a) -> ty, a
				| _ -> fail ()
				end
			in
			(* given a list of variable l1 a list of types l2 and a type ty,
			 * return the same type ty where every variable in l1 has been replaced by
			 * the corresponding type in l2 *)
			let ty_input = subst_mult cdef_output_type_args actual_type_args cdef_input in
			let ve, input = aux ty_input t in
			let type_args = actual_type_args in
			let constr = c.contents in
			let t = PConstr ((constr,ty), type_args, input) in
			ve, t
	| PPTuple t ->
			match ty with
			| Product ty
			| Alias (_, _, Product ty) ->
					let () =
						let lt = List.length t in
						let lty = List.length ty in
						if lt <> lty then
							type_error pat.loc "A %d-uple cannot be affected to a %d-uple" lty lt
					in
					let (ve, t) = List.split (List.map2 (fun ty t -> aux ty t) ty t) in
					let unify =
						let redef x _ _ = type_error pat.loc
							"The variable %s has been defined more than once" x
						in
						List.fold_left (SMap.union redef) SMap.empty
					in
					(unify ve, PTuple t)
			| _ -> type_error pat.loc "The type %s is not a product"
				(Printer.string_of_type ty)
	end


(* given a set of defined type_args [tae], a typing environment [te], and a
 * variable environment [ve], get a typed term from a pterm *)
let rec type_term tae te ve term: term * necro_type =
	let type_term = type_term tae te ve in
		begin match term.contents with
		| PTTuple l ->
				let t, ty = List.split (List.map type_term l) in
				TTuple t, Product ty
		| PTVar (v, ta_got) ->
				let mkvar v t ta = TVar ({tv_typ=t; tv_name=v}, ta) in
				let ta_wrong_length v ta_exp ta_got =
					type_error term.loc "The term v expects %s type arguments but was given %s"
					(string_of_int ta_exp) (string_of_int ta_got)
				in
				let not_found () =
					type_error term.loc "The variable %s was not found in this context" v
				in
				let v_type, ta_exp =
					begin match SMap.find_opt v ve with
					| Some t -> (t, [])
					| None ->
							begin match SMap.find_opt v te.te_terms with
							| Some (ta, t) -> (t, ta)
							| None -> not_found ()
							end
					end
				in
				let () =
					let ta_exp = List.length ta_exp in
					let ta_got = List.length ta_got in
					if ta_exp <> ta_got then
						ta_wrong_length v ta_exp ta_got
				in
				let ta_got = List.map (type_type te.te_types te.te_aliases tae) ta_got in
				let v_type =
					List.fold_left2 (fun ty x s -> subst_type x s ty) v_type ta_exp ta_got
				in
				(mkvar v v_type ta_got, v_type)
		| PTConstr (c, ta_got, t) ->
			let unif_error f_in f_out x =
				type_error term.loc "Unification error: this constructor of type %s \
				cannot be applied to this argument of type %s"
				(string_of_type (Arrow (f_in, f_out))) (string_of_type x)
			in
			let ta_wrong_length f ta_exp ta_got =
				type_error term.loc "The constructor %s expects %s type arguments but \
				was given %s" f (string_of_int ta_exp) (string_of_int ta_got)
			in
			let cdef = try_map_find "constructor" te.te_constr_signatures c in
			let cdef_in = cdef.cs_input_type in
			let cdef_out = cdef.cs_output_type in
			let cdef_ta = cdef.cs_type_args in
			let input, t_in = type_term t in
			let ta_got = List.map (type_type te.te_types te.te_aliases tae) ta_got in
			let () =
				let ta_exp = List.length cdef_ta in
				let ta_got = List.length ta_got in
				if ta_exp <> ta_got then
					ta_wrong_length c.contents ta_exp ta_got
			in
			let f_in, f_out = List.fold_left2 (fun (i,o) x s ->
				(subst_type x s i, subst_type x s o)) (cdef_in, cdef_out) cdef_ta ta_got
			in
			let () = if type_compare t_in f_in <> 0 then unif_error f_in f_out t_in in
			(TConstr ((c.contents, f_out), ta_got, input), f_out)
		| PTType (ty', term) ->
				let ty' = type_type te.te_types te.te_aliases tae ty' in
				let (t, t_typ) = type_term term in
				let () = check_type term.loc t_typ ty' in
				(t, t_typ)
		| PTFunc (x, ty, sk) ->
				let ty = type_type te.te_types te.te_aliases tae ty in
				let new_ve = SMap.add x ty ve in
				let sk, out = type_skeleton tae te new_ve sk in
				(TFunc (x, ty, sk), Arrow (ty, out))
		end


(* Type a skeleton *)
and type_skeleton tae te (ve: variable_env) s =
	begin match s.contents with
	| PLetIn (None, p, b, s) ->
		let (b, output_type) = type_skeleton tae te ve b in
		let new_ve, p =
			assign output_type tae te p in
		let ve = extend_ve ve new_ve in
		let (s, output_type) = type_skeleton tae te ve s in
		(LetIn (None, p, b, s), output_type)
	| PLetIn (Some (m, []), p, s1, s2) ->
			let not_found () =
				type_error s.loc "%s was not found in the environment." m.contents
			in
			let not_a_bind ty =
				let ty = string_of_type ty in
				type_error s.loc "%s does not have the proper type. It has type %s, \
				but a term of type (_ → (_ → _) → _) was expected." m.contents ty
			in
			let sk_not_suiting loc ty exp =
				type_error loc "This skeleton has type %s but is expected to have \
				type %s." (string_of_type ty) (string_of_type exp)
			in
			let cannot_infer_b () =
				type_error p.loc "Cannot infer the type of this pattern."
			in
			let cannot_infer_d () =
				type_error s.loc "Cannot infer the return type of this let-in \
				construct."
			in
			let type_arg_not_inferred x =
				type_error m.loc "Cannot infer the type argument %s for %s. \
				Maybe it doesn't appear in its signature" m.contents x
			in
			let unification_error () =
				type_error m.loc "Unification error: %s cannot be applied \
				to these skeletons" m.contents
			in
			(* get the type of the bind *)
			let ta, bind_type =
				begin match SMap.find_opt m.contents te.te_terms with
				| None -> not_found ()
				| Some (ta, ty) -> (ta, ty)
				end
			in
			(* We expect the bind to have type [a → (b → c) → d] *)
			let a, b, c, d =
				begin match bind_type with
				| Arrow(a, Arrow(Arrow(b, c), d)) -> a, b, c, d
				| _ -> not_a_bind bind_type
				end
			in
			(* type s1 *)
			let (s1_typed, type_of_s1) = type_skeleton tae te ve s1 in
			(* set a = type of s1 *)
			let m1 =
				begin match unify a type_of_s1 with
				| None -> sk_not_suiting s1.loc type_of_s1 a
				| Some m1 -> m1
				end
			in
			let rec subst_aux map cannot =
				begin function
				| Base (s, b, args) ->
						Base (s, b, List.map (subst_aux map cannot) args)
				| Variable v ->
						begin match SMap.find_opt v map with
						| None -> cannot ()
						| Some s -> s
						end
				| Product t ->
						Product (List.map (subst_aux map cannot) t)
				| Alias (str, args, ty) ->
						Alias (str, List.map (subst_aux map cannot) args, subst_aux map cannot ty)
				| Arrow (a, b) ->
						Arrow (subst_aux map cannot a, subst_aux map cannot b)
				end
			in
			(* Check that b is fully inferred *)
			let b_inf = subst_aux m1 cannot_infer_b b in
			(* assign p to have type b_inf *)
			let new_ve, p = assign b_inf tae te p in
			let ve = extend_ve ve new_ve in
			(* type s2 *)
			let (s2_typed, type_of_s2) = type_skeleton tae te ve s2 in
			(* set c = type of s2 *)
			let m2 =
				begin match unify c type_of_s2 with
				| None -> sk_not_suiting s2.loc type_of_s2 c
				| Some m2 -> m2
				end
			in
			(* Check that what is inferred from a and what is inferred from c is
			 * consistent *)
			let map =
				let consistent _ s1 s2 = 
					if type_compare s1 s2 = 0 then Some s1 else
						unification_error ()
				in
				SMap.union consistent m1 m2
			in
			(* Check that d is fully inferred *)
			let d_inf = subst_aux map cannot_infer_d d in
			(* Check that all type arguments have been inferred *)
			let ta = List.map (fun x ->
				begin match SMap.find_opt x map with
				| None -> type_arg_not_inferred x
				| Some s -> s
				end) ta
			in
			(* the output type is d_inf *)
			(LetIn (Some (m.contents,ta), p, s1_typed, s2_typed), d_inf)
	| PLetIn (Some (m, ta), p, s1, s2) ->
			let not_a_bind ty =
				let ty = string_of_type ty in
				type_error s.loc "%s does not have the proper type. It has type %s, \
				but a term of type (_ → (_ → _) → _) was expected." m.contents ty
			in
			let (bind, bind_type) =
				type_term tae te ve {loc=m.loc; contents=PTVar (m.contents, ta)}
			in
			let ta =
				begin match bind with
				| TVar (_, ta) -> ta
				| _ -> assert false (* A PTVar is always typed as a TVar *)
				end
			in
			let a, b, c, d =
				begin match bind_type with
				| Arrow(a, Arrow(Arrow(b, c), d)) -> a, b, c, d
				| _ -> not_a_bind bind_type
				end
			in
			(* type s1 *)
			let (s1_typed, type_of_s1) = type_skeleton tae te ve s1 in
			(* check a = type of s1 *)
			let () = check_type s1.loc type_of_s1 a in
			(* assign p to have type b *)
			let new_ve, p = assign b tae te p in
			let ve = extend_ve ve new_ve in
			(* type s2 *)
			let (s2_typed, type_of_s2) = type_skeleton tae te ve s2 in
			(* check c = type of s2 *)
			let () = check_type s2.loc type_of_s2 c in
			(* the output type is d *)
			(LetIn (Some (m.contents,ta), p, s1_typed, s2_typed), d)
	| PBranching ss ->
		let nbve = List.map (type_skeleton tae te ve) ss in
		let nb, ves = List.split nbve in
		let output_type = merge_types s.loc ves in
		(Branching (output_type, nb, Some (s.loc, !file_name)), output_type)
	| PReturn xs ->
			let (t, typ) = type_term tae te ve xs in
			(Return t, typ)
	| PApply (f, ta_got, l) ->
			let not_an_arrow ta f =
				let ty = string_of_type f in
				type_error s.loc "This argument has type %s, it cannot be applied" ty
			in
			let not_found f =
				type_error s.loc "Unbound variable %s" f
			in
			let unif_error f_typ arg_typ =
				type_error s.loc "Unification error: this term of type %s cannot \
				be applied to this argument of type %s"
				(string_of_type f_typ) (string_of_type arg_typ)
			in
			let ta_wrong_length f ta_exp ta_got =
				type_error s.loc "The term %s expects %s type arguments but was given %s"
				f (string_of_int ta_exp) (string_of_int ta_got)
			in
			let f_in, f_out, ta_exp =
				begin match SMap.find_opt f.contents ve with
				| Some (Arrow (a, b)) -> (a, b, [])
				| Some t -> not_an_arrow [] t
				| None ->
						begin match SMap.find_opt f.contents te.te_terms with
						| Some (ta, Arrow (a, b)) -> (a, b, ta)
						| Some (ta, t) -> not_an_arrow ta t
						| None -> not_found f.contents
						end
				end
			in
			let (l, l_typ) = List.split (List.map (type_term tae te ve) l) in
			let () =
				let ta_exp = List.length ta_exp in
				let ta_got = List.length ta_got in
				if ta_exp <> ta_got then
					ta_wrong_length f.contents ta_exp ta_got
			in
			let ta_got = List.map (type_type te.te_types te.te_aliases tae) ta_got in
			let f_in, f_out =
				List.fold_left2 (fun (i,o) x s -> (subst_type x s i, subst_type x s o)) (f_in, f_out) ta_exp ta_got
			in
			let ty =
				let rec apply f l =
					begin match f, l with
					| Arrow (a, b), a' :: q when type_compare a a' = 0 -> apply b q
					| f, [] -> f
					| f, arg :: _ -> unif_error f arg
					end
				in
				apply (Arrow (f_in, f_out)) l_typ
			in
			Apply ({tv_typ=Arrow (f_in, f_out); tv_name=f.contents}, ta_got, l), ty
	end





(** * Main functions *)


(* Check if there are any duplicate definitions *)
let check_duplicates_decls abs var terms =
	let constructor_names =
		List.flatten (List.map (fun (_, _, l) ->
			List.map (fun (s, _) -> s.contents) l) var)
	in
	begin
		check_duplicates "constructor" constructor_names
	end


(* Construct the field for types in the typing environment *)
let mkte_types abs var =
	let dup_def name =
		failwith ("The type " ^ name ^ " has been defined twice.")
	in
	let fail_type_args name =
		failwith ("The type " ^ name ^ " has been defined twice with a different number of \
		parameters")
	in
	let add_or_fail name base_type m =
		begin match SMap.find_opt name m, base_type with
		| None, _ -> SMap.add name base_type m
		| Some (Base (s, false, a)), Base (s', false, a') ->
				if a = a' then m else fail_type_args name
		| _ -> assert false
		end
	in
	let add_or_replace name base_type m =
		begin match SMap.find_opt name m, base_type with
		| None, _ -> SMap.add name base_type m
		| Some (Base (s, false, a)), Base (s', true, a') ->
				if List.length a = List.length a' then
					SMap.add name base_type m
				else
					fail_type_args name
		| Some (Base (s, true, a)), _ -> dup_def name
		| _ -> assert false
		end
	in
	let var = List.map (fun (x, n, _) -> (x, n)) var in
	let mk_list_args i = List.init i (fun _ -> Variable "_") in
	let m = SMap.empty in
	let mktype a b s = Base (s, b, a) in
	let mkabs n = mktype (mk_list_args n) false in
	let mkvar a = mktype (List.map (fun s -> Variable s) a) true in
	let m = List.fold_left (fun m (x, i) -> add_or_fail x (mkabs i x) m) m abs in
	let m = List.fold_left (fun m (x, n) -> add_or_replace x (mkvar n x) m) m var in
	m

(* Construct the field for aliases in the typing environment *)
let mkte_aliases te_types aliases =
	let redef loc n =
		type_error loc "The type %s already exists, you cannot define an alias \
		with the same name" n
	in
	let redef_alias loc n =
		type_error loc "The alias %s was already defined" n
	in
	let type_alias n ta ty =
		if SMap.mem n te_types then redef ty.loc n
		else
		type_type te_types SMap.empty ta ty
	in
	let add_or_fail (name, ta, ty) m =
		begin match SMap.find_opt name m with
		| None -> SMap.add name (ta, type_alias name ta ty) m
		| Some _ -> redef_alias ty.loc name
		end
	in
	List.fold_left (fun m (n, ta, ty) ->
		add_or_fail (n, ta, ty) m) SMap.empty aliases

(* Create a constructor environment from a list of variant type declarations *)
let mkte_consts var te_types te_aliases =
	let const =
		List.flatten (List.map (fun (o, ta, cst) -> List.map (fun (name, s) ->
			{
				cs_type_args = ta ;
				cs_name = name.contents ;
				cs_input_type = (type_type te_types te_aliases ta) s ;
				cs_output_type = Base (o, true, List.map (fun x -> Variable x) ta)
			}) cst) var) in
	List.fold_left (fun m c -> SMap.add c.cs_name c m) SMap.empty const


(* Construct the field for terms in the typing environment *)
let mkte_terms te_types te_aliases terms =
	let dup_sig name loc =
		type_error loc "The term %s has been declared twice with two different \
		signatures. Second declaration here"
		name
	in
	let add_or_replace loc name (ta, ty) m =
		begin match SMap.find_opt name m with
		| None -> SMap.add name (ta, ty) m
		| Some (ta_bef, ty_bef) when ta = ta_bef && type_compare ty ty_bef = 0 -> m
		(* TODO: allow for redefinition with different name for type arguments.
		 * e.g. term bla<a>: a -> a      and     term bla<b>: b -> b *)
		| Some (ta_bef, ty_bef) -> dup_sig name loc
		end
	in
	List.fold_left
	(fun m (name, ta, ty, pterm) ->
		let ty' = type_type te_types te_aliases ta ty in
		add_or_replace ty.loc name (ta, ty') m)
	SMap.empty terms


(* Construct the field for types in the skeletal_semantics *)
let mkss_types te_types te_constr_signatures =
	let get_constr s =
		SMap.fold (fun cname csig l ->
			begin
				if type_name (csig.cs_output_type) = s then
					List.cons csig
				else
					id
			end
			l) te_constr_signatures []
	in
	SMap.mapi (fun s def ->
		(def, let c = get_constr s in if c = [] then None else Some c))
	te_types


(* Construct the field for terms in the skeletal_semantics *)
let mkss_terms te terms =
	let dup_def name loc =
		type_error loc "The term %s has been defined twice. Second definition here"
		name
	in
	let add_or_replace loc name (ta, ty, term) m =
		begin match SMap.find_opt name m with
		| None | Some (_, _, None) ->
				SMap.add name (ta, ty, term) m
		| Some (_, _, Some _) when term = None -> m
		| _ -> dup_def name loc
		end
	in
	let mk_one_term name ta ty pterm =
		let ty = type_type te.te_types te.te_aliases ta ty in
		let term =
			begin match pterm with
			| None -> None
			| Some pterm ->
					let (term, term_typ) = type_term ta te SMap.empty pterm in
					let () = check_type pterm.loc term_typ ty in
					Some term
			end
		in
		(ta, ty, term)
	in
	List.fold_left (fun m (name, ta, ty, pterm) ->
			add_or_replace ty.loc name (mk_one_term name ta ty pterm) m) SMap.empty terms


(* Rename all variables from [to_remap] into new valid names in the
 * skeletal_semantics [ss] *)
let rename_tmps to_remap ss =
	(* Remap names of fresh variables to correct identifiers *)
	let names = ss_all_names ss in
	let tt = Product [] in
	let tmps = List.map (fun _ -> TVar ({tv_name="_tmp";tv_typ=tt},[])) to_remap in
	let term_tmps = TTuple tmps in
	let term_new_tmps = term_fresh (SSet.elements names) (term_tmps) in
	let new_tmps =
		match term_new_tmps with
		| TTuple l -> List.map (fun t ->
				match t with | TVar (tv,[]) -> tv.tv_name | _ -> assert false) l
		| _ -> assert false
	in
	let rename v =
		match find_index_opt v to_remap with
		| None -> v
		| Some i -> List.nth new_tmps i
	in
	ss_rename rename ss




(* Get a full environnement form a list of declarations *)
let type_program filename (decls, tmps) =

	(* Extract the informations *)
	let () = file_name := filename in
	let abs = decls.pabs_type in
	let var = decls.pvar_type in
	let terms = decls.pterms in
	let aliases = decls.palias in

	(* Check for duplicates in the declarations *)
	let () = check_duplicates_decls abs var terms in

	(* Construct the typing environment *)
	let te_types = mkte_types abs var in
	let te_aliases = mkte_aliases te_types aliases in
	let te_constr_signatures = mkte_consts var te_types te_aliases in
	let te_terms = mkte_terms te_types te_aliases terms in
	let te = { te_types ; te_constr_signatures ; te_terms ; te_aliases } in

	(* Construct the skeletal_semantics *)
	let ss_types = mkss_types te_types te_constr_signatures in
	let ss_aliases = te_aliases in
	let ss_terms = mkss_terms te terms in
	let ss = { ss_types ; ss_terms ; ss_aliases } in

	(* Fix the invalid temporary names generated by the parser *)
	let to_remap = List.init tmps (fun i -> " " ^ string_of_int (i + 1)) in
	let ss_fixed = rename_tmps to_remap ss in

	(* Voilà! *)
	ss_fixed



