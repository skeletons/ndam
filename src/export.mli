
(** A generic mondule signature for exportators.
 * The inner type is the type of the extracted program. **)
module type Exportator = sig
    type t
    type hint
    val export : Skeltypes.skeletal_semantics -> hint -> t
    val print : t -> string
  end

(** Generate a GIL file. **)
module Gil : Exportator with type hint = string ExportInstr.env

