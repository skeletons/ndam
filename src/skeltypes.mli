(** Memory representation of skeletons and skeletal semantics **)

open Util

type location = Lexing.position * Lexing.position

(** The structure of skeletal semantics presents a lot of redundancy.
 * This is normal.  We find that it really helps programming. **)

type type_name = string
type hook_name = string
type annotation = string
type var = string
type type_arg = string


type necro_type =
	| Variable of type_arg
	(* In [Base (n, b, ta)], [n] is the name, [ta] the type arguments for
	 * polymorphic types, and the boolean [b] states whether the type is specified
	 * ([b = true]) or unspecified ([b = false]) *)
	| Base of type_name * bool * necro_type list
	(* Invariant: In [Product t], the length of [t] is either 0 or ≥ 2 *)
	| Product of necro_type list
	(* [Alias (n, ta, t)], means the type [t], which is referred by its alias
	   [n<ta>].
	   Invariant: There is never a term matching [Alias (_, _, Alias _)] *)
	| Alias of type_name * necro_type list * necro_type
	| Arrow of necro_type * necro_type

type bind = (string * necro_type list) option
type constructor = string * necro_type

(** The signature of a constructor is its name, the types of all its arguments, and
 * the necro_type of the term that is produced. **)
type constructor_signature = {
		cs_name : string ;
		cs_type_args : type_arg list ;
		cs_input_type : necro_type ;
		cs_output_type : necro_type
	}

(** A variable, together with its type information. **)
type typed_var = {
		tv_typ : necro_type ;
		tv_name : var
	}

type pattern =
	| PWild of necro_type
	| PVar of typed_var
	| PConstr of constructor * necro_type list * pattern
	| PTuple of pattern list

(** A term, which can be either a variable or a constructor applied to some
 * terms (with some occasionnal type arguments). **)
(* Invariant: In [TTuple t], the length of [t] is not 1 *)
type term =
	| TVar of typed_var * necro_type list
	| TConstr of constructor * necro_type list * term
	| TTuple of term list
	| TFunc of var * necro_type * skeleton

(** Definition of skeletons. **)
and skeleton =
	| Branching of necro_type * skeleton list * (location * string) option
	| LetIn of bind * pattern * skeleton * skeleton
	| Return of term
	(* Invariant: in [Apply (f, x1 :: … :: xn :: [])]
	 * the type of [f] is [A1 → … → An → B], and * [xi:Ai] *)
	| Apply of typed_var * necro_type list * term list


(** The skeletal semantics maps type names to their description, constructor names to
 * their signatures, and terms to their description. **)
(* Invariant: if [ss_types] maps [n] to [(s, Some cl)], then [s] matches
 * [Base (m, true, _) when m = n] and for all [c] in [cl], [c.cs_output_type = s]*)
(* Invariant: if [ss_types] maps [n] to [(s, None)], then [s] matches
 * [Base (m, false, _) when m = n] *)
type skeletal_semantics = {
		ss_types : (necro_type * constructor_signature list option) SMap.t ;
		ss_aliases : (type_arg list * necro_type) SMap.t ;
		ss_terms : (type_arg list * necro_type * term option) SMap.t
	}

type necro_option =
	| SafeMode
	| NoShadow
	| OutputFile of string
	| AbstractType of string
	| AbstractHook of string
