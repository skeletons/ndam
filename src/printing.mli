(* TODO: This file has been automatically generated and needs clean-up and documentation. *)

type 'a cat_list = Base of 'a list | Cat of 'a cat_list list
type printitem
val str : string -> printitem
val nl : printitem
val stdindent : int
val indent : printitem
val dedent : printitem
val empty : 'a cat_list
val flatten_cat_list : 'a cat_list -> 'a list
val pp_print_items : Format.formatter -> int -> printitem list -> unit
val pp_print_cl : Format.formatter -> printitem cat_list -> unit
val make_list :
  ?first:'a cat_list ->
  ?last:'a cat_list ->
  ?sep:'a cat_list -> ?zero:'a cat_list -> 'a cat_list list -> 'a cat_list
val optmap : ('a -> 'b) -> 'a option -> 'b option
val cs : string -> printitem cat_list
val nlc : printitem cat_list
val make_list_cs :
  ?first:string ->
  ?last:string ->
  ?sep:string ->
  ?zero:string -> printitem cat_list list -> printitem cat_list

