
open Util

type 'a item =
	| Var of 'a
	| String of string
type 'a output = 'a item list

let print env l =
	Option.map (fun l -> String.concat "" (List.rev l))
		(List.fold_left (fun o i ->
			Option.bind o (fun l ->
				match i with
				| Var x -> Option.map (fun str -> str :: l) (env x)
				| String str -> Some (str :: l))) (Some []) l)

type 'a filter = {
		inputs : 'a list ;
		fresh : 'a list ;
		instructions : 'a output list ;
		assume : 'a output option ;
		return : 'a output list
	}

type 'a env = 'a filter SMap.t

