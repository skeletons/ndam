
(** The exception that is raised if typing fails. **)
exception TypeError

(** The skeletal semantics declared in the Pskeltypes module are untyped,
 * but the skeletal semantics used throughout the rest of the Necro project
 * are typed.
 * This function performs the conversion. **)
val type_program : string -> Pskeltypes.pdecls * int -> Skeltypes.skeletal_semantics

