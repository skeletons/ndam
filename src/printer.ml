(* This file provides functions to print the types defined in the Skeltypes module. *)

open Util
open Skeltypes
open Skeleton
open Interpretation


let add_parentheses str =
	if String.index_opt str ' ' = None then
		str
	else if str.[0] = '(' then (
		let rec close_paren i =
			if i >= String.length str then i
			else
				match str.[i] with
				| ')' -> i
				| '(' -> close_paren (1 + close_paren (1 + i))
				| _ -> close_paren (1 + i) in
		if close_paren 1 = String.length str - 1 then str
		else "(" ^ str ^ ")"
	) else "(" ^ str ^ ")"

let rec print_type_args ?(deb="<") print = function
	| [] -> ""
	| x :: [] -> deb ^ print x ^ ">"
	| x :: q -> deb ^ print x ^ print_type_args ~deb:(", ") print q

let rec string_of_type =
	begin function
	| Variable s -> s
	| Base (s, _, []) -> s
	| Base (s, _, args) -> s ^ print_type_args string_of_type args
	| Product l -> 
			let l' = (List.map string_of_type l) in
			"(" ^ String.concat ", " l' ^ ")"
	| Alias (str, args, _) -> str ^ print_type_args string_of_type args
	| Arrow (s1, s2) ->
			let print_s1 =
				match s1 with
				| Arrow _ -> "(" ^ string_of_type s1 ^ ")"
				| _ -> string_of_type s1
			in
			print_s1 ^ " → " ^ string_of_type s2
	end


let rec string_of_pattern p =
	begin match p with
	| PTuple [] -> "()"
	| PTuple l -> "(" ^ String.concat ", " (List.map string_of_pattern l) ^ ")"
	| PVar x -> x.tv_name
	| PWild _ -> "_"
	| PConstr ((c,_), _, p) -> 
		let args = string_of_pattern p in
		if args = "()" then c else
		c ^ " " ^ add_parentheses args
	end


(* An interpretation that products string for a given indentation *)
let string_of_interpretation: (int -> string, int -> string) interpretation =
	let nl indent = "\n" ^ String.make indent ' ' in
	{
		var_interp = (fun tv args _ ->
			tv.tv_name ^ print_type_args string_of_type args) ;
		constr_interp = (fun (c, _) args t indent ->
			let t = match add_parentheses (t indent) with | "()" -> "" | s -> " " ^ s in
			c ^ print_type_args string_of_type args ^ t) ;
		tuple_interp = (fun tl indent ->
			"(" ^ String.concat ", " (List.map (fun f -> f indent) tl) ^ ")") ;
		function_interp = (fun x ty sk i ->
			"λ " ^ x ^ " : (" ^ string_of_type ty ^ ") →" ^ nl (i+2) ^ sk (i+2)) ;
		letin_interp = (fun b p s1 s2 indent ->
				let monoline = String.index_opt (s1 indent) '\n' = None in
				let annot = match b with | None -> "" | Some (b,_) -> "%" ^ b in
				let pattern = string_of_pattern p in
				"let" ^ annot ^ " " ^ pattern ^ " =" ^
				(if monoline then
					(" " ^ s1 indent)
				else nl (indent + 2) ^ s1 (indent + 2)) ^
				(if monoline then " " else nl indent) ^
				"in" ^ nl indent ^ s2 indent) ;
		merge_interp = (fun _ s _ indent ->
			let sep = nl indent ^ "or" ^ nl (indent + 2) in
			let branches = String.concat sep (List.map (fun x -> x (indent + 2)) s) in
			"branch" ^ nl (indent + 2) ^ branches ^ nl indent ^ "end" );
		apply_interp = (fun tv a tl indent ->
			tv.tv_name ^ print_type_args string_of_type a ^ " " ^
			String.concat " " (List.map (fun t -> add_parentheses (t (indent + 2))) tl)) ;
		return_interp = id ;
	}

let string_of_term ~indent t = interpret_term string_of_interpretation t indent
let string_of_skeleton ~indent t = interpret_skeleton string_of_interpretation t indent

let string_of_term_decl name ta ty def =
	let nl = "\n" in
	let rec aux ty sk =
		match sk, ty with
		| Return (TFunc (x, x_ty, sk)), Arrow (_, ty) ->
				"(" ^ x ^ ":" ^ string_of_type x_ty ^ ") → " ^ aux ty sk
		| _ -> string_of_type ty ^ " =" ^ nl ^ "  " ^ string_of_skeleton ~indent:2 sk ^ nl
	in
	match def with
	| None ->
			"term " ^ name ^ print_type_args id ta ^ " : " ^ string_of_type ty ^ nl
	| Some t ->
			"term " ^ name ^ print_type_args id ta ^ " : " ^
			aux ty (Return t)

let string_of_alias_decl name ta ty =
	let nl = "\n" in
	"type " ^ name ^ print_type_args id ta ^ " := "
	^ string_of_type ty ^ nl

let string_of_ss env =
	let nl = "\n" in
	let type_decls =
		SMap.fold (fun name (s, constructors) str ->
			str
			^ match s with
				| Product _  | Variable _ | Alias _ | Arrow _ ->
						assert false (* only called on types defined in the skeleton file *)
				| Base (name, spec, args) ->
						begin match constructors with
						| None ->
								let () = assert (not spec) in
								"type " ^ string_of_type s ^ nl
						| Some constructors ->
								let () = assert spec in
								"type " ^ string_of_type s ^ " =" ^ nl ^
								List.fold_left (fun str csig ->
									if type_name csig.cs_output_type = name then
										(str ^ "| " ^ csig.cs_name ^
										(if type_compare (csig.cs_input_type) (Product []) <> 0
										then (" " ^ string_of_type csig.cs_input_type) else "")
										^ nl)
									else str) "" constructors
						end)
		env.ss_types "" in
	let term_decls = SMap.fold (fun name (ta, ty, def) str ->
		str ^ string_of_term_decl name ta ty def) env.ss_terms "" in
	let alias_decls = SMap.fold (fun name (ta, ty) str ->
		str ^ string_of_alias_decl name ta ty) env.ss_aliases "" in
	String.concat nl [type_decls ; alias_decls ; term_decls ]

