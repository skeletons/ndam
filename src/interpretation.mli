(** This module defines interpretations, a generic way to extract values out of skeletons **)

open Skeltypes

(** * Base Interpretation **)

(** The type of interpretations: an interpretation is a collection of functions
 * that produce outputs by induction over the structure of skeletons.
 * This presentation enables to compose interpretations.
 * This type is parameterised by skeletal and bone outputs (['skelout] and ['boneout]). **)
type ('termout, 'skelout) interpretation = {
    var_interp : typed_var -> necro_type list -> 'termout ;
    constr_interp : constructor -> necro_type list -> 'termout -> 'termout ;
    tuple_interp : 'termout list -> 'termout ;
    function_interp : var -> necro_type -> 'skelout -> 'termout ;
    letin_interp : bind -> pattern -> 'skelout -> 'skelout -> 'skelout ;
    apply_interp : typed_var -> necro_type list -> 'termout list -> 'skelout ;
    merge_interp : necro_type -> 'skelout list -> (location * string) option -> 'skelout ;
    return_interp : 'termout -> 'skelout
  }

(** These functions compute a value out of an interpretation and a term or a skeleton.
 * In some ways, these two functions are the two inductive principles over skeletons,
 * and the [interpretation] type stores all its cases. **)
val interpret_term : ('termout, 'skelout) interpretation -> term -> 'termout
val interpret_skeleton : ('termout, 'skelout) interpretation -> skeleton -> 'skelout


val constant_interpretation: 'term -> 'skel -> ('term, 'skel) interpretation

(** * Alternative Interpretations **)

(** Most interpretations take an input and return an output.
 * For instance, the concrete and abstract interpretations assume a state as an input,
 * returning an updated state after going through the skeleton. 
 * This alternative scheme is a slight variation of the ['skelout) interpretation] type,
 * with input values: ['skelin] is the input for skeletons and ['skelout] the
 * output for skeletons In some ways, this type is very close to
 * [('skelin -> 'skelout) interpretation]
 * (see function [finterpretation_to_interpretation] below).
 * In addition to the [interpretation] record, this record depends on two additional fields:
 * [before_letin_interp] called when reaching a [LetIn] constructor; and
 * [before_merge_finterp] called when reaching a [Branching] constructor. **)
(*type ('skelin, 'skelout) finterpretation = {
    before_letin_finterp : 'skelin -> 'skelin ;
    letin_finterp : bind -> term -> 'skelin -> 'skelout -> 'skelin ;
    hook_finterp : call -> 'skelin -> 'skelout ;
    filter_finterp : call -> 'skelin -> 'skelout ;
    before_merge_finterp : 'skelin -> 'skelin ;
    merge_finterp : ntype -> 'skelin -> 'skelout list -> 'skelout ;
    return_finterp : bind -> term -> 'skelin -> 'skelout
  }
*)
(** This function converts this alternative interpretation into the more generic one, which enables
 * to reuse any function defined on top of the more general scheme. **)
(*val finterpretation_to_interpretation :
  ('skelin, 'skelout) finterpretation -> ('skelin -> 'skelout) interpretation
*)
(** Similar functions than [interpret_skeleton] and [interpret_bone], but for this alternative interpretation. **)
(*val finterpret_skeleton : ('skelin, 'skelout) finterpretation -> skeleton -> 'skelin -> 'skelout

*)
