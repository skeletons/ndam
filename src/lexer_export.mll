{
  open Parser
  open Lexing

  exception Lexing_error of string

  let kw = [
    "branch", BRANCH;
    "end", END;
    "hook", HOOK;
    "of", OF;
    "or", OR;
    "type", TYPE;
    "val", VAL;

    "filter", FILTER;
    "in", IN;
    "let", LET;
    "term", TERM;
    "unit", UNIT;

    "monad", MONAD;
    "ret", RET;

    "True", TRUE;
    "and", AND;
    "compare", COMPARE;
    "forall", FORALL;
    "join", JOIN;
    "list", LIST;
    "map", MAP;
    "out", OUT;
    "program_point", PROGRAM_POINT;
    "rule", RULE;
    "set", SET;
    "using", USING;
    "var", VAR;
    "with", WITH;

    "fresh", FRESH;
    "instruction", INSTRUCTION;
    "assume", ASSUME;
    "return", RETURN;
  ]

  let keywords = Hashtbl.create (List.length kw)
  let () = List.iter (fun (a, b) -> Hashtbl.add keywords a b) kw

  let is_keyword str = Hashtbl.mem keywords str

  let newline lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <- { pos with pos_lnum = pos.pos_lnum + 1 ; pos_bol = pos.pos_cnum }
}

let alpha = ['a'-'z'] | ['A'-'Z']
let ident_car = alpha | '_' | '\'' | ['0'-'9']
let lident = ( ['a'-'z'] ident_car* ) | ( '_' ident_car+ )
let uident = ['A'-'Z'] ident_car*
let whitespace = [' ' '\t']
let newline = "\n" | "\r\n" | "\r"

rule token = parse
  | whitespace+ { token lexbuf }
  | "(*"        { comment lexbuf; token lexbuf }
  | newline     { newline lexbuf; token lexbuf }
  (* Operators *)
  | "|"         { BAR }
  (* | "||"        { BARBAR } *)
  (* | "|->"       { BARMINUSGREATER } *)
  | "^"         { CARET }
  | ":"         { COLON }
  | ","         { COMMA }
  | "="         { EQUAL }
  (* | "<-" | "←"  { LESSMINUS } *)
  | "<="        { LESSEQUAL }
  (* | "-"         { MINUS } *)
  | "->" | "→"  { MINUSGREATER }
  (* | "?>"        { QUESTIONGREATER } *)
  | ";"         { SEMI }
  | "/\\" | "∧" { SLASHBACKSLASH }
  | "*"         { STAR }
  | "?"         { QUESTIONMARK }
  | "%"         { PERCENT }
  (* Paired delimiters *)
  | "("         { LPAREN }
  | ")"         { RPAREN }
  | "["         { LBRACK }
  | "]"         { RBRACK }
  | "<"         { LESS }
  | ">"         { GREATER }
  | "{"         { LBRACE }
  | "}"         { RBRACE }
  (* | "[|"        { LBRACKBAR } *)
  (* | "|]"        { BARRBRACK } *)
  | '"'         { STRING (String.concat "" (parse_string lexbuf)) }
  | "_"         { UNDERSCORE }
  | lident as s { try Hashtbl.find keywords s with Not_found -> LIDENT s }
  | uident as s { try Hashtbl.find keywords s with Not_found -> UIDENT s }
  | eof         { EOF }

and comment = parse
  | "\n" { newline lexbuf; comment lexbuf }
  | "*)" { () }
  | "(*" { comment lexbuf; comment lexbuf }
  | _    { comment lexbuf }
  | eof  { raise (Lexing_error "Unterminated comment") }

and parse_string = parse
  | '\n'                { newline lexbuf; parse_string lexbuf }
  | '"'                 { [] }
  | "\\\""              { "\"" :: parse_string lexbuf }
  | "\\\\"              { "\\" :: parse_string lexbuf }
  | [^ '"' '\\' '\n']* as s  { s :: parse_string lexbuf }
  | eof                 { raise (Lexing_error "Unterminated string") }

