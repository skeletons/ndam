open Util

type typedesc = Pconstr.typedesc =
	| MapType of typedesc * typedesc
	| SetType of typedesc
	| ListType of typedesc
	| BaseType of string
	| TupleType of typedesc list
	| Program_point of typedesc
	| TopType
	| BottomType

type quark_decl = Pconstr.quark_decl = {
	q_name : string ;
	q_inputs : typedesc list ;
	q_output : typedesc ;
}

type 'a lambda = 'a Pconstr.lambda

(* Type annotation, to be used for all subterms *)
type 'a with_type = {
	desc : 'a ;
	typ : typedesc ;
}

type lvalue_desc =
	| LMapGet of lvalue * value
	| LNamedVar of string

and lvalue = lvalue_desc with_type

and value_desc =
	| LocalVar of string
	| MapGet of value * value
	| NamedVar of string
	| Func of string * value list
	| VLet of value * value lambda
	| VWith of constr * value
	| VTuple of value list
	| VSet of value list
	| VList of value list
	(* join_{args in v1} v2 *)
	| VJoin of value * value lambda
	(* join_{_ in v1} v2 = if empty v1 then bot else v2 *)
	| VIsEmpty of value * value
	| VUnit

and value = value_desc with_type

and constr =
	| True
	| Conj of constr * constr
	| CLet of value * constr lambda
	| Sub of value * lvalue
	(* forall_{args in v} c *)
	| Forall of value * constr lambda
	(* forall_{_ in v} c = if empty v then c *)
	| CEmpty of value * constr

(* Location: a variable name, and a key if the variable is a map *)
type dpath = string * value option

(* Set of dependencies *)
type deps =
	| DBase of dpath list
	| DLet of value * deps lambda
	| DForall of value * deps lambda
	| DEmpty of value * deps
	| DConj of deps * deps

(* Environment for typing constraints *)
type tcenv = {
	(* Abstract types *)
	tcenv_basetypes : (string * bool * bool) list SMap.t ;
	(* Definition of type abbreviations *)
	tcenv_types : typedesc SMap.t ;
	(* (variable type, is variable local?) *)
	tcenv_vars : (typedesc * bool) SMap.t ;
	(* input and output types for each rule *)
	tcenv_rulevars : (typedesc * typedesc) SMap.t ;
	(* quark declarations *)
	tcenv_funcs : quark_decl SMap.t ;
}

(* filter definition *)
type filter_constr_decl = {
	fc_name : string ;
	fc_value : value lambda ;
}

(* hook input definition *)
type hookin_constr_decl = {
	hi_name : string ;
	hi_annot : string ;
	hi_value : value lambda ;
}

(* hook output definition *)
type hookout_constr_decl = {
	ho_name : string ;
	ho_annot : string ;
	ho_value : value lambda ;
}

(* rule enter definition *)
type rulein_constr_decl = {
	ri_name : string ;
	ri_value : value lambda ;
}

(* rule exit definition *)
type ruleout_constr_decl = {
	ro_name : string ;
	ro_value : value lambda ;
}

(* Environment containing all declarations in a constraint file *)
type cenv = {
	ce_tce : tcenv ;
	ce_tenv : Skeltypes.env ;
	ce_filts : filter_constr_decl SMap.t ;
	ce_hookins : hookin_constr_decl SMap.t ;
	ce_hookouts : hookout_constr_decl SMap.t ;
	ce_ruleins : rulein_constr_decl SMap.t ;
	ce_ruleouts : ruleout_constr_decl SMap.t ;
}

(* Type printer *)
let rec print_type ff t =
	match t with
	| SetType t -> Format.fprintf ff "(%a) set" print_type t
	| ListType t -> Format.fprintf ff "(%a) list" print_type t
	| BaseType s -> Format.fprintf ff "%s" s
	| MapType (t1, t2) -> Format.fprintf ff "(%a, %a)" print_type t1 print_type t2
	| TupleType [] -> Format.fprintf ff "Unit"
	| TupleType l -> Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff " * ") print_type ff l
	| TopType -> Format.fprintf ff "Any"
	| BottomType -> Format.fprintf ff "Nothing"
	| Program_point TopType -> Format.fprintf ff "program_point"
	| Program_point t -> Format.fprintf ff "(%a) program_point" print_type t

(* Fresh variable name generator *)
let fresh =
	let c = ref 0 in
	fun name ->
		incr c; "__" ^ name ^ "_" ^ (string_of_int !c)

(* Output type of a lambda *)
let lambda_type (_, v) = v.typ


(* Count number of variable occurrences *)
let var_appears_lambda var_appears v (args, body) =
	if List.mem v args then 0 else var_appears v body

let rec var_appears_lvalue var lv = match lv.desc with
	| LNamedVar _ -> 0
	| LMapGet (lv, v) -> var_appears_lvalue var lv + var_appears_value var v

and var_appears_value var v = match v.desc with
	| NamedVar _ | VUnit -> 0
	| MapGet (v1, v2) -> var_appears_value var v1 + var_appears_value var v2
	| LocalVar var1 -> if var = var1 then 1 else 0
	| VTuple l | VSet l | VList l | Func (_, l) -> List.fold_left (+) 0 (List.map (var_appears_value var) l)
	| VWith (c, v) -> var_appears_constr var c + var_appears_value var v
	| VLet (v1, body) | VJoin (v1, body) -> var_appears_value var v1 + var_appears_lambda var_appears_value var body
	| VIsEmpty (v1, body) -> var_appears_value var v1 + var_appears_value var body

and var_appears_constr var c = match c with
	| True -> 0
	| Conj (c1, c2) -> var_appears_constr var c1 + var_appears_constr var c2
	| Sub (v1, v2) -> var_appears_value var v1 + var_appears_lvalue var v2
	| CLet (v1, body) | Forall (v1, body) -> var_appears_value var v1 + var_appears_lambda var_appears_constr var body
	| CEmpty (v1, body) -> var_appears_value var v1 + var_appears_constr var body

let rec var_appears_dep var d = match d with
	| DBase l -> List.fold_left (+) 0 (List.map (fun (_, v) -> match v with Some v -> var_appears_value var v | _ -> 0) l)
	| DConj (d1, d2) -> var_appears_dep var d1 + var_appears_dep var d2
	| DLet (v1, body) | DForall (v1, body) -> var_appears_value var v1 + var_appears_lambda var_appears_dep var body
	| DEmpty (v1, body) -> var_appears_value var v1 + var_appears_dep var body


(* Compute the set of free variables *)
module SSet = Set.Make(String)
let fv_lambda fv (args, body) =
	List.fold_left (fun s v -> SSet.remove v s) (fv body) args

let rec fv_lvalue lv = match lv.desc with
	| LNamedVar _ -> SSet.empty
	| LMapGet (lv, v) -> SSet.union (fv_lvalue lv) (fv_value v)

and fv_value v = match v.desc with
	| NamedVar _ | VUnit -> SSet.empty
	| MapGet (v1, v2) -> SSet.union (fv_value v1) (fv_value v2)
	| LocalVar var1 -> SSet.singleton var1
	| VTuple l | VSet l | VList l | Func (_, l) ->
		List.fold_left SSet.union SSet.empty (List.map fv_value l)
	| VWith (c, v) ->
		SSet.union (fv_constr c) (fv_value v)
	| VLet (v1, body) | VJoin (v1, body) ->
		SSet.union (fv_value v1) (fv_lambda fv_value body)
	| VIsEmpty (v1, body) -> SSet.union (fv_value v1) (fv_value body)

and fv_constr c = match c with
	| True -> SSet.empty
	| Conj (c1, c2) -> SSet.union (fv_constr c1) (fv_constr c2)
	| Sub (v1, v2) -> SSet.union (fv_value v1) (fv_lvalue v2)
	| CLet (v1, body) | Forall (v1, body) -> SSet.union (fv_value v1) (fv_lambda fv_constr body)
	| CEmpty (v1, body) -> SSet.union (fv_value v1) (fv_constr body)

let rec fv_dep d = match d with
	| DBase l ->
		List.fold_left SSet.union SSet.empty
			(List.map (fun (_, v) -> match v with Some v -> fv_value v | None -> SSet.empty) l)
	| DConj (d1, d2) -> SSet.union (fv_dep d1) (fv_dep d2)
	| DLet (v1, body) | DForall (v1, body) ->
		SSet.union (fv_value v1) (fv_lambda fv_dep body)
	| DEmpty (v1, body) -> SSet.union (fv_value v1) (fv_dep body)


(*
	 Shallow with-lifting: transform a value into a value and a list of constraints, where the value does
	 not contain any [VWith], and thus has no side-effects.
	 The constraints can however still contain values with [VWith] in them, thus the lifting being shallow.
*)
let lift_with_lambda liftf (args, v) =
	let v, c = liftf v in
	((args, v), (List.map (fun c1 -> (args, c1)) c))

let rec lift_with_shallow_value v = match v.desc with
	| NamedVar _ | LocalVar _ | VUnit -> (v, [])
	| MapGet (v1, v2) ->
		let v1, c1 = lift_with_shallow_value v1 in
		let v2, c2 = lift_with_shallow_value v2 in
		({ desc = MapGet (v1, v2) ; typ = v.typ }, c1 @ c2)
	| VWith (c, v) ->
		let v, c1 = lift_with_shallow_value v in
		(v, c :: c1)
	| VTuple l ->
		let l, c = lift_with_shallow_value_list l in
		({ desc = VTuple l ; typ = v.typ }, c)
	| VSet l ->
		let l, c = lift_with_shallow_value_list l in
		({ desc = VSet l ; typ = v.typ }, c)
	| VList l ->
		let l, c = lift_with_shallow_value_list l in
		({ desc = VList l ; typ = v.typ }, c)
	| Func (f, l) ->
		let l, c = lift_with_shallow_value_list l in
		({ desc = Func (f, l) ; typ = v.typ }, c)
	| VLet (v1, body) ->
		let v1, c1 = lift_with_shallow_value v1 in
		let body, c2 = lift_with_lambda lift_with_shallow_value body in
		({ desc = VLet (v1, body) ; typ = v.typ }, c1 @ (List.map (fun c -> CLet (v1, c)) c2))
	| VJoin (v1, body) ->
		let v1, c1 = lift_with_shallow_value v1 in
		let body, c2 = lift_with_lambda lift_with_shallow_value body in
		({ desc = VJoin (v1, body) ; typ = v.typ }, c1 @ (List.map (fun c -> Forall (v1, c)) c2))
	| VIsEmpty (v1, body) ->
		let v1, c1 = lift_with_shallow_value v1 in
		let body, c2 = lift_with_shallow_value body in
		({ desc = VIsEmpty (v1, body) ; typ = v.typ}, c1 @ (List.map (fun c -> CEmpty (v1, c)) c1))

and lift_with_shallow_value_list l =
	let l = List.map lift_with_shallow_value l in
	(List.map fst l, List.flatten (List.map snd l))

(* With-filtering: like shallow with-lifting, but only returns the value part *)
let rec filter_with_value v = match v.desc with
	| NamedVar _ | LocalVar _ | VUnit -> v
	| MapGet (v1, v2) ->
		let v1 = filter_with_value v1 in
		let v2 = filter_with_value v2 in
		{ desc = MapGet (v1, v2) ; typ = v.typ }
	| VTuple l ->
		{ desc = VTuple (List.map filter_with_value l) ; typ = v.typ }
	| VSet l ->
		{ desc = VSet (List.map filter_with_value l) ; typ = v.typ }
	| VList l ->
		{ desc = VList (List.map filter_with_value l) ; typ = v.typ }
	| Func (f, l) ->
		{ desc = Func (f, List.map filter_with_value l) ; typ = v.typ }
	| VWith (_, v) -> filter_with_value v
	| VLet (v1, (args, body)) ->
		{ desc = VLet (filter_with_value v1, (args, filter_with_value body)) ; typ = v.typ }
	| VJoin (v1, (args, body)) ->
		{ desc = VJoin (filter_with_value v1, (args, filter_with_value body)) ; typ = v.typ }
	| VIsEmpty (v1, body) ->
		{ desc = VIsEmpty (filter_with_value v1, filter_with_value body) ; typ = v.typ }

(* Adds all the constraints in [c] to the value [v] using [VWith] *)
let make_vwith_list v c =
	match c with
	| [] -> v
	| c1 :: c -> { desc = VWith (List.fold_left (fun c1 c2 -> Conj (c1, c2)) c1 c, v) ; typ = v.typ }

(* Smart constructor for [VWith], which outputs the type as well *)
let make_with c v = { desc = VWith (c, v) ; typ = v.typ }

let make_tuple_type = function
	| [] -> BaseType "unit"
	| [t] -> t
	| l -> TupleType l

(* Smart constructor for [VTuple], which outputs the type as well *)
let make_tuple l =
	{ desc =
			(match l with
			 | [] -> VUnit
			 | [x] -> x.desc
			 | l -> VTuple l) ;
		typ = make_tuple_type (List.map (fun x -> x.typ) l)
	}

(* Smart constructor for [VList], which outputs the type as well. Only works for non-empty lists. *)
let make_list l =
	assert (l <> []);
	{ desc = VList l ; typ = ListType (List.hd l).typ }

(* ====== *)

let find_var_name fv n =
	fresh n

(*
	 Apply a substitution under a lambda, using [sf] as the substitution function for the body.
	 - [subst] is a map from variables to values giving the substitution
	 - [fv] contains a set of variables that must not be captured in the resulting term
*)
let subst_lambda sf subst fv (args, body) =
	let nargs, nsubst, nfv = List.fold_left (fun (nargs, subst, fv) arg ->
			if SSet.mem arg fv then
				let narg = find_var_name fv arg in
				(narg :: nargs, SMap.add arg (LocalVar narg) subst, SSet.add narg fv)
			else if SMap.mem arg subst then
				(arg :: nargs, SMap.remove arg subst, fv)
			else
				(arg :: nargs, subst, fv)
		) ([], subst, fv) (List.rev args)
	in
	(nargs, sf nsubst nfv body)

(* Alpha-convert a lambda to avoid the variables in [fv] *)
let alpha_convert sf fv (args, body) =
	subst_lambda sf SMap.empty fv (args, body)

(* Substitution function for values *)
let rec subst_value subst fv v = match v.desc with
	| NamedVar _ | VUnit -> v
	| LocalVar name ->
		{ desc = (try SMap.find name subst with Not_found -> v.desc) ; typ = v.typ }
	| MapGet (v1, v2) -> { desc = MapGet (subst_value subst fv v1, subst_value subst fv v2) ; typ = v.typ }
	| VWith (c, v1) -> { desc = VWith (subst_constr subst fv c, subst_value subst fv v1) ; typ = v.typ }
	| VTuple l -> { desc = VTuple (List.map (subst_value subst fv) l) ; typ = v.typ }
	| VSet l -> { desc = VSet (List.map (subst_value subst fv) l) ; typ = v.typ }
	| VList l -> { desc = VList (List.map (subst_value subst fv) l) ; typ = v.typ }
	| Func (f, l) -> { desc = Func (f, List.map (subst_value subst fv) l) ; typ = v.typ }
	| VLet (v1, body) ->
		make_vlet (subst_value subst fv v1) (subst_lambda subst_value subst fv body)
	| VJoin (v1, body) ->
		make_join (subst_value subst fv v1) (subst_lambda subst_value subst fv body)
	| VIsEmpty (v1, body) ->
		make_visempty (subst_value subst fv v1) (subst_value subst fv body)

(* Substitution function for constraints *)
and subst_constr subst fv c = match c with
	| True -> True
	| Conj (c1, c2) -> Conj (subst_constr subst fv c1, subst_constr subst fv c2)
	| CLet (v1, body) -> make_clet (subst_value subst fv v1) (subst_lambda subst_constr subst fv body)
	| Forall (v1, body) -> Forall (subst_value subst fv v1, subst_lambda subst_constr subst fv body)
	| Sub (v1, v2) -> Sub (subst_value subst fv v1, subst_lvalue subst fv v2)
	| CEmpty (v1, body) -> CEmpty (subst_value subst fv v1, subst_constr subst fv body)

(* Substitution function for lvalues *)
and subst_lvalue subst fv lv = match lv.desc with
	| LNamedVar _ -> lv
	| LMapGet (lv1, v2) -> { desc = LMapGet (subst_lvalue subst fv lv1, subst_value subst fv v2) ; typ = lv.typ }


(* [VLet] smart constructor *)
and make_vlet v (args, body) =
	match v.desc with
	| VTuple l when List.length args > 1 ->
		(* Optimize "let (x1, ..., xn) = (v1, ..., vn) in ..." to
			 "let _x1 = v1 in ... let _xn = vn in let x1 = _x1 in ... let xn = _xn in ..." *)
		assert (List.length l = List.length args);
		let args = List.map (fun x -> (x, fresh x)) args in
		let body =
			List.fold_left2 (fun body (arg, narg) av ->
					make_vlet ({ desc = LocalVar narg ; typ = av.typ }) ([arg], body)
				) body args l
		in
		let body =
			List.fold_left2 (fun body (arg, narg) av ->
					make_vlet av ([narg], body)) body args l
		in
		body
	| LocalVar v1 when List.length args = 1 ->
		(* Optimize "let x = y in body" to "body[x |-> y]" *)
		let arg = List.hd args in
		subst_value (SMap.singleton arg (LocalVar v1)) (SSet.singleton v1) body
	| _ ->
		if List.length args = 1 && var_appears_value (List.hd args) body = 1 then
			(* Inline a variable which only appears once *)
			let v, c = lift_with_shallow_value v in
			make_vwith_list (subst_value (SMap.singleton (List.hd args) v.desc) (fv_value v) body) c
		else if List.for_all (fun var -> var_appears_value var body = 0) args then
			(* Remove an unused let-binding *)
			let _, c = lift_with_shallow_value v in
			make_vwith_list body c
		else
			{ desc = VLet (v, (args, body)) ; typ = body.typ }

and make_clet v (args, body) =
	match v.desc with
	| VTuple l when List.length args > 1 ->
		(* Optimize "let (x1, ..., xn) = (v1, ..., vn) in ..." to
			 "let _x1 = v1 in ... let _xn = vn in let x1 = _x1 in ... let xn = _xn in ..." *)
		assert (List.length l = List.length args);
		let args = List.map (fun x -> (x, fresh x)) args in
		let body =
			List.fold_left2 (fun body (arg, narg) av ->
					make_clet ({ desc = LocalVar narg ; typ = av.typ }) ([arg], body)
				) body args l
		in
		let body =
			List.fold_left2 (fun body (arg, narg) av ->
					make_clet av ([narg], body)) body args l
		in
		body
	| LocalVar v1 when List.length args = 1 ->
		(* Optimize "let x = y in body" to "body[x |-> y]" *)
		let arg = List.hd args in
		subst_constr (SMap.singleton arg (LocalVar v1)) (SSet.singleton v1) body
	| _ ->
		if List.length args = 1 && var_appears_constr (List.hd args) body = 1 then
			(* Inline a variable which only appears once *)
			let v, c = lift_with_shallow_value v in
			List.fold_left (fun c1 c2 -> Conj (c1, c2))
				(subst_constr (SMap.singleton (List.hd args) v.desc) (fv_value v) body) c
		else if List.for_all (fun var -> var_appears_constr var body = 0) args then
			(* Remove an unused let-binding *)
			let _, c = lift_with_shallow_value v in
			List.fold_left (fun c1 c2 -> Conj (c1, c2)) body c
		else
			CLet (v, (args, body))

(* Checks whether [v] can never be empty *)
and never_empty v = match v.desc with
	| LocalVar _ | NamedVar _ | VUnit | VTuple _ | Func _ | MapGet _ -> false
	| VWith (_, v) -> never_empty v
	| VLet (_, (_, body)) -> never_empty body
	| VJoin (v1, (_, body)) | VIsEmpty (v1, body) -> never_empty v1 && never_empty body
	| VSet l | VList l -> l <> []

(* Simplifies the condition for which [v] will be empty or not *)
and not_empty_cond v = match v.desc with
	| LocalVar _ | NamedVar _ | VUnit | VTuple _ | Func _ | MapGet _ -> v
	| VWith (c, v1) -> make_with c (not_empty_cond v1)
	| VLet (v1, (args, body)) -> make_vlet v1 (args, not_empty_cond body)
	| VJoin (v1, (args, body)) ->
		if never_empty body then not_empty_cond v1 else v
	| VIsEmpty (v1, body) ->
		if never_empty body then not_empty_cond v1 else v
	| VSet l | VList l -> v

(* Checks whether [v] is always empty *)
and always_empty v = match v.desc with
	| LocalVar _ | NamedVar _ | VUnit | VTuple _ | Func _ | MapGet _ -> false
	| VWith (_, v) -> always_empty v
	| VLet (_, (_, body)) -> always_empty body
	| VJoin (v1, (_, body)) | VIsEmpty (v1, body) -> always_empty v1 || always_empty body
	| VSet l | VList l -> l = []


(*
	 Assuming [v] produces an expression of type "t_1 * ... * t_n" and [fields] is a list [i_1; ...; i_k],
	 either produce [None] or [Some v1] where [v1] is an expression of type "t_{i_1} * ... * t_{i_k}" selecting the
	 corresponding fields of [v].
*)
and try_select_fields fields v = match v.desc with
	| LocalVar _ | NamedVar _ | VUnit | Func _ | MapGet _ | VJoin _ | VSet _ | VList _ | VIsEmpty _ -> None
	| VWith (c, v) ->
		(match try_select_fields fields v with
		 | None -> None
		 | Some v -> Some (make_with c v))
	| VLet (v1, (args, body)) ->
		(match try_select_fields fields body with
		 | None -> None
		 | Some body -> Some (make_vlet v1 (args, body)))
	| VTuple l ->
		let _, _, nl, cs = List.fold_left (fun (i, fds, nl, cs) vi ->
				match fds with
				| j :: fds when i = j -> (i + 1, fds, vi :: nl, cs)
				| _ -> (i + 1, fds, nl, snd (lift_with_shallow_value vi) @ cs)
			) (0, fields, [], []) l
		in
		let nl = List.rev nl in
		Some (make_vwith_list (make_tuple nl) cs)

and try_select_fields_type fields t =
	assert (fields <> []);
	let l = (match t with TupleType l -> l | _ -> [t]) in
	let _, _, nl = List.fold_left (fun (i, fds, nl) ti ->
			match fds with
			| j :: fds when i = j -> (i + 1, fds, ti :: nl)
			| _ -> (i + 1, fds, nl)
		) (0, fields, []) l in
	let nl = List.rev nl in
	match nl with
	| [] -> assert false
	| [t] -> t
	| _ -> TupleType nl

and try_select_fields_type_in fields = function
	| ListType t -> ListType (try_select_fields_type fields t)
	| SetType t -> SetType (try_select_fields_type fields t)
	| _ -> assert false

(*
	 Assuming [v] produces an expression of type "(t_1 * ... * t_n) (list|set)" and [fields] is a list [i_1; ...; i_k],
	 either produce [None] or [Some v1] where [v1] is an expression of type
	 "(t_{i_1} * ... * t_{i_k}) (list|set)" selecting the corresponding fields of each value of [v].
	 If [is_set] is [true], then duplicates may be removed by the projection; if it is false, duplicates will remain
	 and set expressions will not be simplified.
*)
and try_select_fields_in is_set fields v = match v.desc with
	| LocalVar _ | NamedVar _ | VUnit | VTuple _ | Func _ | MapGet _ -> None
	| VWith (c, v) ->
		(match try_select_fields_in is_set fields v with
		 | None -> None
		 | Some v -> Some (make_with c v))
	| VLet (v1, (args, body)) ->
		(match try_select_fields_in is_set fields body with
		 | None -> None
		 | Some body -> Some (make_vlet v1 (args, body)))
	| VJoin (v1, (args, body)) ->
		(match try_select_fields_in is_set fields body with
		 | None -> None
		 | Some body -> Some (make_join_gen is_set v1 (args, body)))
	| VIsEmpty (v1, body) ->
		(match try_select_fields_in is_set fields body with
		 | None -> None
		 | Some body -> Some (make_visempty v1 body)
		)
	| VSet l ->
		if not is_set then None else begin
			let l = List.map (try_select_fields fields) l in
			if List.for_all (fun x -> x <> None) l then
				let l = List.map (function None -> assert false | Some x -> x) l in
				Some { desc = VSet l ; typ = try_select_fields_type_in fields v.typ }
			else
				None
		end
	| VList l ->
		let l = List.map (try_select_fields fields) l in
		if List.for_all (fun x -> x <> None) l then
			let l = List.map (function None -> assert false | Some x -> x) l in
			Some { desc = VList l ; typ = try_select_fields_type_in fields v.typ }
		else
			None

(* Smart constructor for [VJoin]. Is simplifies joins over empty or singleton sets and lists to avoid joins. *)
and make_join1 is_set v1 (args, v2) =
	match v1.desc with
	| VList [v] -> make_vlet v (args, v2)
	| VSet [v] when is_set -> make_vlet v (args, v2)
	| VList [] | VSet [] ->
		{ desc = (match v2.typ with ListType _ -> VList [] | SetType _ -> VSet [] | _ -> VJoin (v1, (args, v2))) ;
			typ = v2.typ }
	| _ -> { desc = VJoin (v1, (args, v2)) ; typ = v2.typ }

(*
	 Smart constructor for [VJoin]. It tries to remove unused variables, then calls [make_join1] for
	 further simplifications.
*)
and make_join_gen is_set v1 (args, v2) =
	let ar = List.mapi (fun i x -> (i, x)) args in
	let fields1 = List.filter (fun (_, x) -> var_appears_value x v2 > 0) ar in
	if fields1 = [] then
		make_visempty v1 v2
	else if List.length args = 1 then
		make_join1 is_set v1 (args, v2)
	else
		match try_select_fields_in is_set (List.map fst fields1) v1 with
		| None -> make_join1 is_set v1 (args, v2)
		| Some v1 -> make_join1 is_set v1 (List.map snd fields1, v2)

(*
	 Smart constructor for [VJoin], which fixes [is_set] to [false] so that sets are not overly simplified,
	 since the join operation is not usually idempotent (due to lists).
*)
and make_join v1 v2 = make_join_gen false v1 v2

(* Smart constructor for [VIsEmpty], that simplifies expressions it knows are never empty *)
and make_visempty v1 v2 =
	if never_empty v1 then v2 else { desc = VIsEmpty (not_empty_cond v1, v2) ; typ = v2.typ }

(* ====== *)

(*
	 Deep with-lifting, removing [VWith] constructors from everything it returns.
	 It also transforms constraint conjunction into a list of constraints.
*)
let rec lift_with_lvalue lv = match lv.desc with
	| LNamedVar _ -> (lv, [])
	| LMapGet (lv1, v1) ->
		let lv1, c1 = lift_with_lvalue lv1 in
		let v1, c2 = lift_with_value v1 in
		({ desc = LMapGet (lv1, v1) ; typ = lv.typ }, c1 @ c2)

and lift_with_value v = match v.desc with
	| NamedVar _ | LocalVar _ | VUnit -> (v, [])
	| MapGet (v1, v2) ->
		let v1, c1 = lift_with_value v1 in
		let v2, c2 = lift_with_value v2 in
		({ desc = MapGet (v1, v2) ; typ = v.typ }, c1 @ c2)
	| VWith (c, v) ->
		let v, c1 = lift_with_value v in
		(v, lift_with_constr c @ c1)
	| VTuple l ->
		let l, c = lift_with_value_list l in
		({ desc = VTuple l ; typ = v.typ }, c)
	| VSet l ->
		let l, c = lift_with_value_list l in
		({ desc = VSet l ; typ = v.typ }, c)
	| VList l ->
		let l, c = lift_with_value_list l in
		({ desc = VList l ; typ = v.typ }, c)
	| Func (f, l) ->
		let l, c = lift_with_value_list l in
		({ desc = Func (f, l) ; typ = v.typ }, c)
	| VLet (v1, body) ->
		let v1, c1 = lift_with_value v1 in
		let body, c2 = lift_with_lambda lift_with_value body in
		(make_vlet v1 body, c1 @ (List.map (fun c -> make_clet v1 c) c2))
	| VJoin (v1, body) ->
		let v1, c1 = lift_with_value v1 in
		let body, c2 = lift_with_lambda lift_with_value body in
		(make_join v1 body, c1 @ (List.map (fun c -> Forall (v1, c)) c2))
	| VIsEmpty (v1, body) ->
		let v1, c1 = lift_with_value v1 in
		let body, c2 = lift_with_value body in
		(make_visempty v1 body, c1 @ (List.map (fun c -> CEmpty (v1, c)) c2))

and lift_with_value_list l =
	let l = List.map lift_with_value l in
	(List.map fst l, List.flatten (List.map snd l))

and lift_with_constr c = match c with
	| True -> []
	| Conj (c1, c2) ->
		let cl1 = lift_with_constr c1 in
		let cl2 = lift_with_constr c2 in
		cl1 @ cl2
	| Sub (v1, v2) ->
		let v1, cl1 = lift_with_value v1 in
		let v2, cl2 = lift_with_lvalue v2 in
		Sub (v1, v2) :: cl1 @ cl2
	| Forall (v, (args, c2)) ->
		let v, cl1 = lift_with_value v in
		let cl2 = lift_with_constr c2 in
		cl1 @ List.map (fun c -> Forall (v, (args, c))) cl2
	| CLet (v, (args, c2)) ->
		let v, cl1 = lift_with_value v in
		let cl2 = lift_with_constr c2 in
		cl1 @ List.map (fun c -> make_clet v (args, c)) cl2
	| CEmpty (v, body) ->
		let v, cl1 = lift_with_value v in
		let cl2 = lift_with_constr body in
		cl1 @ List.map (fun c -> CEmpty (v, body)) cl2


(* Smart constructor for conjunction of dependency expressions *)
let make_dconj x y =
	match x, y with
	| DBase l1, DBase l2 -> DBase (l1 @ l2)
	| DBase [], _ -> y
	| _, DBase [] -> x
	| _ -> DConj (x, y)

(*
	 Controls whether [DEmpty(v, d)] should be over-approximated as [d].
*)
let over_approx_dep_empty = true

(* Substitution function for dependency expressions *)
let rec subst_dep subst fv d = match d with
	| DBase l -> DBase (List.map (fun (name, v) -> match v with None -> (name, v) | Some v -> (name, Some (subst_value subst fv v))) l)
	| DConj (d1, d2) -> DConj (subst_dep subst fv d1, subst_dep subst fv d2)
	| DLet (v, body) -> make_dlet (subst_value subst fv v) (subst_lambda subst_dep subst fv body)
	| DForall (v, body) -> make_dforall (subst_value subst fv v) (subst_lambda subst_dep subst fv body)
	| DEmpty (v, body) -> make_dempty (subst_value subst fv v) (subst_dep subst fv body)

(* Smart constructor for [DLet] *)
and make_dlet v (args, d) =
	match v.desc with
	| VTuple l when List.length args > 1 ->
		(* Optimize "let (x1, ..., xn) = (v1, ..., vn) in ..." to
			 "let _x1 = v1 in ... let _xn = vn in let x1 = _x1 in ... let xn = _xn in ..." *)
		assert (List.length l = List.length args);
		let args = List.map (fun x -> (x, fresh x)) args in
		let d =
			List.fold_left2 (fun d (arg, narg) av ->
					make_dlet ({ desc = LocalVar narg ; typ = av.typ }) ([arg], d)
				) d args l
		in
		let d =
			List.fold_left2 (fun d (arg, narg) av ->
					make_dlet av ([narg], d)) d args l
		in
		d
	| _ ->
		if List.length args = 1 && var_appears_dep (List.hd args) d = 1 then
			(* Inline a variable which only appears once *)
			subst_dep (SMap.singleton (List.hd args) v.desc) (fv_value v) d
		else if List.for_all (fun var -> var_appears_dep var d = 0) args then
			(* Remove an unused let-binding *)
			d
		else
			DLet (v, (args, d))

(* Optimize foralls over zero or one elements, or a join *)
and make_dforall1 v1 (args, d) =
	match v1.desc with
	| VList [v] | VSet [v] -> make_dlet v (args, d)
	| VList [] | VSet [] -> DBase []
	| VJoin (v2, (args2, body)) ->
		let args2, body = alpha_convert subst_value (fv_dep d) (args2, body) in
		make_dforall v2 (args2, make_dforall body (args, d))
	| _ -> DForall (v1, (args, d))

(* Try to remove useless variables in a forall *)
and make_dforall v (args, d) =
	if d = DBase [] then DBase [] else begin
		let ar = List.mapi (fun i x -> (i, x)) args in
		let fields1 = List.filter (fun (_, x) -> var_appears_dep x d > 0) ar in
		if fields1 = [] then
			make_dempty v d
		else if List.length args = 1 then
			make_dforall1 v (args, d)
		else
			match try_select_fields_in true (List.map fst fields1) v with
			| None -> make_dforall1 v (args, d)
			| Some v1 -> make_dforall1 v1 (List.map snd fields1, d)
	end

(* Creates a [DEmpty] node. Tries to simplify it, but can be over-approximated to [d]. *)
and make_dempty v d =
	if never_empty v then
		d
	else if always_empty v then
		DBase []
	else if over_approx_dep_empty then
		d
	else match v.desc with
		| VJoin (v2, (args2, body)) ->
			let args2, body = alpha_convert subst_value (fv_dep d) (args2, body) in
			make_dforall v2 (args2, make_dempty body d)
		| _ -> DEmpty (not_empty_cond v, d)


(* Computes the variables read by a lvalue *)
let rec deps_lvalue_in lv = match lv.desc with
	| LNamedVar x -> DBase []
	| LMapGet (lv, v) ->
		make_dconj (deps_lvalue_in lv) (deps_value_in v)

(* Computes the variables read by a value *)
and deps_value_in v = match v.desc with
	| NamedVar x -> DBase [(x, None)]
	| MapGet ({ desc = NamedVar x }, v) ->
		make_dconj (deps_value_in v) (DBase [(x, Some v)])
	| MapGet (v1, v2) ->
		make_dconj (deps_value_in v1) (deps_value_in v2)
	| VUnit -> DBase []
	| LocalVar _ -> DBase []
	| VSet l | VList l | Func (_, l) | VTuple l ->
		List.fold_left (fun acc v -> make_dconj acc (deps_value_in v)) (DBase []) l
	| VWith (c, v) ->
		make_dconj (deps_constr_in c) (deps_value_in v)
	| VLet (v, (args, body)) ->
		let d = deps_value_in v in
		make_dconj d (make_dlet (filter_with_value v) (args, deps_value_in body))
	| VJoin (v, (args, body)) ->
		let d = deps_value_in v in
		make_dconj d (make_dforall (filter_with_value v) (args, deps_value_in body))
	| VIsEmpty (v, body) ->
		let d = deps_value_in v in
		make_dconj d (make_dempty (filter_with_value v) (deps_value_in body))

(* Computes the variables read by a constraint *)
and deps_constr_in c = match c with
	| True -> DBase []
	| Conj (c1, c2) -> make_dconj (deps_constr_in c1) (deps_constr_in c2)
	| Sub (v1, v2) ->
		let d1 = deps_value_in v1 in
		let d2 = deps_lvalue_in v2 in
		make_dconj d1 d2
	| Forall (v, (args, c2)) ->
		let d = deps_value_in v in
		make_dconj d (make_dforall (filter_with_value v) (args, deps_constr_in c2))
	| CLet (v, (args, c2)) ->
		let d = deps_value_in v in
		make_dconj d (make_dlet (filter_with_value v) (args, deps_constr_in c2))
	| CEmpty (v, c2) ->
		let d = deps_value_in v in
		make_dconj d (make_dempty (filter_with_value v) (deps_constr_in c2))

(* Computes the variables written to by a lvalue *)
let rec deps_lvalue_out lv = match lv.desc with
	| LNamedVar x -> DBase [(x, None)]
	| LMapGet ({ desc = LNamedVar x }, v) -> make_dconj (DBase [(x, Some v)]) (deps_value_out v)
	| LMapGet (lv, v) ->
		make_dconj (deps_lvalue_out lv) (deps_value_out v)

(* Computes the variables written to by a value *)
and deps_value_out v = match v.desc with
	| NamedVar x -> DBase []
	| MapGet (v1, v2) -> make_dconj (deps_value_out v1) (deps_value_out v2)
	| VUnit -> DBase []
	| LocalVar _ -> DBase []
	| VSet l | VList l | Func (_, l) | VTuple l ->
		List.fold_left (fun acc v -> make_dconj acc (deps_value_out v)) (DBase []) l
	| VWith (c, v) ->
		make_dconj (deps_constr_out c) (deps_value_out v)
	| VLet (v, (args, body)) ->
		let d = deps_value_out v in
		make_dconj d (make_dlet (filter_with_value v) (args, deps_value_out body))
	| VJoin (v, (args, body)) ->
		let d = deps_value_out v in
		make_dconj d (make_dforall (filter_with_value v) (args, deps_value_out body))
	| VIsEmpty (v, body) ->
		let d = deps_value_out v in
		make_dconj d (make_dempty (filter_with_value v) (deps_value_out body))

(* Computes the variables written to by a constraint *)
and deps_constr_out c = match c with
	| True -> DBase []
	| Conj (c1, c2) -> make_dconj (deps_constr_out c1) (deps_constr_out c2)
	| Sub (v1, v2) ->
		let d1 = deps_value_out v1 in
		let d2 = deps_lvalue_out v2 in
		make_dconj d1 d2
	| Forall (v, (args, c2)) ->
		let d = deps_value_out v in
		make_dconj d (make_dforall (filter_with_value v) (args, deps_constr_out c2))
	| CLet (v, (args, c2)) ->
		let d = deps_value_out v in
		make_dconj d (make_dlet (filter_with_value v) (args, deps_constr_out c2))
	| CEmpty (v, body) ->
		let d = deps_value_out v in
		make_dconj d (make_dempty (filter_with_value v) (deps_constr_out body))

(* Computes the variables than can change a dependency expression *)
let rec deps_deps d = match d with
	| DConj (d1, d2) -> make_dconj (deps_deps d1) (deps_deps d2)
	| DLet (v, (args, body)) ->
		make_dconj (deps_value_in v) (make_dlet (filter_with_value v) (args, deps_deps body))
	| DForall (v, (args, body)) ->
		make_dconj (deps_value_in v) (make_dforall (filter_with_value v) (args, deps_deps body))
	| DEmpty (v, body) ->
		make_dconj (deps_value_in v) (make_dempty (filter_with_value v) (deps_deps body))
	| DBase l ->
		List.fold_left make_dconj (DBase []) (List.map (function _, None -> DBase [] | _, Some v -> deps_value_in v) l)

