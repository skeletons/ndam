%{
	open Pskeltypes
	let ghost_loc = (Lexing.dummy_pos, Lexing.dummy_pos)

	let split_decl l =
		let rec aux (a, b, c, d) = function
			| [] -> (a, b, c, d)
			| PAbstractType (s, n) :: l ->
					aux ((s, n) :: a, b, c, d) l
			| PVariantType (name, n, consts) :: l ->
					aux (a, (name, n, consts) :: b, c, d) l
			| PTerm (name, ta, ty, t) :: l ->
					aux (a, b, (name, ta, ty, t) :: c, d) l
			| PAlias (name, ta, ty) :: l ->
					aux (a, b, c, (name, ta, ty) :: d) l
		in
		let (abs, var, terms, aliases) =
			aux ([], [], [], []) (List.rev l)
		in
		{
			pabs_type = abs ;
			pvar_type = var ;
			pterms = terms ;
			palias = aliases
		}

	let tt loc = {loc; contents=PTTuple []}
	let tt_pat loc = {loc; contents=PPTuple []}

	(* Get a fresh identifier for a created variable, to be then replaced by a
	 * fresh real name *)
	let tmp_number = ref 0
	let get_tmp () =
		let () = incr tmp_number in (" " ^ string_of_int !tmp_number)


	let lambda (t, ty, s) =
		begin match t.contents with
		| PPVar x -> PTFunc (x, ty, s)
		| _ -> let var = get_tmp () in
				let skel = PLetIn (None, t, {contents=PReturn ({loc=ghost_loc;contents=PTVar (var, [])});loc=t.loc}, s) in
				PTFunc (var, ty, {contents=skel;loc=s.loc})
		end

	(* Get the matching term of arrow type *)
	let pterm (n, ta, (args, ty), s) =
		let args = List.rev args in
		begin match args with
		| [] ->
				begin match s with
				| Some {contents=PReturn t; loc} -> PTerm (n, ta, ty, Some t)
				| None -> PTerm (n, ta, ty, None)
				| _ -> raise Parsing.Parse_error
				end
		| _ when s = None -> raise Parsing.Parse_error
		| (n_arg, ty_arg) :: q ->
				let initial_skel =
					{contents=lambda (n_arg, ty_arg, Option.get s);
					loc=(Option.get s).loc} in
				let initial_ty = {contents=PArrow (ty_arg, ty);loc=(fst ty_arg.loc,
				snd ty.loc)} in
				let (term, type_out) =
					List.fold_left (fun (s,s_ty) (x,ty) ->
					let loc = (fst x.loc, snd s.loc) in
					let inner_skel ={contents=PReturn s;loc=s.loc} in
					let s_ty_2 = PArrow (ty, s_ty) in
					let s_ty_loc = {contents=s_ty_2; loc=(fst ty.loc, snd s_ty.loc)} in
					({contents=(lambda (x, ty, inner_skel));loc},s_ty_loc))
					(initial_skel, initial_ty) q
				in
				PTerm (n, ta, type_out, Some term)
		end

	let make_func (f, args, body, next) =
		let actual_body =
			List.fold_right (fun (var, ty) body ->
				let loc = (fst var.loc,snd body.loc) in
				{contents= PReturn {contents=PTFunc (var.contents, ty, body); loc}; loc})
				 args body
		in
		PLetIn(None, {contents=PPVar f.contents;loc=f.loc}, actual_body, next)

%}

(** * Definitions **)

(** ** Keywords **)

%token BRANCH
%token END
%token OR
%token TYPE

%token IN
%token LET
%token TERM

(* Keywords used by the constraint generation. *)
%token LAMBDA


(** ** Identifiers **)
%token <string> LIDENT
%token <string> UIDENT

(** ** Operators **)
%token PERCENT
%token BAR
%token COLON
%token COLONEQ
%token COMMA
%token EQUAL
%token MINUSGREATER
%token SEMI
%token UNDERSCORE

(** ** Paired delimiters **)
%token LPAREN
%token RPAREN
%token LESS
%token GREATER

(** ** Other **)
%token EOF

%start main
%type <Pskeltypes.pdecls * int> main
%start term_decls
%type <Pskeltypes.pterm Pskeltypes.with_loc list> term_decls

(* Precedence *)
%nonassoc lambda_prec
%nonassoc let_prec
%right SEMI
%right MINUSGREATER


%% (** Rules **)


(** auxiliary symbols **)

let empty == {}

let located(x) ==
	~ = x ; { { contents = x ; loc = $loc } }

let separated_list2(sep, X) :=
	x = X ; sep ;
	l = separated_nonempty_list(sep, X) ;
	{ x :: l }

let list2(X) ==
	x = X ; l = X+ ; { x :: l }

(* (X) *)
let parenthesized(X) ==
	delimited(LPAREN, X, RPAREN)

(* <X> *)
let chevronned(X) ==
	delimited(LESS, X, GREATER)

(* (X, …, X) *)
let tuple(X) == parenthesized(separated_list(COMMA, X))

(* () *)
let empty_tuple == parenthesized(empty)

let nonempty_tuple(X) == parenthesized(separated_nonempty_list(COMMA, X))

let tuple2(X) == parenthesized(separated_list2(COMMA, X))

(* (x) is not a valid tuple *)
let valid_tuple(X) ==
| empty_tuple ;
	{ [] }
| tuple2(X)

(** Useful nonterminal symbols **)
let type_arg :=
| LIDENT

let poly_arity :=
	| l = chevronned(separated_list(COMMA, UNDERSCORE)); { List.length l  }
	| empty; { 0 }

let type_annot(X) ==
	| empty; { [] }
	| chevronned(separated_nonempty_list(COMMA,X))



(** Main definitions **)
let main :=
	decls = terminated(decl*, EOF) ;
	{ (split_decl decls, !tmp_number) }

let ntype :=
| simple_type
| s1 = located(ntype) ; MINUSGREATER ; s2 = located(ntype); < PArrow >

let simple_type :=
| parenthesized(ntype)
| ~ = LIDENT ; ~ = type_annot(located(ntype)) ; < PType >
| ~ = valid_tuple(located(ntype)) ; < Prod >

let decl :=
| preceded(TYPE, type_decl)
| preceded(TERM, term_decl)
| preceded(TYPE, alias_decl)

let term_decl :=
	n = LIDENT ;
	ta = type_annot(type_arg) ;
	COLON ;
	ty = term_type_decl ;
	t = ioption(EQUAL ; located(skeleton)) ;
	< pterm >

let term_type_decl :=
| ty = located(ntype) ; { ([], ty) }
| LPAREN;
		n_arg = located(LIDENT) ;
	COLON ;
		ty_arg = located(ntype) ;
	RPAREN ;
	MINUSGREATER ;
	(l, ty) = term_type_decl ;
	{ (({contents=PPVar n_arg.contents;loc=n_arg.loc}, ty_arg) :: l, ty) }

let type_decl :=
| ~ = LIDENT ; ~ = poly_arity ;
	< PAbstractType >
| ~ = LIDENT ; ~ = type_annot(LIDENT) ;
	EQUAL ; ~ = preceded(BAR, constr_decl)+ ;
	< PVariantType >

let alias_decl :=
| ~ = LIDENT ;
	~ = type_annot(LIDENT) ;
	COLONEQ ;
	~ = located(ntype) ;
	< PAlias >

let constr_decl :=
	c  = located(UIDENT) ;
	a  = located(ntype)? ;
	{ (c, Option.value ~default:({loc=ghost_loc; contents=Prod []}) a) }

let skeleton :=
| LET ;
	~ = located(LIDENT) ;
	~ = nonempty_list(
		LPAREN ;
		~ = located(LIDENT) ;
		COLON ;
		~ = located(ntype) ;
		RPAREN ;
		< >
	) ;
	EQUAL ;
	~ = located(skeleton) ;
	IN ;
	~ = located(skeleton) ;
	< make_func > %prec let_prec
| LET ; ~ = ioption(monadic_annotation) ;
	~ = located(pattern); EQUAL ;
	~ = located(skeleton) ;
	IN ;
	~ = located(skeleton) ;
	< PLetIn > %prec let_prec
| b = located(skeleton) ;
	c = located(SEMI) ; a = option(monadic_annotation) ;
	s = located(skeleton) ;
	{ PLetIn (a, {contents=PPWild; loc=c.loc}, b, s) }
| ~ = delimited(
			BRANCH,
				separated_list(OR, located(skeleton)),
			END) ;
	< PBranching >
| ~ = located(term) ;
	< PReturn >
| f = located(LIDENT) ; ta = type_annot(located(ntype)); arg = located(simple_term)+ ;
	< PApply >


let monadic_annotation ==
		PERCENT;
		b = located(LIDENT);
		ta = type_annot(located(ntype));
		{(b, ta)}

let simple_pattern ==
| parenthesized(pattern)
| t = located(UIDENT) ;
	{ PPConstr (t, (tt_pat t.loc)) }
| ~ = LIDENT ;
	< PPVar >
| UNDERSCORE ; { PPWild }
| ~ = valid_tuple(located(pattern)) ;
	< PPTuple >

let pattern :=
| simple_pattern
| c = located(UIDENT) ;
	t = located(simple_pattern) ;
	{ PPConstr (c, t) }

let simple_term :=
| parenthesized(term)
| t = located(UIDENT) ; ta = type_annot(located(ntype)) ;
	{ PTConstr (t, ta, (tt t.loc)) }
| ~ = LIDENT ; ~ = type_annot(located(ntype)) ;
	< PTVar >
| (t, ty) = parenthesized(separated_pair(located(term), COLON, located(ntype))) ;
	{ PTType (ty, t)}
| ~ = valid_tuple(located(term)) ;
	< PTTuple >
(* λ (t : ty) → S *)

let term :=
| simple_term
| ~ = located(UIDENT) ; ~ = type_annot(located(ntype)) ;
	~ = located(simple_term) ;
	< PTConstr >
| LAMBDA ;
	t = located(pattern) ;
	COLON ;
	ty = located(simple_type) ;
	MINUSGREATER ;
	s = located(skeleton) ;
	{ lambda (t, ty, s)} %prec lambda_prec

(** * Term Declarations **)

let term_decl2 := preceded(TERM, located(term))

let term_decls := terminated(term_decl2*, EOF)

