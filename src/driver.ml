let global_filename = ref ""

let report_error filename start_pos end_pos =
	let open Lexing in
	let start_col = start_pos.pos_cnum - start_pos.pos_bol + 1 in
	let end_col = end_pos.pos_cnum - start_pos.pos_bol + 1 in
	Format.eprintf "File %S, line %d, characters %d-%d:@." filename start_pos.pos_lnum start_col end_col

let report_warning location =
	let (start_pos, end_pos) = location in
	report_error !global_filename start_pos end_pos

let parse_from_file parsefunc filename =
	let oc = open_in filename in
	let lexbuf = Lexing.from_channel oc in
	let r = try
			parsefunc Lexer.token lexbuf
		with
			(Parser.Error | Lexer.Lexing_error _) as e ->
			let errs = match e with Lexer.Lexing_error s -> s | _ -> "Syntax error." in
			report_error filename (Lexing.lexeme_start_p lexbuf) (Lexing.lexeme_end_p lexbuf);
			Format.eprintf "%s@." errs; exit 1
	in
	close_in oc;
	r

let typing filename f arg =
		global_filename := filename;
		f arg

let parse_and_type filename =
	let pdecls = parse_from_file Parser.main filename in
	typing filename Typer.type_program infile pdecls


