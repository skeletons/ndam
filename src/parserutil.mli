(** Useful functions to help lexing and parsing **)

val getlines: in_channel -> string

(** Given a filename, a error start and an error end, prints an error message. **)
val report_error : string -> Lexing.position -> Lexing.position -> unit

val parse_from_file :
  ((Lexing.lexbuf -> Parser.token) -> Lexing.lexbuf -> 'a) -> string -> 'a

