{
  open Parser
  open Lexing

  exception Lexing_error of string

  let kw = [
    "branch", BRANCH;
    "or", OR;
    "end", END;

    "let", LET;
    "in", IN;

    "term", TERM;
    "type", TYPE;
  ]

  let keywords = Hashtbl.create (List.length kw)
  let () = List.iter (fun (a, b) -> Hashtbl.add keywords a b) kw

  let is_keyword str = Hashtbl.mem keywords str

  let newline lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <- { pos with pos_lnum = pos.pos_lnum + 1 ; pos_bol = pos.pos_cnum }
}

let alpha = ['a'-'z'] | ['A'-'Z']
let ident_car = alpha | '_' | '\'' | ['0'-'9']
let lident = ( ['a'-'z'] ident_car* ) | ( '_' ident_car+ )
let uident = ['A'-'Z'] ident_car*
let whitespace = [' ' '\t']
let newline = "\n" | "\r\n" | "\r"

rule token = parse
  | whitespace+ { token lexbuf }
  | "(*"        { comment lexbuf; token lexbuf }
  | newline     { newline lexbuf; token lexbuf }
  (* Operators *)
  | "|"         { BAR }
  | ":"         { COLON }
  | ","         { COMMA }
  | "="         { EQUAL }
  | ":="        { COLONEQ }
  | "->" | "→"  { MINUSGREATER }
  | "\\" | "λ"  { LAMBDA }
  | ";"         { SEMI }
  | "%"         { PERCENT }
  (* Paired delimiters *)
  | "("         { LPAREN }
  | ")"         { RPAREN }
  | "<"         { LESS }
  | ">"         { GREATER }
  | "_"         { UNDERSCORE }
  | lident as s { try Hashtbl.find keywords s with Not_found -> LIDENT s }
  | uident as s { try Hashtbl.find keywords s with Not_found -> UIDENT s }
  | _           { raise (Lexing_error "Invalid character") }
  | eof         { EOF }

and comment = parse
  | "\n" { newline lexbuf; comment lexbuf }
  | "*)" { () }
  | "(*" { comment lexbuf; comment lexbuf }
  | _    { comment lexbuf }
  | eof  { raise (Lexing_error "Unterminated comment") }
