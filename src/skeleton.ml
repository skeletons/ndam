open Util
open Skeltypes
open Interpretation


let rec type_name = function
	| Variable s -> s
	| Base (s, _, _) -> s
	| Product _ -> assert false
	| Arrow _ -> assert false
	| Alias (_, _, s) -> type_name s

let empty_ss = {
		ss_types = SMap.empty ;
		ss_terms = SMap.empty ;
		ss_aliases = SMap.empty
	}

let rec pattern_typ =
	begin function
	| PWild ty -> ty
	| PVar v -> v.tv_typ
	| PConstr ((c, ty), _, _) -> ty
	| PTuple pl -> Product (List.map pattern_typ pl)
	end

(* For [s] and [s'] two types, and [x] a variable,
 * [subst_type x s s'] = s'{x<-s} *)
let rec subst_type x s =
	begin function
	| Variable y when x = y -> s
	| Variable y -> Variable y
	| Base (s', spec, args) -> Base (s', spec, List.map (subst_type x s) args)
	| Product l -> Product (List.map (subst_type x s) l)
	| Alias (str, args, ty) ->
			Alias (str, List.map (subst_type x s) args, subst_type x s ty)
	| Arrow (s1, s2) -> Arrow (subst_type x s s1, subst_type x s s2)
	end

let subst_mult l1 l2 ty =
	let () = assert(List.length l1 = List.length l2) in
	List.fold_left2 (fun x v1 t2 -> subst_type v1 t2 x) ty l1 l2

let rec type_compare s1 s2 =
	begin match s1, s2 with
	| Alias (_, _, s1), s2 | s1, Alias (_, _, s2) ->
			type_compare s1 s2
	(* Variable < Base < Product < Arrow *)
	| Variable s1, Variable s2 -> compare s1 s2
	| Base (s1, spec1, l1), Base (s2, spec2, l2) ->
			let r = compare s1 s2 in
			if r <> 0 then r else
				let r = (if spec1 then 0 else 1) - (if spec2 then 0 else 1) in
				if r <> 0 then r else
					list_compare type_compare l1 l2
	| Product l1, Product l2 ->
			list_compare type_compare l1 l2
	| Arrow (s1, s'1), Arrow (s2, s'2) ->
			list_compare type_compare [s1;s'1] [s2;s'2]
	| Variable _, Base _ -> -1
	| Variable _, Product _ -> -1
	| Variable _, Arrow _ -> -1
	| Base _, Product _ -> -1
	| Base _, Variable _ -> 1
	| Base _, Arrow _ -> -1
	| Product _, Variable _ -> 1
	| Product _, Base _ -> 1
	| Product _, Arrow _ -> -1
	| Arrow _, _ -> 1
	end

let rec term_compare var_compare t1 t2 =
	match t1, t2 with
	| TVar (x1, ta1), TVar (x2, ta2) ->
			begin match compare x1 x2 with
			| 0 -> list_compare type_compare ta1 ta2
			| r -> r
			end
	| TTuple l1, TTuple l2 ->
			list_compare (term_compare var_compare) l1 l2
	| TConstr (c1, a1, l1), TConstr (c2, a2, l2) ->
			begin
				match compare c1 c2 with
				| 0 -> term_compare var_compare l1 l2
				| r -> r
			end
	| TFunc (x1, ty1, s1), TFunc (x2, ty2, s2) ->
			begin match compare x1 x2 with
			| 0 ->
					begin match type_compare ty1 ty2 with
					| 0 -> skeleton_compare var_compare s1 s2
					| r -> r
					end
			| r -> r
			end
	(* TVar < TConstr < TTuple < TFunc *)
	| TVar _, _ -> -1
	| TConstr _, TVar _ -> 1
	| TConstr _, _ -> -1
	| TTuple _, TFunc _ -> -1
	| TTuple _, _ -> 1
	| TFunc _, _ -> 1

and pattern_compare p1 p2 =
	match p1, p2 with
	| PVar x1, PVar x2 -> compare x1 x2
	| PTuple l1, PTuple l2 ->
			list_compare (pattern_compare) l1 l2
	| PConstr (c1, a1, l1), PConstr (c2, a2, l2) ->
			begin
				match compare c1 c2 with
				| 0 -> pattern_compare l1 l2
				| r -> r
			end
	| PWild s1, PWild s2 -> type_compare s1 s2
	(* PVar < PConstr < PTuple < PWild *)
	| PVar _, _ -> -1
	| PConstr _, PVar _ -> 1
	| PConstr _, _ -> -1
	| PTuple _, PWild _ -> -1
	| PTuple _, _ -> 1
	| PWild _, _ -> 1

and skeleton_compare var_compare b1 b2 =
	match b1, b2 with
	(* Cases with same constructor *)
	| Branching (s1, b1, _), Branching (s2, b2, _) ->
			(* We sort the branchings, so that the order of the branches doesn't
			 * matter *)
			let sl1 = List.sort (skeleton_compare var_compare) b1 in
			let sl2 = List.sort (skeleton_compare var_compare) b2 in
			let r = list_compare (skeleton_compare var_compare) sl1 sl2 in
			if r <> 0 then r else
				type_compare s1 s2
	| Return v1, Return v2 ->
			term_compare var_compare v1 v2
	| LetIn (a1, v1, b1, s1), LetIn (a2, v2, b2, s2)->
			let r = Option.compare compare a1 a2 in
			if r <> 0 then r else
			begin
				let r = pattern_compare v1 v2 in
				if r <> 0 then r else
				begin
					let r = skeleton_compare var_compare b1 b2 in
					if r <> 0 then r else
					begin
						skeleton_compare var_compare s1 s2
					end
				end
			end
	| Apply (f1, ta1, t1), Apply (f2, ta2, t2) ->
			begin match var_compare f1 f2 with
			| 0 ->
					begin match list_compare type_compare ta1 ta2 with
					| 0 -> list_compare (term_compare var_compare) t1 t2
					| r -> r
					end
			| r -> r
			end
	(* Other cases (Apply < Branching < Return < LetIn) *)
	| Apply _, Branching _ -> -1
	| Apply _, Return _ -> -1
	| Apply _, LetIn _ -> -1
	| Branching _, Return _ -> -1
	| Branching _, LetIn _ -> 1
	| Return _, LetIn _ -> -1
	| Branching _, Apply _ -> 1
	| Return _, Apply _ -> 1
	| LetIn _, Apply _ -> -1
	| Return _, Branching _ -> 1
	| LetIn _, Branching _ -> -1
	| LetIn _, Return _ -> -1




let get_constructors_by_type ss =
	SMap.fold (fun _ (s,c) l ->
		begin match c with
		| None -> l
		| Some c -> (s,c) :: l
		end) ss.ss_types []

let get_all_constructors ss =
	SMap.fold (fun s (_,c) l ->
		begin match c with
		| None -> l
		| Some c -> List.rev_append c l
		end) ss.ss_types []

(** Given a list, sort it and remove any duplicate. *)
let normalise l =
	let rec aux = function
		| [] -> []
		| e :: [] -> [e]
		| a :: ((b :: _) as l) ->
			if a = b then aux l else a :: aux l in
	aux (List.sort compare l)

(* Get the list of variables in a pattern *)
let rec pattern_variables p =
	begin match p with
	| PWild _ -> []
	| PVar x -> x :: []
	| PTuple l -> List.concat (List.map pattern_variables l)
	| PConstr (_, _, p) -> pattern_variables p
	end

let (free_variables_skel, free_variables_term) =
		let interp : (typed_var list, typed_var list) interpretation =
		{
			var_interp = (fun v _ -> [v]) ;
			constr_interp = (fun _ _ l -> l) ;
			tuple_interp = List.concat ;
			function_interp = (fun v _ l -> List.filter (fun x -> x.tv_name <> v) l) ;
			letin_interp = (fun _ xs bxs sxs ->
				List.filter (fun x -> not (List.mem x (pattern_variables xs))) sxs @ bxs) ;
			apply_interp = (fun f _ xs -> f :: List.flatten xs);
			merge_interp = (fun _ rs _ -> (List.concat rs)) ;
			return_interp = (fun l -> l)
		} in
		((fun s -> normalise (interpret_skeleton interp s)),
		fun t -> normalise (interpret_term interp t))

let (skeleton_type, term_type) =
		let interp : (necro_type, necro_type) interpretation =
		{
			var_interp = (fun tv _ -> tv.tv_typ) ;
			constr_interp = (fun (_, s) _ _ -> s) ;
			tuple_interp = (fun t -> Product t) ;
			function_interp = (fun _ ty s -> Arrow (ty, s)) ;
			letin_interp = (fun _ _ _ s -> s) ;
			apply_interp = (fun f ta s ->
				let rec apply f l =
					begin match f, l with
					| Arrow (a, b), a' :: q when type_compare a a' = 0 -> apply b q
					| f, [] -> f
					| _, _ -> assert false
					end
				in
				apply f.tv_typ s) ;
			merge_interp = (fun s b _ ->
				let () = List.iter (fun x -> assert(x = s)) b in s) ;
			return_interp = (fun t -> t) ;
		} in
		(interpret_skeleton interp, interpret_term interp)

let (skel_map, term_map) =
		let interp f : (term, skeleton) interpretation =
		{
			var_interp = (fun v ta -> f v) ;
			constr_interp = (fun (c, out) ta t -> TConstr ((c, out), ta, t)) ;
			tuple_interp = (fun t -> TTuple t) ;
			function_interp = (fun x ty s -> TFunc (x, ty, s)) ;
			letin_interp = (fun a t s1 s2 -> LetIn (a, t, s1, s2));
			apply_interp = (fun t1 ta t2 -> Apply (t1, ta, t2)) ;
			merge_interp = (fun s b loc -> Branching (s, b, loc)) ;
			return_interp = (fun l -> Return l)
		} in
		((fun f -> interpret_skeleton (interp f)),
			fun f -> interpret_term (interp f))

let rec ss_map f ss =
	let new_ss_terms =
		SMap.map (fun (ta, s, t) ->
			(ta, s, Option.map (term_map f) t))
		ss.ss_terms
	in
	{ ss with ss_terms=new_ss_terms }

let rec pattern_rename f =
	begin function
	| PWild ty -> PWild ty
	| PVar v -> PVar {v with tv_name = f v.tv_name}
	| PConstr (c, ta, p) -> PConstr (c, ta, pattern_rename f p)
	| PTuple t -> PTuple (List.map (pattern_rename f) t)
	end

let (skel_rename, term_rename) =
		let interp f : (term, skeleton) interpretation =
		{
			var_interp = (fun v ta -> TVar ({v with tv_name=f v.tv_name},ta)) ;
			constr_interp = (fun (c, out) ta t -> TConstr ((c,out), ta, t)) ;
			tuple_interp = (fun t -> TTuple t) ;
			function_interp = (fun x ty s -> TFunc (f x, ty, s)) ;
			letin_interp = (fun a t s1 s2 -> LetIn (a, pattern_rename f t, s1, s2));
			apply_interp = (fun t1 ta t2 -> Apply ({t1 with tv_name = f t1.tv_name}, ta, t2)) ;
			merge_interp = (fun s b loc -> Branching (s, b, loc)) ;
			return_interp = (fun l -> Return l)
		} in
		((fun f -> interpret_skeleton (interp f)),
			fun f -> interpret_term (interp f))

let rec ss_rename f ss =
	let new_ss_terms =
		SMap.map (fun (ta, s, t) ->
			(ta, s, Option.map (term_rename f) t))
		ss.ss_terms
	in
	{ ss with ss_terms=new_ss_terms }

let rec term_fresh taken =
	function
	| TVar (x, ta) -> TVar (
			{ x with tv_name = fresh
				(fun x -> not (List.mem x taken || Lexer.is_keyword x))
				x.tv_name } ,
			ta
		)
	| TConstr (c, a, subterm) ->
			TConstr (c, a, term_fresh taken subterm)
	| TTuple subterms ->
		let (subterms, _) =
			List.fold_left (fun (l, taken) t ->
				let t = term_fresh taken t in
				let taken =
					List.map (fun x -> x.tv_name) (free_variables_term t)
					@ taken in
				(t :: l, taken)) ([], taken) subterms in
		TTuple (List.rev subterms)
	| TFunc _ -> assert false (* TODO *)

let term_fresh_separate taken t =
	let rec aux local t =
		match t with
		| TVar (x, ta) ->
			let (local, x') =
				match List.assoc_opt x local with
				| Some x -> (local, x)
				| None ->
					let x' =
						let local = List.map snd local in
						fresh (fun x ->
							not (List.mem x (local @ taken) || Lexer.is_keyword x)) x.tv_name in
					((x, x') :: local, x') in
			(local, TVar ({ x with tv_name = x' }, ta))
		| TConstr (c, a, subterm) ->
				let (local, subterm) = aux local subterm in
			(local, TConstr (c, a, subterm))
		| TTuple subterms ->
			let (subterms, local) =
				List.fold_left (fun (l, local) t ->
					let (local, t) = aux local t in
					(t :: l, local)) ([], local) subterms in
			(local, TTuple (List.rev subterms))
	| TFunc _ -> assert false (* TODO *)
	in snd (aux [] t)


let rec term_equals var_equals t1 t2 =
	match t1, t2 with
	| TVar (x1, ta1), TVar (x2, ta2) ->
			var_equals x1 x2 &&
			List.length ta1 = List.length ta2 &&
			List.for_all2 (=) ta1 ta2
	| TConstr (c1, a1, l1), TConstr (c2, a2, l2) ->
		c1 = c2 && term_equals var_equals l1 l2
	| TTuple l1, TTuple l2 ->
		List.length l1 = List.length l2 && List.for_all2 (term_equals var_equals) l1 l2
	| TFunc (x1, ty1, s1), TFunc (x2, ty2, s2) ->
			x1 = x2 &&
			ty1 = ty2 &&
			skeleton_equals var_equals s1 s2
	| _, _ -> false

and skeleton_equals var_equals s1 s2 =
	match s1, s2 with
	| Branching (s, b, _), Branching (s', b', _) ->
			List.length b = List.length b' &&
			List.for_all2 (skeleton_equals var_equals) b b' &&
			s = s'
	| LetIn (b, p, s1, s2), LetIn (b', p', s1', s2') ->
			b = b' &&
			p = p' &&
			skeleton_equals var_equals s1 s1' &&
			skeleton_equals var_equals s2 s2'
	| Return t, Return t' ->
			term_equals var_equals t t'
	| Apply (v, ta, t), Apply (v', ta', t') ->
			var_equals v v' &&
			List.length ta = List.length ta' &&
			List.for_all2 (=) ta ta' &&
			List.for_all2 (term_equals var_equals) t t'
	| _, _ -> false

let (declared_variables_skel, declared_variables_term) =
		let interp : (typed_var list, typed_var list) interpretation =
		{
			var_interp = (fun _ _ -> []) ;
			constr_interp = (fun _ _ l -> l) ;
			tuple_interp = List.concat ;
			function_interp = (fun v ty l ->  {tv_name=v;tv_typ=ty} :: l) ;
			letin_interp = (fun _ t bxs sxs -> pattern_variables t @ bxs @ sxs) ;
			apply_interp = (fun f ta l -> List.concat l) ;
			merge_interp = (fun _ rs _ -> (List.concat rs)) ;
			return_interp = (fun l -> l)
		} in
	((fun s -> normalise (interpret_skeleton interp s)),
	fun t -> normalise (interpret_term interp t))

let (alive_variables_skel, alive_variables_term) =
		let interp : (var -> bool, var -> bool) interpretation =
		{
			var_interp = (fun x _ id -> x.tv_name = id) ;
			constr_interp = (fun _ _ f -> f) ;
			tuple_interp = (fun fl id -> List.exists (fun f -> f id) fl) ;
			function_interp = (fun v ty f id -> id <> v && f id) ;
			letin_interp = (fun _ p f1 f2 id ->
				f1 id || f2 id && not (List.exists (fun t -> t.tv_name = id) (pattern_variables p)) );
			apply_interp = (fun x ta fl id -> id = x.tv_name ||
				List.exists (fun f -> f id) fl) ;
			merge_interp = (fun _ fl _ id -> List.exists (fun f -> f id) fl) ;
			return_interp = (fun f -> f)
		} in
	(interpret_skeleton interp, interpret_term interp)

let (all_variables_skel, all_variables_term) =
		let interp : (var -> bool, var -> bool) interpretation =
		{
			var_interp = (fun x _ id -> x.tv_name = id) ;
			constr_interp = (fun _ _ f -> f) ;
			tuple_interp = (fun fl id -> List.exists (fun f -> f id) fl) ;
			function_interp = (fun v ty f id -> id = v || f id) ;
			letin_interp = (fun _ p f1 f2 id ->
				f1 id || f2 id || (List.exists (fun t -> t.tv_name = id) (pattern_variables p)) );
			apply_interp = (fun x ta fl id -> id = x.tv_name ||
				List.exists (fun f -> f id) fl) ;
			merge_interp = (fun _ fl _ id -> List.exists (fun f -> f id) fl) ;
			return_interp = (fun f -> f)
		} in
	(interpret_skeleton interp, interpret_term interp)

let all_variables_ss ss x =
	SMap.exists (fun name (_, _, t) ->
		begin match t with
		| None -> name = x
		| Some t -> name = x || all_variables_term t x
		end) ss.ss_terms


let (all_variables_list_skel, all_variables_list_term) =
		let interp : (var list, var list) interpretation =
		{
			var_interp = (fun x _ -> x.tv_name :: []) ;
			constr_interp = (fun _ _ l -> l) ;
			tuple_interp = (fun ll -> List.concat ll) ;
			function_interp = (fun v ty l -> v :: l) ;
			letin_interp = (fun _ p l1 l2 ->
				normalise(List.map (fun x -> x.tv_name) (pattern_variables p) @ l1 @ l2) );
			apply_interp = (fun x ta l ->  x.tv_name :: List.concat l ) ;
			merge_interp = (fun _ ll _ -> List.concat ll) ;
			return_interp = (fun l -> l)
		} in
	((fun s -> normalise (interpret_skeleton interp s)),
		(fun t -> normalise (interpret_term interp t)))

let all_variables_list_ss ss =
	normalise (SMap.fold (fun name (_, _, value) l ->
		name :: begin match value with
						| None -> l | Some t -> all_variables_list_term t @ l end)
		ss.ss_terms [])

let (all_variables_set_skel, all_variables_set_term) =
		let union_list = List.fold_left SSet.union SSet.empty in
		let interp : (SSet.t, SSet.t) interpretation =
		{
			var_interp = (fun x _ -> SSet.singleton x.tv_name) ;
			constr_interp = (fun _ _ l -> l) ;
			tuple_interp = (fun ll -> union_list ll) ;
			function_interp = (fun v ty l -> SSet.add v l) ;
			letin_interp = (fun _ p l1 l2 ->
				SSet.union
					(SSet.union
						(SSet.of_list
							(List.map (fun x -> x.tv_name) (pattern_variables p)))
						l1)
					l2);
			apply_interp = (fun x ta l -> SSet.add x.tv_name (union_list l)) ;
			merge_interp = (fun _ ll _ -> union_list ll) ;
			return_interp = (fun l -> l)
		} in
	(interpret_skeleton interp, interpret_term interp)

let all_variables_set_ss ss =
	SMap.fold (fun name (_, _, value) l ->
		SSet.add name begin match value with
			| None -> l | Some t -> SSet.union (all_variables_set_term t) l end)
		ss.ss_terms SSet.empty

let (declared_outer_variables_skel, declared_outer_variables_term) =
		let interp : (typed_var list, typed_var list) interpretation =
		{
			var_interp = (fun _ _ -> []) ;
			constr_interp = (fun _ _ l -> l) ;
			tuple_interp = List.concat ;
			function_interp = (fun v ty l ->  {tv_name=v;tv_typ=ty} :: l) ;
			letin_interp = (fun _ t _ sxs -> pattern_variables t @ sxs) ;
			apply_interp = (fun f ta l -> List.concat l) ;
			merge_interp = (fun _ rs _ -> (List.concat rs)) ;
			return_interp = (fun l -> l)
		} in
	((fun s -> normalise (interpret_skeleton interp s)),
	fun t -> normalise (interpret_term interp t))


let (protect_skel, protect_term, protect_ss) =
	let rec protect_list protect_one ag av m =
		begin function
		| [] -> ([], av, m)
		| s :: q ->
				let (s', av', m') = protect_one ag av m s in
				let (q', av'', m'') = protect_list protect_one ag av' m' q in
				(s' :: q', av'', m'')
		end
	in
	let protect_var ag av m name =
		begin match SMap.find_opt name m with
		| Some name' -> (name', av, m)
		| None when List.mem name ag ->
				let name' = fresh (fun x -> not (List.mem x av || Lexer.is_keyword x)) name in
				(name', name :: av, SMap.add name name' m)
		| None -> (name, av, m)
		end
	in
	let rec protect_type ag av m =
		begin function
		| Variable a ->
				let (a', av', m') = protect_var ag av m a in
				(Variable a', av', m')
		| Base (s, spec, args) ->
				let (s', av', m') = protect_var ag av m s in
				let (args', av'', m'') = protect_list protect_type ag av' m' args in
				(Base (s', spec, args'), av'', m'')
		| Product sl ->
				let (sl', av', m') = protect_list protect_type ag av m sl in
				(Product sl', av', m')
		| Alias (n, a, s) ->
			let (n, av, m) = protect_var ag av m n in
			let (a, av, m) = protect_list protect_type ag av m a in
			let (s, av, m) = protect_type ag av m s in
			(Alias (n, a, s), av, m)
		| Arrow (s1, s2) ->
				let (s1', av', m') = protect_type ag av m s1 in
				let (s2', av'', m'') = protect_type ag av' m' s2 in
				(Arrow (s1', s2'), av'', m'')
		end
	in
	let rec protect_pattern ag av m p =
		begin match p with
		| PWild ty -> (PWild ty, av, m)
		| PVar {tv_name;tv_typ} ->
				let (tv_name, av, m) = protect_var ag av m tv_name in
				let (tv_typ, av, m) = protect_type ag av m tv_typ in
				(PVar {tv_name;tv_typ}, av, m)
		| PConstr ((c, ty), args, p) ->
				let (ty, av, m) = protect_type ag av m ty in
				let (args, av, m) = protect_list protect_type ag av m args in
				let (p, av, m) = protect_pattern ag av m p in
				(PConstr ((c, ty), args, p), av, m)
		| PTuple pl ->
				let (pl, av, m) = protect_list protect_pattern ag av m pl in
				(PTuple pl, av, m)
		end
	in
	let interp ag:
		(string list -> string SMap.t -> (term * string list * string SMap.t),
		 string list -> string SMap.t -> (skeleton * string list * string SMap.t))
	interpretation =
		{
			var_interp = (fun tv args av m ->
				let (tv_name, av, m) = protect_var ag av m tv.tv_name in
				let (tv_typ, av, m) = protect_type ag av m tv.tv_typ in
				let (args, av, m) = protect_list protect_type ag av m args in
				(TVar ({tv_name;tv_typ}, args), av, m));
			constr_interp = (fun (c, ty) args t av m ->
				let (c, av, m) = protect_var ag av m c in
				let (ty, av, m) = protect_type ag av m ty in
				let (t, av, m) = t av m in
				let (args, av, m) = protect_list protect_type ag av m args in
				(TConstr ((c, ty), args, t), av, m)) ;
			tuple_interp = (fun tl av m ->
				let (tl, av, m) = protect_list (fun _ av m t -> t av m) ag av m tl in
				(TTuple tl, av, m));
			function_interp = (fun x ty sk av m ->
				let (x, av, m) = protect_var ag av m x in
				let (ty, av, m) = protect_type ag av m ty in
				let (sk, av, m) = sk av m in
				(TFunc (x, ty, sk), av, m)) ;
			letin_interp = (fun b p s1 s2 av m ->
				let (b, av, m) =
					begin match b with
					| None -> (None, av, m)
					| Some (bind, ta) ->
							let (b, av, m) = protect_var ag av m bind in
							let (ta, av, m) = protect_list protect_type ag av m ta in
							(Some (b, ta), av, m)
					end
				in
				let (p, av, m) = protect_pattern ag av m p in
				let (s1, av, m) = s1 av m in
				let (s2, av, m) = s2 av m in
				(LetIn (b, p, s1, s2), av, m)) ;
			apply_interp = (fun {tv_typ;tv_name} ta tl av m ->
				let (tv_name, av, m) = protect_var ag av m tv_name in
				let (tv_typ, av, m) = protect_type ag av m tv_typ in
				let (ta, av, m) = protect_list protect_type ag av m ta in
				let (tl, av, m) =
					List.fold_right (fun f (l, av, m) ->
						let (t, av, m) = f av m in (t :: l, av, m))
					tl ([], av, m) in
				Apply ({tv_name;tv_typ}, ta, tl), av, m);
			merge_interp = (fun ty sl loc av m ->
				let (ty, av, m) = protect_type ag av m ty in
				let (sl, av, m) = protect_list (fun _ av m t -> t av m) ag av m sl in
				(Branching (ty, sl, loc), av, m));
			return_interp = (fun t av m ->
				let (t, av, m) = t av m in
				(Return t, av, m))
		} in
	let protect_skel against s =
		let av = List.rev_append against (all_variables_list_skel s) in
		let (s', _, _) = interpret_skeleton (interp against) s av SMap.empty in
		s'
	in
	let protect_term against t =
		let av = List.rev_append against (all_variables_list_term t) in
		let (t', _, _) = interpret_term (interp against) t av SMap.empty in
		t'
	in
	let protect_ss against ss =
		let av = List.rev_append against (all_variables_list_ss ss) in
		let (ss_terms, av, m) =
			SMap.fold (fun name (ta, ty, t_maybe) (ss_terms, av, m) ->
				let (name, av, m) = protect_var against av m name in
				let (ta, av, m) = protect_list protect_var against av m ta in
				let (ty, av, m) = protect_type against av m ty in
				begin match t_maybe with
				| None -> (SMap.add name (ta, ty, None) ss_terms, av, m)
				| Some t ->
						let (t, av, m) = interpret_term (interp against) t av m in
						(SMap.add name (ta, ty, Some t) ss_terms, av, m)
				end) ss.ss_terms (SMap.empty, av, SMap.empty)
		in
		let protect_csig ag av m csig  =
			let (cs_name, av, m) = protect_var ag av m csig.cs_name in
			let (cs_type_args, av, m) = protect_list protect_var ag av m csig.cs_type_args in
			let (cs_input_type, av, m) = protect_type ag av m csig.cs_input_type in
			let (cs_output_type, av, m) = protect_type ag av m csig.cs_output_type in
			({cs_name; cs_type_args; cs_input_type; cs_output_type}, av, m)
		in
		let (ss_types, av, m) =
			SMap.fold (fun name (ty, cl_maybe) (ss_types, av, m) ->
				let (ty, av, m) = protect_type against av m ty in
				let (name, av, m) = protect_var against av m name in
				begin match cl_maybe with
				| None -> (SMap.add name (ty, None) ss_types, av, m)
				| Some cl ->
						let (cl, av, m) = protect_list protect_csig against av m cl in
						(SMap.add name (ty, Some cl) ss_types, av, m)
				end) ss.ss_types (SMap.empty, av, m)
		in
		let (ss_aliases, av, m) =
			SMap.fold (fun name (ta, ty) (ss_aliases, av, m) ->
				let (ta, av, m) = protect_list protect_var against av m ta in
				let (ty, av, m) = protect_type against av m ty in
						(SMap.add name (ta, ty) ss_aliases, av, m))
			ss.ss_aliases (SMap.empty, av, m)
		in
		{ss_terms; ss_types; ss_aliases}
	in
	(protect_skel, protect_term, protect_ss)


(* Return List.rev (List.map f l1) @ l2 *)
let list_rev_map_append (f: 'a -> 'b) (l1: 'a list) (l2: 'b list) : 'b list =
	let rec aux l accu =
		begin match l with
		| [] -> accu
		| a :: q -> aux q (f a :: accu)
		end
	in
	aux l1 l2

let ss_reserved_names ss =
	(* This is one of these functions where one would like to use [List.map]
	 * but can’t without using [Obj.magic].
	 * This would be so much nicer in Coq. *)
	SSet.of_list (List.concat [
			smap_domain ss.ss_types ;
			smap_domain ss.ss_terms ;
			List.map (fun c -> c.cs_name) (get_all_constructors ss)
		])

let ss_all_names ss =
	let set = ss_reserved_names ss in
	let add_term t set =
		SSet.union set (SSet.of_list
			(List.map (fun x -> x.tv_name) (declared_variables_term t))) in
	let set =
		SMap.fold
		(fun _ (_, _, t) -> Option.fold ~none:id ~some:add_term t) ss.ss_terms set in
	set



(** Compare two semantics *)
let same_ss e1 e2 =
	let same_type s1 s2 =
		(s1 = s2) in
	let var_compare v1 v2 =
		let r = type_compare v1.tv_typ v2.tv_typ in
		if r <> 0 then r else
			compare v1.tv_name v2.tv_name
	in
	let type_arg_compare a1 a2 = compare a1 a2
	in
	let rec type_args_compare l1 l2 =
		match l1, l2 with
		(* [] < _::_ *)
		| [], [] -> 0
		| a1 :: q1, a2 :: q2 ->
				let r = type_arg_compare a1 a2 in
				if r <> 0 then r else type_args_compare q1 q2
		| [], _ :: _ -> -1
		| _ :: _, [] -> 1
	in
	let same_term (l1, s1, t1) (l2, s2, t2) =
		type_args_compare l1 l2 = 0 &&
		type_compare s1 s2 = 0 &&
		Option.compare (term_compare var_compare) t1 t2 = 0
	in
	let same_ss_types s1 s2 =
		SMap.equal same_type s1 s2 in
	let same_ss_terms m1 m2 =
		SMap.equal same_term m1 m2 in
	same_ss_types e1.ss_types e2.ss_types &&
	same_ss_terms e1.ss_terms e2.ss_terms



(* Get a common refinement of two patterns. The set of matched
 * terms is the whole type *)
let rec common_refinement (env:skeletal_semantics) (s: necro_type) (p1: pattern) (p2: pattern): pattern list =
	let get_constructors s =
		begin match snd (SMap.find (type_name s) env.ss_types) with
		| None -> assert false
		| Some x -> x
		end
	in
	begin match p1, p2 with
	| PWild _, PWild _
	| PWild _, PVar _
	| PVar _, PWild _
	| PVar _, PVar _ ->
			PWild s :: []
	| PConstr ((c, s'), _, p), PVar _
	| PConstr ((c, s'), _, p), PWild _
	| PVar _, PConstr ((c, s'), _, p)
	| PWild _, PConstr ((c, s'), _, p) ->
			let () = assert(s' = s) in
			let is = pattern_typ p in
			let l = common_refinement env is p p in
			let constrs = get_constructors s' in
			let all_cons = List.map (fun csig ->
				PConstr ((csig.cs_name, s'), [], PWild csig.cs_input_type)) constrs in
			let almost_all_cons = List.filter (function
				| PConstr ((c',_), _, _) -> c <> c'
				| _ -> assert false)
			all_cons in
			let this_cons = List.map (fun t -> PConstr ((c,s'), [], t)) l in
			almost_all_cons @ this_cons
	| PConstr ((c1, s1), _, a1), PConstr ((c2, s2), _, a2) when c1 <> c2 ->
			let is1 = pattern_typ a1 in
			let is2 = pattern_typ a2 in
			let l1 = common_refinement env is1 a1 a1 in
			let l2 = common_refinement env is2 a2 a2 in
			let constrs = get_constructors s1 in
			let all_cons = List.map (fun csig ->
				PConstr ((csig.cs_name, s1), [], PWild csig.cs_input_type)) constrs in
			let almost_all_cons = List.filter (function PConstr ((c,_), _, _) ->
				c <> c1 && c <> c2 | _ -> assert false) all_cons
			in
			let this_cons = List.map (fun t -> PConstr ((c1, s1), [], t)) l1 in
			let that_cons = List.map (fun t -> PConstr ((c2, s2), [], t)) l2 in
			almost_all_cons @ this_cons @ that_cons
	| PConstr ((c1, s1), _, p1), PConstr (c2, _, p2) (*when c1 = c2*) ->
			let is = pattern_typ p1 in
			let l = common_refinement env is p1 p2 in
			let constrs = get_constructors s1 in
			let all_cons = List.map (fun csig ->
				PConstr ((csig.cs_name, s1), [], PWild csig.cs_input_type)) constrs in
			let almost_all_cons = List.filter (function PConstr ((c, _), _, _) ->
				c <> c1 | _ -> assert false) all_cons in
			let this_cons = List.map (fun t -> PConstr ((c1, s1), [], t)) l in
			almost_all_cons @ this_cons
	| PTuple t, PWild _ | PWild _, PTuple t
	| PTuple t, PVar _ | PVar _, PTuple t ->
			let new_tuple =
				List.map (fun p -> PWild (pattern_typ p)) t in
			common_refinement env s p1 (PTuple new_tuple)
	| PTuple v1, PTuple v2 ->
			let s' = begin match s with
				| Product s' | Alias (_, _, Product s') -> s'
				| _ -> assert false
			end in
			let tmp = (list_map3 (common_refinement env) s' v1 v2) in
			let refinements = product tmp in
			List.map (fun t -> PTuple t) refinements
	| PConstr _, PTuple _ | PTuple _, PConstr _ -> assert false
	end




