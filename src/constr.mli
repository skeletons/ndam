(* TODO: This file has been automatically generated and needs clean-up and documentation. *)

open Util

type typedesc =
  Pconstr.typedesc =
    MapType of typedesc * typedesc
  | SetType of typedesc
  | ListType of typedesc
  | BaseType of string
  | TupleType of typedesc list
  | Program_point of typedesc
  | TopType
  | BottomType

type quark_decl =
  Pconstr.quark_decl = {
  q_name : string;
  q_inputs : typedesc list;
  q_output : typedesc;
}
type 'a lambda = 'a Pconstr.lambda
type 'a with_type = { desc : 'a; typ : typedesc; }
type lvalue_desc = LMapGet of lvalue * value | LNamedVar of string
and lvalue = lvalue_desc with_type
and value_desc =
    LocalVar of string
  | MapGet of value * value
  | NamedVar of string
  | Func of string * value list
  | VLet of value * value lambda
  | VWith of constr * value
  | VTuple of value list
  | VSet of value list
  | VList of value list
  | VJoin of value * value lambda
  | VIsEmpty of value * value
  | VUnit
and value = value_desc with_type
and constr =
    True
  | Conj of constr * constr
  | CLet of value * constr lambda
  | Sub of value * lvalue
  | Forall of value * constr lambda
  | CEmpty of value * constr
type dpath = string * value option
type deps =
    DBase of dpath list
  | DLet of value * deps lambda
  | DForall of value * deps lambda
  | DEmpty of value * deps
  | DConj of deps * deps
type tcenv = {
  tcenv_basetypes : (string * bool * bool) list SMap.t ;
  tcenv_types : typedesc SMap.t;
  tcenv_vars : (typedesc * bool) SMap.t;
  tcenv_rulevars : (typedesc * typedesc) SMap.t;
  tcenv_funcs : quark_decl SMap.t;
}
type filter_constr_decl = { fc_name : string; fc_value : value lambda; }
type hookin_constr_decl = {
  hi_name : string;
  hi_annot : string;
  hi_value : value lambda;
}
type hookout_constr_decl = {
  ho_name : string;
  ho_annot : string;
  ho_value : value lambda;
}
type rulein_constr_decl = { ri_name : string; ri_value : value lambda; }
type ruleout_constr_decl = { ro_name : string; ro_value : value lambda; }
type cenv = {
  ce_tce : tcenv;
  ce_tenv : Skeltypes.skeletal_semantics;
  ce_filts : filter_constr_decl SMap.t;
  ce_hookins : hookin_constr_decl SMap.t;
  ce_hookouts : hookout_constr_decl SMap.t;
  ce_ruleins : rulein_constr_decl SMap.t;
  ce_ruleouts : ruleout_constr_decl SMap.t;
}
val print_type : Format.formatter -> typedesc -> unit
val fresh : string -> string
val lambda_type : 'a * 'b with_type -> typedesc
val var_appears_lambda : ('a -> 'b -> int) -> 'a -> 'a list * 'b -> int
val var_appears_lvalue : string -> lvalue_desc with_type -> int
val var_appears_value : string -> value -> int
val var_appears_constr : string -> constr -> int
val var_appears_dep : string -> deps -> int
module SSet :
  sig
    type elt = String.t
    type t = Set.Make(String).t
    val empty : t
    val is_empty : t -> bool
    val mem : elt -> t -> bool
    val add : elt -> t -> t
    val singleton : elt -> t
    val remove : elt -> t -> t
    val union : t -> t -> t
    val inter : t -> t -> t
    val diff : t -> t -> t
    val compare : t -> t -> int
    val equal : t -> t -> bool
    val subset : t -> t -> bool
    val iter : (elt -> unit) -> t -> unit
    val map : (elt -> elt) -> t -> t
    val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
    val for_all : (elt -> bool) -> t -> bool
    val exists : (elt -> bool) -> t -> bool
    val filter : (elt -> bool) -> t -> t
    val partition : (elt -> bool) -> t -> t * t
    val cardinal : t -> int
    val elements : t -> elt list
    val min_elt : t -> elt
    val min_elt_opt : t -> elt option
    val max_elt : t -> elt
    val max_elt_opt : t -> elt option
    val choose : t -> elt
    val choose_opt : t -> elt option
    val split : elt -> t -> t * bool * t
    val find : elt -> t -> elt
    val find_opt : elt -> t -> elt option
    val find_first : (elt -> bool) -> t -> elt
    val find_first_opt : (elt -> bool) -> t -> elt option
    val find_last : (elt -> bool) -> t -> elt
    val find_last_opt : (elt -> bool) -> t -> elt option
    val of_list : elt list -> t
    val to_seq_from : elt -> t -> elt Seq.t
    val to_seq : t -> elt Seq.t
    val add_seq : elt Seq.t -> t -> t
    val of_seq : elt Seq.t -> t
  end
val fv_lambda : ('a -> SSet.t) -> SSet.elt list * 'a -> SSet.t
val fv_lvalue : lvalue_desc with_type -> SSet.t
val fv_value : value -> SSet.t
val fv_constr : constr -> SSet.t
val fv_dep : deps -> SSet.t
val lift_with_lambda :
  ('a -> 'b * 'c list) -> 'd * 'a -> ('d * 'b) * ('d * 'c) list
val lift_with_shallow_value : value -> value * constr list
val lift_with_shallow_value_list : value list -> value list * constr list
val filter_with_value : value_desc with_type -> value
val make_vwith_list : value -> constr list -> value
val make_with : constr -> value -> value_desc with_type
val make_tuple_type : typedesc list -> typedesc
val make_tuple : value list -> value_desc with_type
val make_list : value list -> value_desc with_type
val find_var_name : 'a -> string -> string
val subst_lambda :
  (value_desc SMap.t -> SSet.t -> 'a -> 'b) ->
  value_desc SMap.t -> SSet.t -> SSet.elt list * 'a -> SSet.elt list * 'b
val alpha_convert :
  (value_desc SMap.t -> SSet.t -> 'a -> 'b) ->
  SSet.t -> SSet.elt list * 'a -> SSet.elt list * 'b
val subst_value :
  value_desc SMap.t -> SSet.t -> value_desc with_type -> value
val subst_constr : value_desc SMap.t -> SSet.t -> constr -> constr
val subst_lvalue : value_desc SMap.t -> SSet.t -> lvalue -> lvalue
val make_vlet : value -> SSet.elt list * value -> value
val make_clet : value -> SMap.key list * constr -> constr
val never_empty : value -> bool
val not_empty_cond : value -> value
val always_empty : value_desc with_type -> bool
val try_select_fields : int list -> value_desc with_type -> value option
val try_select_fields_in : bool -> int list -> value -> value option
val make_join1 : bool -> value -> SSet.elt list * value -> value
val make_join_gen : bool -> value -> SSet.elt list * value -> value
val make_join : value -> SSet.elt list * value -> value
val make_visempty : value -> value -> value
val lift_with_lvalue : lvalue_desc with_type -> lvalue * constr list
val lift_with_value : value -> value * constr list
val lift_with_value_list : value list -> value list * constr list
val lift_with_constr : constr -> constr list
val make_dconj : deps -> deps -> deps
val over_approx_dep_empty : bool
val subst_dep : value_desc SMap.t -> SSet.t -> deps -> deps
val make_dlet : value -> SSet.elt list * deps -> deps
val make_dforall1 : value -> SSet.elt list * deps -> deps
val make_dforall : value -> SSet.elt list * deps -> deps
val make_dempty : value -> deps -> deps
val deps_lvalue_in : lvalue_desc with_type -> deps
val deps_value_in : value -> deps
val deps_constr_in : constr -> deps
val deps_lvalue_out : lvalue_desc with_type -> deps
val deps_value_out : value -> deps
val deps_constr_out : constr -> deps
val deps_deps : deps -> deps

