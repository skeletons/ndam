%{
    open Pskeltypes
    let ghost_loc = (Lexing.dummy_pos, Lexing.dummy_pos)

    let split_decl l =
      let rec aux (a, b, c, d, e) = function
        | [] -> (a, b, c, d, e)
        | PAbstractType (s, n) :: l ->
            aux ((s, n) :: a, b, c, d, e) l
        | PVariantType (name, n, consts) :: l ->
            aux (a, (name, n, consts) :: b, c, d, e) l
        | PHookDecl (name, type_args, ins, outs) :: l ->
            aux (a, b, (name, type_args, ins, outs) :: c, d, e) l
        | PRuleDef ppd :: l ->
            aux (a, b, c, ppd :: d, e) l
        | PMonad (m, o) :: l ->
            aux (a, b, c, d, (m, o) :: e) l
      in
      let (abs, var, hooks, rules, monads) =
        aux ([], [], [], [], []) (List.rev l)
      in
      {
        pabs_type = abs ;
        pvar_type = var ;
        phook_decl = hooks ;
        prule_def = rules ;
        pmonads = monads
      }

    let tt = {loc=ghost_loc; contents=PTTuple []}

%}

(** * Definitions **)

(** ** Keywords **)

%token BRANCH
%token END
%token HOOK
%token OF
%token OR
%token TYPE

%token FILTER
%token IN
%token LET
%token TERM
%token UNIT

%token MONAD
%token RET

(* Keywords used by the constraint generation. *)
%token RULE

%token FRESH
%token INSTRUCTION
%token ASSUME
%token RETURN

(** ** Identifiers **)
%token <string> LIDENT
%token <string> UIDENT

(** ** Operators **)
%token PERCENT
%token BAR
(*%token BARBAR*)
(*%token BARMINUSGREATER*)
%token CARET
%token COLON
%token COMMA
%token EQUAL
(*%token LESSMINUS*)
%token MINUSGREATER
(*%token QUESTIONGREATER*)
%token QUESTIONMARK
%token SEMI
%token STAR
%token UNDERSCORE

(** ** Paired delimiters **)
%token LPAREN
%token RPAREN
%token LESS
%token GREATER
%token LBRACK
%token RBRACK
%token LBRACE
%token RBRACE
(*%token LBRACKBAR*)
(*%token BARRBRACK*)

(** ** Values **)
%token <string> STRING

(** ** Other **)
%token EOF

%start main
%type <Pskeltypes.pdecls> main
%start term_decls
%type <Pskeltypes.pterm list> term_decls
%start export
%type <string ExportInstr.env> export

%nonassoc let_prec
%left SEMI

%% (** Rules **)


(** auxiliary symbols **)

(* (X) *)
let parenthesized(X) ==
  delimited(LPAREN, X, RPAREN)

(* (X, …, X) *)
let tuple(X) == parenthesized(separated_list(COMMA, X))

(** * Exportation Instructions **)

let export :=
  l = terminated(filter_export_decl*, EOF) ;
  { List.fold_left (fun m (f, e) -> Util.SMap.add f e m) Util.SMap.empty l }

let filter_export_decl :=
  FILTER ; f = LIDENT ;
  args = tuple(LIDENT) ;
  EQUAL ; e = filter_export ;
  { let () =
      Option.iter (fun x -> failwith ("Duplicated argument " ^ x ^
      " in hint for filter " ^ f ^ ".")) (Util.find_duplicate args)
    in
    let (fresh, instr, a, l) = e f args in
    (f, {
      ExportInstr.inputs = args ;
      ExportInstr.fresh = fresh ;
      ExportInstr.instructions = instr ;
      ExportInstr.assume = a ;
      ExportInstr.return = l
    }) }

let filter_export :=
  fresh = preceded(FRESH, LIDENT)* ;
  instr = preceded(INSTRUCTION, output_export)* ;
  a     = preceded(ASSUME, output_export)? ;
  l     = preceded(RETURN, output_export)* ;
      { fun f args ->
        let args =
          Option.iter (fun x -> failwith ("Duplicated fresh variable " ^ x ^
          " in hint for filter " ^ f ^ ".")) (Util.find_duplicate fresh) ;
          List.iter (fun x ->
            if List.mem x args then
              failwith ("Shadowed variable " ^ x ^ " in hint for filter " ^ f ^ ".")) fresh ;
          fresh @ args in
        (fresh,
         List.map (fun i -> i f args) instr,
         Option.map (fun a -> a f args) a,
         List.map (fun s -> s f args) l) }

let output_export :=
  l = output_export_item* ;
  { fun f args -> List.map (fun s -> s f args) l }

let output_export_item :=
| s = STRING ;
  { fun _ _ -> ExportInstr.String s }
| x = LIDENT ;
  { fun f args ->
    if List.mem x args then
      ExportInstr.Var x
    else
      failwith ("Undeclared variable " ^ x ^ " in hint for filter " ^ f ^ ".") }


