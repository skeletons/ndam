open Util
open Pconstr
open Constr
open Skeleton
open Skeltypes

let type_error (lstart, lend) fmt =
	Format.kasprintf (fun s ->
		Parserutil.report_error ("Type error: " ^ s) lstart lend ;
		raise Typer.TypeError) fmt

(* Reduce a type to head normal form: head type should not be a type alias *)
let rec canonize_type env t =
	match t with
	| BaseType s when SMap.mem s env.tcenv_types -> canonize_type env (SMap.find s env.tcenv_types)
	| _ -> t

type unification_failure_type = SubType | EqType | Unify
exception TypeUnificationError of (unification_failure_type * typedesc * typedesc)

(* Raise unification error if t1 is not equal to t2 *)
let rec check_eqtype env t1 t2 =
	match t1, t2 with
	| SetType t1, SetType t2
	| ListType t1, ListType t2 -> check_eqtype env t1 t2
	| BaseType s1, BaseType s2 when s1 = s2 -> ()
	| TopType, TopType -> ()
	| BottomType, BottomType -> ()
	| TupleType l1, TupleType l2 when List.length l1 = List.length l2 ->
		List.iter2 (check_eqtype env) l1 l2
	| MapType (t1k, t1v), MapType (t2k, t2v) ->
		check_eqtype env t1k t2k; check_eqtype env t1v t2v
	| Program_point t1, Program_point t2 -> check_eqtype env t1 t2
	| BaseType s1, BaseType s2 when SMap.mem s1 env.tcenv_types && SMap.mem s2 env.tcenv_types ->
		check_eqtype env (canonize_type env t1) (canonize_type env t2)
	| BaseType s1, _ when SMap.mem s1 env.tcenv_types ->
		check_eqtype env (canonize_type env t1) t2
	| _, BaseType s2 when SMap.mem s2 env.tcenv_types ->
		check_eqtype env t1 (canonize_type env t2)
	| _ -> raise (TypeUnificationError (EqType, t1, t2))

(* Raise unification error if t1 is not a subtype of t2 *)
let rec check_subtype env t1 t2 =
	match t1, t2 with
	| _, TopType -> ()
	| BottomType, _ -> ()
	| SetType t1, SetType t2
	| ListType t1, ListType t2 -> check_subtype env t1 t2
	| BaseType s1, BaseType s2 when s1 = s2 -> ()
	| TupleType l1, TupleType l2 when List.length l1 = List.length l2 ->
		List.iter2 (check_subtype env) l1 l2
	| MapType (t1k, t1v), MapType (t2k, t2v) ->
		check_eqtype env t1k t2k; check_subtype env t1v t2v
	| Program_point t1, Program_point t2 -> check_subtype env t1 t2
	| BaseType s1, BaseType s2 when SMap.mem s1 env.tcenv_types && SMap.mem s2 env.tcenv_types ->
		check_subtype env (canonize_type env t1) (canonize_type env t2)
	| BaseType s1, _ when SMap.mem s1 env.tcenv_types ->
		check_subtype env (canonize_type env t1) t2
	| _, BaseType s2 when SMap.mem s2 env.tcenv_types ->
		check_subtype env t1 (canonize_type env t2)
	| _ -> raise (TypeUnificationError (SubType, t1, t2))

(* Returns the most general unifier of t1 and t2; raises unification error if t1 and t2 are not unifiable *)
let rec unify_types env t1 t2 =
	match t1, t2 with
	| TopType, _ | _, TopType -> TopType
	| BottomType, t | t, BottomType -> t
	| SetType t1, SetType t2 -> SetType (unify_types env t1 t2)
	| ListType t1, ListType t2 -> ListType (unify_types env t1 t2)
	| BaseType s1, BaseType s2 when s1 = s2 -> t1
	| TupleType l1, TupleType l2 when List.length l1 = List.length l2 ->
		TupleType (List.map2 (unify_types env) l1 l2)
	| MapType (t1k, t1v), MapType (t2k, t2v) ->
		check_eqtype env t1k t2k; MapType (t1k, unify_types env t1v t2v)
	| Program_point t1, Program_point t2 ->
		(try Program_point (unify_types env t1 t2)
		 with TypeUnificationError _ -> Program_point TopType)
	| BaseType s1, BaseType s2 when SMap.mem s1 env.tcenv_types && SMap.mem s2 env.tcenv_types ->
		unify_types env (canonize_type env t1) (canonize_type env t2)
	| BaseType s1, _ when SMap.mem s1 env.tcenv_types ->
		unify_types env (canonize_type env t1) t2
	| _, BaseType s2 when SMap.mem s2 env.tcenv_types ->
		unify_types env t1 (canonize_type env t2)
	| _ -> raise (TypeUnificationError (Unify, t1, t2))

let print_unification_error ff (ut, t1, t2, ta, tb) =
	match ut with
	| EqType ->
		Format.fprintf ff "Type %a is not equal to type %a: type %a is not equal to type %a"
			print_type t1 print_type t2 print_type ta print_type tb
	| SubType ->
		Format.fprintf ff "Type %a is not a subtype of type %a: type %a is not a subtype of type %a"
			print_type t1 print_type t2 print_type ta print_type tb
	| Unify ->
		Format.fprintf ff "Type %a is not unifiable with type %a: type %a is not unifiable with type %a"
			print_type t1 print_type t2 print_type ta print_type tb
let protect_unification_error f ?loc env t1 t2 =
	match loc with
	| None -> f env t1 t2
	| Some loc ->
		try f env t1 t2 with
		| TypeUnificationError (ut, ta, tb) -> type_error loc "%a" print_unification_error (ut, t1, t2, ta, tb)

let check_eqtype = protect_unification_error check_eqtype
let check_subtype = protect_unification_error check_subtype
let unify_types = protect_unification_error unify_types

(* Returns the most general unifier of a list of types *)
let unify_types_list env l = List.fold_left (fun t (loc, t1) -> unify_types ~loc:loc env t t1) BottomType l

(* Get variable s from environment *)
let tcenv_get_var env s =
	try SMap.find s.txt env.tcenv_vars with Not_found -> type_error s.loc "Unbound variable: %s" s.txt

(* Get function definition s from environment *)
let tcenv_get_func env s =
	try SMap.find s.txt env.tcenv_funcs with Not_found -> type_error s.loc "Unbound function: %s" s.txt

(*
	 Bind variables with an expression of type t in environment.
	 Can produce a type error if there are an incorrect number of variables to unpack.
*)
let tcenv_bind_vars env vars t =
	let t = canonize_type env t in
	match vars.txt with
	| [] ->
		if t <> BaseType "unit" then
			type_error vars.loc "unit pattern used with non-unit type";
		env
	| [x] -> { env with tcenv_vars = SMap.add x (t, true) env.tcenv_vars }
	| l ->
		let lt =
			match t with
			| TupleType lt -> lt
			| _ -> type_error vars.loc "non-tuple type used with multiple variable binding"
		in
		if List.length l <> List.length lt then
			type_error vars.loc "variable binding with incorrect number of variables: expected %d, got %d"
				(List.length lt) (List.length l);
		let nvars = List.fold_left2 (fun nv x t -> SMap.add x (t, true) nv) env.tcenv_vars l lt in
		{ env with tcenv_vars = nvars }

(* Type a function, given the function to type its body *)
let type_lambda type_val vt env (vars, v) =
	(vars.txt, type_val (tcenv_bind_vars env vars vt) v)

(* Returns the type of elements when iterating over a value of type t; fails if t is not an iterable type *)
let iterator_type loc env t = match canonize_type env t with
	| SetType t | ListType t -> t
	| MapType (t1, t2) -> TupleType [t1; t2]
	| t -> type_error loc "Expected an iterable type but got %a" print_type t

(*
	 Given a lvalue [lv] and an expected type [t], refine the type of subterms of [lv] knowing the result is of the
	 expected type, eliminating [BottomType] where possible. This assumes that the type of [lv] is already compatible
	 with [t].
*)
let rec type_down_prop_lvalue env t lv = match lv.desc with
	| LNamedVar _ -> { desc = lv.desc ; typ = t }
	| LMapGet (lv1, v1) ->
		{ desc = LMapGet (type_down_prop_lvalue env (MapType (v1.typ, t)) lv1, v1) ; typ = t }

(* Same of [type_down_prop_lvalue], but with values *)
let rec type_down_prop_value env t v = match v.desc with
	| NamedVar _ | LocalVar _ | VUnit | Func _ -> { desc = v.desc ; typ = t }
	| VTuple l ->
		(match canonize_type env t with
		 | TupleType tl ->
			 assert (List.length l = List.length tl);
			 { desc = VTuple (List.map2 (type_down_prop_value env) tl l) ; typ = t }
		 | _ -> assert false)
	| VSet l ->
		(match canonize_type env t with
		 | SetType t1 -> { desc = VSet (List.map (type_down_prop_value env t1) l) ; typ = t }
		 | _ -> assert false)
	| VList l ->
		(match canonize_type env t with
		 | ListType t1 -> { desc = VList (List.map (type_down_prop_value env t1) l) ; typ = t }
		 | _ -> assert false)
	| VWith (c, v) -> { desc = VWith (c, type_down_prop_value env t v) ; typ = t }
	| MapGet (v1, v2) ->
		{ desc = MapGet (type_down_prop_value env (MapType (v2.typ, t)) v1, v2) ; typ = t }
	| VLet (v1, (args, body)) ->
		{ desc = VLet (v1, (args, type_down_prop_value env t body)) ; typ = t }
	| VJoin (v1, (args, body)) ->
		{ desc = VJoin (v1, (args, type_down_prop_value env t body)) ; typ = t }
	| VIsEmpty (v1, body) ->
		{ desc = VIsEmpty (v1, type_down_prop_value env t body) ; typ = t }

(*
	 Smart constructor for [MapGet], automatically doing the type-checking and inferring the type.
	 If a type error is found and [loc] is defined, a type error is reported at [loc].
*)
let make_mapget ?loc env v1 v2 =
	let (t1, t2) = match canonize_type env v1.typ with
		| MapType (t1, t2) -> (t1, t2)
		| t -> match loc with
			| None -> failwith "Type error: map expected"
			| Some (loc1, _) -> type_error loc1 "Expected a map but got %a" print_type t
	in
	check_subtype ?loc:(match loc with None -> None | Some (_, loc2) -> Some loc2) env v2.typ t1;
	let v2 = type_down_prop_value env t1 v2 in
	{ desc = MapGet (v1, v2) ; typ = t2 }

(*
	 Same as previously, but with [LMapGet].
*)
let make_lmapget ?loc env v1 v2 =
	let (t1, t2) = match canonize_type env v1.typ with
		| MapType (t1, t2) -> (t1, t2)
		| t -> match loc with
			| None -> failwith "Type error: map expected"
			| Some (loc1, _) -> type_error loc1 "Expected a map but got %a" print_type t
	in
	check_subtype ?loc:(match loc with None -> None | Some (_, loc2) -> Some loc2) env v2.typ t1;
	let v2 = type_down_prop_value env t1 v2 in
	{ desc = LMapGet (v1, v2) ; typ = t2 }


(*
	 Convert a parsed value/lvalue into an lvalue, and type it.
	 Will raise an error if the input is not an lvalue.
*)
let rec type_lvalue env lv =
	match lv.txt with
	| PNamedVar s ->
		let t, is_local = tcenv_get_var env s in
		if is_local then type_error lv.loc "Local variable used in lvalue position";
		{ desc = LNamedVar s.txt ; typ = t }
	| PMapGet (lv, v) ->
		let tlv = type_lvalue env lv in
		let tv = type_value env v in
		make_lmapget ~loc:(lv.loc, v.loc) env tlv tv
	| _ -> type_error lv.loc "Incorrect term in lvalue position"

(* Convert a parsed value to its typed form *)
and type_value env v =
	match v.txt with
	| PNamedVar s ->
		let t, is_local = tcenv_get_var env s in
		{ desc = if is_local then LocalVar s.txt else NamedVar s.txt ; typ = t }
	| PMapGet (m, v) ->
		let tm = type_value env m in
		let tv = type_value env v in
		make_mapget ~loc:(m.loc, v.loc) env tm tv
	| PFunc (f, args) ->
		let args = List.map (fun v -> (v.loc, type_value env v)) args in
		let args, typ =
			(* Special-case treatment of the polymorphic "subterm" function, of type 'a program_point -> 'a *)
			if f.txt = "subterm" then begin
				let argloc, arg = match args with
					| [arg] -> arg
					| _ ->
						type_error v.loc "Incorrect number of arguments: subterm expected one argument but got %d."
							(List.length args)
				in
				match canonize_type env arg.typ with
				| Program_point t -> [arg], t
				| _ ->
					type_error argloc "Incorrect argument type: subterm expects a program_point as argument but got a %a."
						print_type arg.typ
			end else begin
				let q = tcenv_get_func env f in
				if List.length args <> List.length q.q_inputs then begin
					type_error v.loc "Incorrect number of arguments: %s expected %d arguments but got %d."
						f.txt (List.length q.q_inputs) (List.length args)
				end;
				List.iter2 (fun (argloc, arg) t -> check_subtype ~loc:argloc env arg.typ t) args q.q_inputs;
				let args = List.map2 (fun tp (_, arg) -> type_down_prop_value env tp arg) q.q_inputs args in
				args, q.q_output
			end
		in
		{ desc = Func (f.txt, args) ; typ = typ }
	| PVLet (v, body) ->
		let v = type_value env v in
		let body = type_lambda type_value v.typ env body in
		{ desc = VLet (v, body) ; typ = lambda_type body }
	| PVWith (c, v) ->
		let c = type_constr env c in
		let v = type_value env v in
		{ desc = VWith (c, v) ; typ = v.typ }
	| PVTuple l ->
		let l = List.map (type_value env) l in
		{ desc = VTuple l ; typ = TupleType (List.map (fun v -> v.typ) l) }
	| PVSet l ->
		let tl = List.map (type_value env) l in
		{ desc = VSet tl ; typ = SetType (unify_types_list env (List.map2 (fun x y -> (x.loc, y.typ)) l tl)) }
	| PVList l ->
		let tl = List.map (type_value env) l in
		{ desc = VList tl ; typ = ListType (unify_types_list env (List.map2 (fun x y -> (x.loc, y.typ)) l tl)) }
	| PVJoin (v, body) ->
		let tv = type_value env v in
		let body = type_lambda type_value (iterator_type v.loc env tv.typ) env body in
		{ desc = VJoin (tv, body) ; typ = lambda_type body }
	| PVUnit -> { desc = VUnit ; typ = BaseType "unit" }

(* Convert a parsed constraint to its typed form *)
and type_constr env c =
	match c.txt with
	| PTrue -> True
	| PConj (c1, c2) -> Conj (type_constr env c1, type_constr env c2)
	| PCLet (v, body) ->
		let v = type_value env v in
		let body = type_lambda type_constr v.typ env body in
		CLet (v, body)
	| PSub (v1, v2) ->
		let tv1 = type_value env v1 in
		let tv2 = type_lvalue env v2 in
		check_eqtype ~loc:v2.loc env tv1.typ tv2.typ;
		Sub (tv1, tv2)
	| PForall (v, body) ->
		let tv = type_value env v in
		Forall (tv, type_lambda type_constr (iterator_type v.loc env tv.typ) env body)


(* Create a constraint type from a skeleton type *)
let make_type tenv s =
	match s with
	| Variable _ -> failwith "Polymorphism not implemented"
	| Sort s when s.s_kind = Variant ->
			SetType (Program_point (BaseType s.s_name))
	| Sort s when is_base_sort tenv s.s_name ->
			SetType (Program_point (BaseType s.s_name))
	| Sort s ->
			BaseType s.s_name

(* Returns the expected input and output types of the filter functions *)
let make_filter_type tenv f =
	let f = SMap.find f tenv.env_filter_signatures in
	let it = TupleType (Program_point TopType :: BaseType "globalcontext" :: BaseType "localcontext" ::
											List.map (make_type tenv) f.fs_input_sorts)
	in
	let ot = ListType (make_tuple_type (BaseType "localcontext" :: List.map (make_type tenv) f.fs_output_sorts)) in
	(it, ot)

(* Returns the expected input and output types of the "hook in" functions *)
let make_hook_intype tenv env hname =
	let h = SMap.find hname tenv.env_hooks in
	let it = TupleType (
			Program_point TopType :: BaseType "globalcontext" :: BaseType "localcontext" ::
			(List.map (fun v -> make_type tenv (v.tv_typ)) h.hs_input_vars))
	in
	let ot = ListType (TupleType [BaseType "globalcontext"; fst (SMap.find hname env.tcenv_rulevars)]) in
	(it, ot)

(* Returns the expected input and output types of the "hook out" functions *)
let make_hook_outtype tenv env hname =
	let h = SMap.find hname tenv.env_hooks in
	let it = TupleType [
			Program_point TopType; BaseType "globalcontext"; BaseType "localcontext";
			snd (SMap.find hname env.tcenv_rulevars)]
	in
	let ot = ListType (TupleType (BaseType "localcontext" :: List.map (make_type tenv) h.hs_output_sorts)) in
	(it, ot)

(* Returns the expected input and output types of the "rule in" functions *)
let make_rule_intype tenv env hname =
	let h = SMap.find hname tenv.env_hooks in
	let term_sort =
		match h.hs_matching.tv_typ with
		| Sort s when s.s_kind=Variant ->
				s.s_name
		| _ -> assert false (* Matching on a non-variant type, should be caught at typing *)
	in
	let it = TupleType [
			Program_point (BaseType term_sort); BaseType "globalcontext";
			fst (SMap.find hname env.tcenv_rulevars)]
	in
	let input_vars = List.filter (fun v -> v.tv_name <> h.hs_matching.tv_name) h.hs_input_vars in
	let ot = ListType (
			TupleType (BaseType "localcontext" :: List.map (fun v -> make_type tenv v.tv_typ) input_vars))
	in
	(it, ot)

(* Returns the expected input and output types of the "rule out" functions *)
let make_rule_outtype tenv env hname =
	let h = SMap.find hname tenv.env_hooks in
	let term_sort =
		match h.hs_matching.tv_typ with
		| Sort s when s.s_kind=Variant ->
				s.s_name
		| _ -> assert false (* Matching on a non-variant type, should be caught at typing *)
	in
	let it = TupleType (
			Program_point (BaseType term_sort) :: BaseType "globalcontext" :: BaseType "localcontext" ::
			List.map (make_type tenv) h.hs_output_sorts)
	in
	let ot = snd (SMap.find hname env.tcenv_rulevars) in
	(it, ot)

let rec check_no_bot_type env loc t =
	let t = canonize_type env t in
	match t with
	| TopType -> assert false
	| BottomType -> type_error loc "The type of a subexpression of this expression is not fully known"
	| Program_point _ | BaseType _ -> ()
	| ListType t | SetType t -> check_no_bot_type env loc t
	| TupleType l -> List.iter (check_no_bot_type env loc) l
	| MapType (t1, t2) -> check_no_bot_type env loc t1; check_no_bot_type env loc t2

let rec check_no_bot_value env loc v =
	check_no_bot_type env loc v.typ;
	match v.desc with
	| VUnit | LocalVar _ | NamedVar _ -> ()
	| VTuple l | VSet l | VList l | Func (_, l) -> List.iter (check_no_bot_value env loc) l
	| MapGet (v1, v2) | VIsEmpty (v1, v2) -> check_no_bot_value env loc v1; check_no_bot_value env loc v2
	| VLet (v, body) | VJoin (v, body) ->
		check_no_bot_value env loc v; check_no_bot_value env loc (snd body)
	| VWith (c, v) -> check_no_bot_value env loc v; check_no_bot_constr env loc c

and check_no_bot_constr env loc c =
	match c with
	| Sub (v, lv) -> check_no_bot_value env loc v; check_no_bot_lvalue env loc lv
	| Conj (c1, c2) -> check_no_bot_constr env loc c1; check_no_bot_constr env loc c2
	| True -> ()
	| CLet (v, body) | Forall (v, body) ->
		check_no_bot_value env loc v; check_no_bot_constr env loc (snd body)
	| CEmpty (v, body) ->
		check_no_bot_value env loc v; check_no_bot_constr env loc body

and check_no_bot_lvalue env loc lv =
	check_no_bot_type env loc lv.typ;
	match lv.desc with
	| LNamedVar _ -> ()
	| LMapGet (m, v) -> check_no_bot_lvalue env loc m; check_no_bot_value env loc v

(* Type a filter function and produce a lambda representing it *)
let type_fc tenv env fc = {
	fc_name = fc.pfc_name ;
	fc_value =
		let it, ot = make_filter_type tenv fc.pfc_name in
		let v = type_lambda type_value it env fc.pfc_value in
		check_subtype ~loc:(snd fc.pfc_value).loc env (lambda_type v) ot;
		let (args, body) = v in
		let v = (args, type_down_prop_value env ot body) in
		check_no_bot_value env (snd fc.pfc_value).loc (snd v);
		v
}

(* Type a "hook in" function and produce a lambda representing it *)
let type_hi tenv env hi = {
	hi_name = hi.phi_name ;
	hi_annot = hi.phi_annot ;
	hi_value =
		let it, ot = make_hook_intype tenv env hi.phi_name in
		let v = type_lambda type_value it env hi.phi_value in
		check_subtype ~loc:(snd hi.phi_value).loc env (lambda_type v) ot;
		let (args, body) = v in
		let v = (args, type_down_prop_value env ot body) in
		check_no_bot_value env (snd hi.phi_value).loc (snd v);
		v
}

(* Type a "hook out" function and produce a lambda representing it *)
let type_ho tenv env ho = {
	ho_name = ho.pho_name ;
	ho_annot = ho.pho_annot ;
	ho_value =
		let it, ot = make_hook_outtype tenv env ho.pho_name in
		let v = type_lambda type_value it env ho.pho_value in
		check_subtype ~loc:(snd ho.pho_value).loc env (lambda_type v) ot;
		let (args, body) = v in
		let v = (args, type_down_prop_value env ot body) in
		check_no_bot_value env (snd ho.pho_value).loc (snd v);
		v
}

(* Type a "rule in" function and produce a lambda representing it *)
let type_ri tenv env ri = {
	ri_name = ri.pri_name ;
	ri_value =
		let it, ot = make_rule_intype tenv env ri.pri_name in
		let v = type_lambda type_value it env ri.pri_value in
		check_subtype ~loc:(snd ri.pri_value).loc env (lambda_type v) ot;
		let (args, body) = v in
		let v = (args, type_down_prop_value env ot body) in
		check_no_bot_value env (snd ri.pri_value).loc (snd v);
		v
}

(* Type a "rule out" function and produce a lambda representing it *)
let type_ro tenv env ro = {
	ro_name = ro.pro_name ;
	ro_value =
		let it, ot = make_rule_outtype tenv env ro.pro_name in
		let v = type_lambda type_value it env ro.pro_value in
		check_subtype ~loc:(snd ro.pro_value).loc env (lambda_type v) ot;
		let (args, body) = v in
		let v = (args, type_down_prop_value env ot body) in
		check_no_bot_value env (snd ro.pro_value).loc (snd v);
		v
}


(* Create an environment containing the various quark and variable declarations, for use in typing *)
let make_tcenv decls =
	let rulevars = SMap.merge (fun _ v1 v2 -> match v1, v2 with Some v1, Some v2 -> Some (v1, v2) | _ -> assert false)
			(List.fold_left (fun m (name, t) -> SMap.add name.txt t.txt m) SMap.empty decls.pc_rulein_vars)
			(List.fold_left (fun m (name, t) -> SMap.add name.txt t.txt m) SMap.empty decls.pc_ruleout_vars)
	in
	{
		tcenv_basetypes = List.fold_left
				(fun m (name, l) -> SMap.add name.txt
						(List.map (fun (s, c, j) -> (s.txt, c, j)) l) m) SMap.empty decls.pc_typedecls ;
		tcenv_types = List.fold_left (fun m (name, t) -> SMap.add name.txt t.txt m) SMap.empty decls.pc_types ;
		tcenv_vars = List.fold_left
				(fun m v -> SMap.add v.vc_name.txt (v.vc_type.txt, false) m) SMap.empty decls.pc_vars ;
		tcenv_rulevars = rulevars ;
		tcenv_funcs = List.fold_left (fun m q -> SMap.add q.q_name q m) SMap.empty decls.pc_quarks ;
	}

(* Create an environment containing all typed filters and various hook/rule functions *)
let make_cenv tenv tcenv decls = {
	ce_tce = tcenv ;
	ce_tenv = tenv ;
	ce_filts = List.fold_left (fun m f -> SMap.add f.pfc_name (type_fc tenv tcenv f) m) SMap.empty decls.pc_filters ;
	ce_hookins = List.fold_left (fun m h -> SMap.add (h.phi_name ^ "^" ^ h.phi_annot) (type_hi tenv tcenv h) m) SMap.empty decls.pc_hookins ;
	ce_hookouts = List.fold_left (fun m h -> SMap.add (h.pho_name ^ "^" ^ h.pho_annot) (type_ho tenv tcenv h) m) SMap.empty decls.pc_hookouts ;
	ce_ruleins = List.fold_left (fun m r -> SMap.add r.pri_name (type_ri tenv tcenv r) m) SMap.empty decls.pc_ruleins ;
	ce_ruleouts = List.fold_left (fun m r -> SMap.add r.pro_name (type_ro tenv tcenv r) m) SMap.empty decls.pc_ruleouts ;
}
