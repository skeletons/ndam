(** Instructions on how to export filters. **)

(** Given a type for variables, these chains produce strings. **)
type 'a item =
  | Var of 'a
  | String of string
type 'a output = 'a item list

(** Print the given chain. **)
val print : ('a -> string option) -> 'a output -> string option

(** This record stores all the information needed to export a filter. **)
type 'a filter = {
    inputs : 'a list ;
    fresh : 'a list ;
    instructions : 'a output list ;
    assume : 'a output option ;
    return : 'a output list
  }

(** Each filter is associated an output. **)
type 'a env = 'a filter Util.SMap.t

