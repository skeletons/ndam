(** Miscellaneous declarations *)

module SMap : Map.S with type key = String.t
module SSet : Set.S with type elt = String.t

(** compare lists of comparable elements *)
val list_compare: ('a -> 'a -> int) -> ('a list -> 'a list -> int)

(** Union of a list of SSet *)
val sset_list_union: SSet.t list -> SSet.t

(** The identity function. *)
val id : 'a -> 'a

(** The function composition *)
val comp: ('a -> 'b) -> ('b -> 'c) -> ('a -> 'c)

(* Take n list of lists and return a list of (n-lists)
 * e.g.: [[a; b] [c] [d; e]] →
 *  [[a; c; d]; [a; c; e]; [b; c; d]; [b; c; e]] (maybe in another order) *)
val product: 'a list list -> 'a list list

(** Return the domain of a map. *)
val smap_domain : 'a SMap.t -> string list

(** Iter all the way through the three lists, which must be of the same size. *)
val list_iter3 : ('a -> 'b -> 'c -> unit) -> 'a list -> 'b list -> 'c list -> unit

(** Same as List.map2, but with 3 lists. *)
val list_map3 : ('a -> 'b -> 'c -> 'd) -> 'a list -> 'b list -> 'c list -> 'd list

(** Return a list such that [(a, b)] is in the final list iff [a] was in the first list
 * and [b] in the second list. *)
val list_square : 'a list -> 'b list -> ('a * 'b) list

(** Map the list.
 * If any of the element of the list maps to [None], then the whole list
 * is mapped to [None]. *)
val list_map_option : ('a -> 'b option) -> 'a list -> 'b list option

(** Given a function [f] and name, returns a name [n] such that [f n].
 * The name will be inspired from the argument name.
 * This function may not terminate if the given function returns [false]
 * on an infinite number of values. *)
val fresh : (string -> bool) -> string -> string

(** Return another list with the same elements but where all element appear at most once.
 * The order in the original list is preserved if considering only the first occurence of
 * each element. *)
val uniq : 'a list -> 'a list

(** If the list has a duplicated element, return one of them. *)
val find_duplicate : string list -> string option

(** If the lists overlap, return an element that is in both lists . *)
val find_overlap : 'a list -> 'a list -> 'a option

(** Find an element in a list and return its index, or [None] if the element is not there. *)
val find_index_opt : 'a -> 'a list -> int option

(** Replace the [n]th element of the list by a particular element.
 * Return [None] if the list is two short. *)
val write_nth : int -> 'a -> 'a list -> 'a list option

(** Return a list from [0] to the argument excluded. *)
val seq : int -> int list

(* Printing a list using Format *)
val print_list: ?sep:string -> ?empty:(Format.formatter -> unit) ->
  (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit
