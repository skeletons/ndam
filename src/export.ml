open Util
open Skeltypes
open Skeleton

module type Exportator = sig
		type t
		type hint
		val export : Skeltypes.env -> hint -> t
		val print : t -> string
	end

module Gil = struct

	(** * Type defininitions **)

	type gil_var = string
	type proc_name = string
	type label = string
	type symbol = string
	type action = string

	type unop =
		| Not

	type binop =
		| Eq
		| Add
		| Sub
		| LNth
		| LSub

	(** GIL values. **)
	type value =
		| VFloat of float
		| VInt of int
		| VString of string
		| VBool of bool
		| VSymbol of symbol
		(* | VType of gil_type *)
		| VProc of proc_name

	(** GIL expressions. **)
	type expr =
		| Val of value
		| Var of gil_var
		| UnOp of unop * expr
		| BinOp of binop * expr * expr
		| VList of expr list
		| Raw of string

	(** GIL commands. **)
	type command =
		| CAsn of gil_var * expr
		| CIfGoto of expr * label * label
		| CDynCall of gil_var * expr * expr list
		| CAction of gil_var * action * expr list
		(* | CFresh of gil_var * bool (** The boolean states whether the fresh symbol is interpretated. **) *)
		| CReturn
		| CFail of string * expr list
		| CAssume of gil_var
		| CAssumeFalse
		| CRaw of string

	(** Procedure body.
	 * It is composed of a list of commands, which can be associated a label. **)
	type body = (label option * command) list

	(** GIL procedures. **)
	type proc = {
			proc_name : proc_name ;
			proc_arguments : string list ;
			proc_body : body
		}

	type t = proc list

	type hint = string ExportInstr.env

	(** * Utilities **)

	(** Reserved keywords in GIL. **)
	let keywords = [
			"$$LocalTime" ;
			"$$UTCTime" ;
			"$$e" ;
			"$$ln10" ;
			"$$ln2" ;
			"$$log10e" ;
			"$$log2e" ;
			"$$max_float" ;
			"$$min_float" ;
			"$$pi" ;
			"$$random" ;
			"$$scope" ;
			"$$sqrt1_2" ;
			"$$sqrt2" ;
			"--e--" ;
			"--s--" ;
			"-d-" ;
			"-e-" ;
			"-i-" ;
			"-s-" ;
			"-u-" ;
			"Bool" ;
			"Empty" ;
			"Extensible" ;
			"False" ;
			"List" ;
			"MetaData" ;
			"None" ;
			"Null" ;
			"Num" ;
			"Obj" ;
			"PHI" ;
			"Set" ;
			"Str" ;
			"True" ;
			"Type" ;
			"Undefined" ;
			"_" ;
			"and" ;
			"apply" ;
			"args" ;
			"assert" ;
			"assume" ;
			"assume_type" ;
			"bind" ;
			"bispec" ;
			"branch" ;
			"callspec" ;
			"car" ;
			"cdr" ;
			"closure" ;
			"delete" ;
			"deleteObject" ;
			"else" ;
			"emp" ;
			"empty" ;
			"empty_fields" ;
			"error" ;
			"existentials" ;
			"extern" ;
			"fail" ;
			"false" ;
			"flash" ;
			"fold" ;
			"forall" ;
			"getFields" ;
			"goto" ;
			"hasField" ;
			"if" ;
			"import" ;
			"inf" ;
			"invariant" ;
			"isNaN" ;
			"js_only_spec" ;
			"js_only_spec" ;
			"l-len" ;
			"l-nth" ;
			"l-rev" ;
			"l-sub" ;
			"l-sub" ;
			"lemma" ;
			"m_abs" ;
			"m_acos" ;
			"m_asin" ;
			"m_atan" ;
			"m_atan2" ;
			"m_ceil" ;
			"m_cos" ;
			"m_exp" ;
			"m_floor" ;
			"m_log" ;
			"m_round" ;
			"m_sgn" ;
			"m_sin" ;
			"m_sqrt" ;
			"m_tan" ;
			"macro" ;
			"main" ;
			"make-symbol-bool" ;
			"make-symbol-number" ;
			"make-symbol-string" ;
			"metadata" ;
			"nan" ;
			"new" ;
			"nil" ;
			"none" ;
			"normal" ;
			"not" ;
			"not" ;
			"null" ;
			"num_to_int" ;
			"num_to_int32" ;
			"num_to_string" ;
			"num_to_uint16" ;
			"num_to_uint32" ;
			"o_chains" ;
			"only" ;
			"or" ;
			"outcome" ;
			"post" ;
			"pre" ;
			"pred" ;
			"proc" ;
			"pure" ;
			"ret" ;
			"return" ;
			"s-len" ;
			"s-nth" ;
			"sc_scope" ;
			"schain" ;
			"scope" ;
			"sep_apply" ;
			"sep_assert" ;
			"set_to_list" ;
			"skip" ;
			"spec" ;
			"spec_var" ;
			"string_to_num" ;
			"then" ;
			"this" ;
			"throw" ;
			"true" ;
			"typeOf" ;
			"types" ;
			"undefined" ;
			"unfold" ;
			"unfold" ;
			"unfold_all" ;
			"use_subst" ;
			"variant" ;
			"with" ;
		]

	(** Return all the labels of a body. **)
	let get_labels body =
		List.concat (List.map (fun (label, _) ->
			match label with
			| None -> []
			| Some label -> [label]) body)

	(** * Printing **)

	let print_unop = function
		| Not -> "not"

	let print_binop = function
		| Eq -> "="
		| Add -> "+"
		| Sub -> "-"
		| LNth -> "l-nth"
		| LSub -> "l-sub"

	let infix = function
		| Eq | Add | Sub -> true
		| LNth | LSub -> false

	let rec print_val = function
		| VFloat f -> string_of_float f
		| VInt i -> string_of_int i
		| VString str -> "\"" ^ String.escaped str ^ "\""
		| VBool b -> if b then "true" else "false"
		| VSymbol symbol -> symbol
		| VProc proc -> proc

	let rec print_expr = function
		| Val v -> print_val v
		| Var x -> x
		| UnOp (o, e) -> print_unop o ^ " " ^ Printer.add_parentheses (print_expr e)
		| BinOp (o, e1, e2) ->
			if infix o then (
				Printer.add_parentheses (print_expr e1)
				^ " " ^ print_binop o ^ " "
				^ Printer.add_parentheses (print_expr e2)
			) else (
				print_binop o
				^ " (" ^ print_expr e1 ^ ", " ^ print_expr e2 ^ ")"
			)
		| VList l ->
			if l <> [] then
				"{{ " ^ String.concat ", " (List.map print_expr l) ^ " }}"
			else "{{ }}"
		| Raw str -> str

	let print_command next = function
		| CAsn (x, e) -> x ^ " := " ^ print_expr e
		| CIfGoto (Val (VBool true), l, _) ->
			if next <> Some l then "goto " ^ l else "skip"
		| CIfGoto (Val (VBool false), _, l) ->
			if next <> Some l then "goto " ^ l else "skip"
		| CIfGoto (e, l1, l2) -> "goto [" ^ print_expr e ^ "] " ^ l1 ^ " " ^ l2
		| CDynCall (x, e, l) ->
			x ^ " := " ^ Printer.add_parentheses (print_expr e)
								 ^ " (" ^ String.concat ", " (List.map print_expr l) ^ ")"
		| CAction (x, a, l) -> x ^ " := [" ^ a ^ "] (" ^ String.concat ", " (List.map print_expr l) ^ ")"
		(* | CFresh (x, interp) -> x ^ " := " ^ if interp then "fresh" else "symb" *)
		| CReturn -> "return"
		| CFail (msg, l) -> "fail [" ^ msg ^ "] (" ^ String.concat ", " (List.map print_expr l) ^ ")"
		| CAssume x -> "assume [" ^ x ^ " == true]"
		| CAssumeFalse -> "assume [False]"
		| CRaw i -> i

	let print_body body =
		let indent =
			List.fold_left (fun i (label, _) ->
				match label with
				| None -> i
				| Some label -> max i (String.length label)) 0 body in
		let get_next_label = function
			| [] -> None
			| (label, _) :: _ -> label in
		let rec aux = function
			| [] -> []
			| (label, cmd) :: body ->
				let indent =
					match label with
					| Some name ->
						"  " ^ name ^ ": " ^ String.make (indent - String.length name) ' '
					| None -> String.make (4 + indent) ' ' in
				(indent ^ print_command (get_next_label body) cmd) :: aux body in
		String.concat (" ;\n") (aux body)

	(** Print a GIL procedure **)
	let print_proc proc =
		"proc " ^ proc.proc_name
		^ " (" ^ String.concat ", " proc.proc_arguments ^ ") {\n"
		^ print_body proc.proc_body ^ "\n} ;\n"

	let print gil =
		String.concat "\n" (List.map print_proc gil)

	(** * Exportation **)

	(** To help the exportation, we introduce this structure where labels and fresh
	 * identifiers are abstracted out. **)
	module M : sig
			type t

			(** An empty body. **)
			val empty : t

			(** Assign the following variable to the given expression. **)
			val asn : gil_var -> expr -> t -> t

			(** Assign the following variable to return value of this function
			 * called with these arguments. **)
			val call : gil_var -> proc_name -> expr list -> t -> t

			(** Assign the following variable to return value of this action
			 * called with these arguments. **)
			val action : gil_var -> action -> expr list -> t -> t

			(** Immediately return. **)
			val return : expr -> t

			(** Fail with a message and a list of variables to be printed. **)
			val fail : string -> gil_var list -> t

			(** Assuming something, cutting the branches where it is not the case
			 * (but without producing an error). **)
			val assume : expr -> t -> t

			(** Vanish, which is just a shortcut for [assume false]. **)
			val vanish : t

			(** Print a raw GIL command. **)
			val raw : string -> t -> t

			(** Create a fresh identifier. **)
			val fresh : string -> (string -> t) -> t

			(** Build an if-expression. **)
			val build_if : expr -> t -> t -> t

			(** Build a while-expression. **)
			val build_while : expr -> t -> t

			(** Sequence two expressions. **)
			val seq : t -> t -> t

			(** Given a list of already taken identifiers, produce a GIL body. **)
			val extract : t -> SSet.t -> body
		end = struct
			(** The type [t] is a function taking the set of reserved names, the name of
			 * the label immediately after the current subbody.
			 * It then returns a new set of reserved names and a body as well as a boolean.
			 * The boolean states whether the given label is actually never jumped to.
			 * The returned body is never empty. **)
			type t = SSet.t -> string -> SSet.t * body * bool

			let end_of_flow = CFail ("ReachingEndOfFlow", [])

			(** Simplify the given GIL program. **)
			let rec simplify = function
				| [] -> []
				| (None, CIfGoto (_, jmp1, jmp2)) :: (((Some jmp3, _) :: _) as l)
					when jmp1 = jmp2 && jmp2 = jmp3 ->
					simplify l
				| (Some lbl, CIfGoto (_, jmp1, jmp2)) :: l when jmp1 = jmp2 ->
					(Some lbl, CIfGoto (Val (VBool true), jmp1, jmp1)) :: simplify l
				| instr :: l -> instr :: simplify l

			let extract gen taken =
				let fail = fresh (fun name -> not (SSet.mem name taken)) "fail" in
				let taken = SSet.add fail taken in
				let (_, body, finished) = gen taken fail in
				let body = simplify body in
				if not finished then body @ [(Some fail, end_of_flow)] else body

			let empty taken jmp =
				(taken, [(None, CIfGoto (Val (VBool true), jmp, jmp))], false)

			let print cmd gen taken label =
				let (taken, body, finished) = gen taken label in
				(taken, (None, cmd) :: body, finished)

			let asn x e = print (CAsn (x, e))
			let call x f args = print (CDynCall (x, Val (VProc f), args))
			let action x a args = print (CAction (x, a, args))

			let return e taken _ = (taken, [(None, CAsn ("ret", e)); (None, CReturn)], true)

			let fail msg l taken _ =
				let l = List.map (fun x -> Var x) l in
				(taken, [(None, CFail (msg, l))], true)

			let assume e gen taken label =
				let x = fresh (fun name -> not (SSet.mem name taken)) "assume" in
				let taken = SSet.add x taken in
				let (taken, after, finished) = gen taken label in
				(taken, (None, CAsn (x, e)) :: (None, CAssume x) :: after, finished)

			let vanish taken _ = (taken, [(None, CAssumeFalse)], true)

			let raw instr = print (CRaw instr)

			(** Given a set of reserved names, a label name to be inspired on,
			 * and a body, return:
			 * - an updated set of reserved names,
			 * - an updated body with the same structure than the provided one,
			 * - a label present at the beginning of the body. **)
			let get_label taken name = function
				| [] -> assert false
				| ((Some label, _) :: _) as body -> (taken, body, label)
				| (None, cmd) :: body ->
					let label = fresh (fun name -> not (SSet.mem name taken)) name in
					(SSet.add label taken, (Some label, cmd) :: body, label)

			let seq gen1 gen2 taken label =
				let (taken, body2, finished2) = gen2 taken label in
				let (taken, body2, label2) = get_label taken "then" body2 in
				let (taken, body1, finished1) = gen1 taken label2 in
				(taken, (if not finished1 then body1 @ body2 else body1), finished1 || finished2)

			let build_if e gen_true gen_false taken label =
				let (taken, false_case, false_finished) = gen_false taken label in
				let (taken, true_case, true_finished) = gen_true taken label in
				let (taken, false_case, false_label) = get_label taken "false" false_case in
				let (taken, true_case, true_label) = get_label taken "true" true_case in
				(taken, (None, CIfGoto (e, true_label, false_label))
				 :: false_case @ true_case,
				 true_finished && false_finished)

			let build_while e gen taken label =
				let test = fresh (fun name -> not (SSet.mem name taken)) "while" in
				let taken = SSet.add test taken in
				let (taken, body, finished) = gen taken label in
				let (taken, body, body_label) = get_label taken "while_body" body in
				let body =
					(Some test, CIfGoto (e, body_label, label))
					:: if not finished then
							 body @ [(None, CIfGoto (Val (VBool true), test, test))]
						 else body in
				(taken, body, false)

			let fresh name cont taken =
				let name = fresh (fun name -> not (SSet.mem name taken)) name in
				let taken = SSet.add name taken in
				cont name taken

		end

	module PSMap = Map.Make (struct type t = string * string option let compare = compare end)

	let get_var ve x =
		match SMap.find_opt x.tv_name ve with
		| None -> assert false
		| Some name -> name

	(** Given a skeletal term and a continuation, produce a [M.t].
	 * The continuation is then called with the name of the GIL variable where the term
	 * is stored as an argument.
	 * This function furthermore takes as argument:
	 * - a mapping from skeletal constructor to pairs of GIL actions (construction and
	 *   destruction) and arity,
	 * - a function associating skeletal variables to their GIL variable name. **)
	let rec export_term constr_names ve t cont =
		match t.v_desc with
		| VVar x -> cont (get_var ve x)
		| VConstr (c, _::_, ts) ->
				failwith "Variant polymorphic types not supported on this function"
		| VConstr (c, [], ts) ->
			let rec aux names = function
				| [] ->
					M.fresh ("t_" ^ c) (fun ret ->
						let cons =
							match SMap.find_opt c constr_names with
							| None -> assert false
							| Some (cons, des, arity) -> cons in
						let names = List.rev names in
						M.call ret cons (List.map (fun x -> Var x) names)
						(cont ret))
				| t :: ts ->
					export_term constr_names ve t (fun ret ->
						aux (ret :: names) ts) in
			aux [] ts

	(** Given a skeletal value and a continuation, produce a [M.t].
	 * The continuation is then called with the name of the GIL variable where the value
	 * is stored as an argument.
	 * This function furthermore thakes as argument:
	 * - a mapping from skeletal constructor to pairs of GIL actions (construction and destruction),
	 * - a function associating skeletal variables to their GIL variable name. **)
	let export_value = export_term

	(** Given a list of values and a continuation taking a list of expressions as argument,
	 * produce a [M.t]. **)
	let export_values constr_names ve l cont =
		let rec aux rets = function
			| [] -> cont (List.rev rets)
			| v :: l ->
				export_value constr_names ve v
				(fun ret -> aux (ret :: rets) l) in
		aux [] l

	(** Given a GIL list and a number of expected size, call the continuation on
	 * a list of GIL variables holding each components of the list. **)
	let decouple x ?start:(start = 0) size cont =
		let rec aux rets i =
			if i = size then
				cont (List.rev rets)
			else
				M.fresh (x ^ "_proj_" ^ string_of_int i) (fun pr ->
					M.asn pr (BinOp (LNth, Var x, Val (VInt i)))
					(aux (pr :: rets) (1 + i))) in
		aux [] start

	(** Create a [M.t] from a skeleton. **)
	let rec extract_skeleton env env_names filter_env non_det ve skel cont =
		match skel with
		| Computation b ->
			extract_bone env env_names filter_env non_det ve b cont
		| LetIn (a, xs, b, skel) ->
			(* For now, let’s forget about the annotation.
			 * Later on, it would be a function called on the result of the bone,
			 * which might throw. *)
			extract_bone env env_names filter_env non_det ve b (fun l ->
				let rec aux ve l xs =
					match l, xs with
					| [], [] ->
						extract_skeleton env env_names filter_env non_det ve skel cont
					| y :: l, x :: xs ->
						let ve = SMap.add x.tv_name y ve in
						aux ve l xs
					| _, _ -> assert false in
				aux ve l xs)

	(** Create a [M.t] from a bone. **)
	and extract_bone env env_names filter_env non_det ve b cont =
		let (constr_names, filter_names, hook_names, proc_names) = env_names in
		match b with
		| Hook h ->
			let (f, inputs) =
				let v = (Skeleton.get_term_hook env h) in
				match v.v_desc with
				| VVar x ->
					(match PSMap.find_opt (h.c_name, None) hook_names with
					 | None -> assert false
					 | Some f -> (f, h.c_inputs @ [v]))
				| VConstr(_, _::_, _) ->
					failwith "The function [extract_bone] doesn't support variant polymorphism"
				| VConstr (c, [], subterms) ->
					(match PSMap.find_opt (h.c_name, Some c) hook_names with
					 | None -> assert false
					 | Some f -> (f, h.c_inputs @ subterms)) in
			export_values constr_names ve inputs (fun args ->
				M.fresh "hook_ret" (fun ret ->
					let args = List.map (fun x -> Var x) args in
					M.call ret f args
					(decouple ret (List.length h.c_output_sorts) cont)))
		| Proc p ->
			let f =
				match SMap.find_opt p.c_name proc_names with
				| None -> assert false
				| Some f -> f in
			export_values constr_names ve p.c_inputs (fun args ->
				M.fresh "proc_ret" (fun ret ->
					let args = List.map (fun x -> Var x) args in
					M.call ret f args
					(decouple ret (List.length p.c_output_sorts) cont)))
		| Filter f ->
			export_values constr_names ve f.c_inputs (fun args ->
				match SMap.find_opt f.c_name filter_env with
				| None -> failwith ("Missing filter declaration: " ^ f.c_name ^ ".")
				| Some hint ->
					(** First, we reserve the names for the fresh variables. **)
					let rec aux allocated = function
						| x :: fresh ->
							M.fresh x (fun x -> aux (x :: allocated) fresh)
						| [] ->
							let print =
								let env =
									if List.length hint.ExportInstr.inputs
										 <> List.length args then
										failwith ("Mismatch hint inputs for filter " ^ f.c_name) ;
									let env =
										List.fold_left2 (fun m x y ->
											SMap.add x y m) SMap.empty hint.ExportInstr.inputs args in
									List.fold_left2 (fun m x y ->
										SMap.add x y m) env hint.ExportInstr.fresh (List.rev allocated) in
								fun o ->
									match ExportInstr.print (fun x -> SMap.find_opt x env) o with
									| Some str -> str
									| None -> assert false in
							let cont =
								let cont =
									(** Finally, we return the computed values. **)
									let rec aux rets = function
										| [], [] -> cont (List.rev rets)
										| o :: l, sort :: sorts ->
											M.fresh ("x_" ^ Printer.string_of_sort sort) (fun x ->
												M.asn x (Raw (print o))
												(aux (x :: rets) (l, sorts)))
										| _, _ ->
											failwith ("Mismatch hint outputs for filter " ^ f.c_name) in
									aux [] (hint.ExportInstr.return, f.c_output_sorts) in
								(** Third, we put or not an assume instruction. **)
								match hint.ExportInstr.assume with
								| None -> cont
								| Some o ->
									M.assume (Raw (print o)) cont in
							(** Second, we insert the requested instructions. **)
							let rec aux = function
								| [] -> cont
								| instr :: instrs ->
									M.raw (print instr) (aux instrs) in
							aux hint.ExportInstr.instructions in
					aux [] hint.ExportInstr.fresh)
		| Branching b ->
			(match b.b_skeletons with
			 (** Some small optimisations for cases with a small number of branches. **)
			 | [] -> M.vanish
			 | skel :: [] ->
				 extract_skeleton env env_names filter_env non_det ve skel cont
			 | _ ->
				 M.fresh "branch" (fun i ->
					 M.action i non_det [Val (VInt (List.length b.b_skeletons))]
					 (let rec aux names = function
							| [] ->
								let names = List.rev names in
								let body skel =
									extract_skeleton env env_names filter_env non_det ve skel (fun rets ->
										let rec aux names rets =
											match names, rets with
											| [], [] -> M.empty
											| name :: names, ret :: rets ->
												M.asn name (Var ret)
												(aux names rets)
											| _, _ -> assert false in
										aux names rets) in
								let rec aux j = function
									| [] -> M.vanish
									| skel :: [] ->
										M.assume (BinOp (Eq, Var i, Val (VInt j))) (body skel)
									| skel :: skels ->
										M.build_if (BinOp (Eq, Var i, Val (VInt j)))
											(body skel)
											(aux (1 + j) skels)
									in
								M.seq (aux 0 b.b_skeletons) (cont names)
							| s :: os ->
								 M.fresh ("ret_" ^ Printer.string_of_sort s) (fun name ->
									 aux (name :: names) os) in
						aux [] b.b_outputs_sorts)))
		| Return l ->
			export_values constr_names ve l cont

	(** Given a dispatch function for a given hook and a list of possible constructors. **)
	let rec create_dispatch constr_names hook_names h args t = function
		| [] -> M.fail ("InvalidConstructorForHook_" ^ h) [t]
		| c :: cons ->
			let (des, arity) =
				match SMap.find_opt c constr_names with
				| None -> assert false
				| Some (cons, des, arity) -> (des, arity) in
			let f =
				match PSMap.find_opt (h, Some c) hook_names with
				| None -> assert false
				| Some f -> f in
			M.fresh "match" (fun ret ->
				M.call ret des [Var t]
				(M.build_if (BinOp (LNth, Var ret, Val (VInt 0)))
					(** The term matches. **)
					(M.fresh "subterms" (fun sub ->
						 M.asn sub (BinOp (LNth, Var ret, Val (VInt 1)))
						 (decouple sub arity (fun l ->
							 M.fresh "return" (fun ret ->
								 let args = List.map (fun x -> Var x) (args @ l) in
								 M.call ret f args
								 (M.return (Var ret)))))))
					(** Otherwise, we continue. **)
					(create_dispatch constr_names hook_names h args t cons)))

	(** Create a constructor function of a constructor. **)
	let create_constructor get_fresh proc_name c csig taken =
		let (_, args) =
			List.fold_left (fun (taken, args) s ->
				let (taken, arg) = get_fresh taken ("x_" ^ Printer.string_of_sort s) in
				(taken, arg :: args)) (taken, []) csig.cs_input_sorts in {
			proc_name = proc_name ;
			proc_arguments = args ;
			proc_body =
				M.extract (M.return (VList (Val (VString c) :: List.map (fun x -> Var x) args))) taken
		}

	(** Create the destructor function of a constructor. **)
	let create_destructor get_fresh proc_name c csig taken =
		let (taken, arg) = get_fresh taken ("x_" ^ sort_name csig.cs_output_sort) in {
			proc_name = proc_name ;
			proc_arguments = [arg] ;
			proc_body =
				M.extract
					(M.build_if (BinOp (Eq, BinOp (LNth, Var arg, Val (VInt 0)), Val (VString c)))
						(decouple arg ~start:1 (1 + List.length csig.cs_input_sorts) (fun subterms ->
								 M.return (VList [ Val (VBool true) ; VList (List.map (fun x -> Var x) subterms)])))
						(M.return (VList [ Val (VBool false) ; VList [] ]))) taken
		}

	(** A note on how filters are supposed to be related to the corresponding action.
	 * The action corresponding to a filter should either return:
	 * - a list of size [1] with only element [false] if the filter doesn’t apply,
	 * - a list of size [2] with first element [true] and second element a list of
	 *   returned values if the filter applies. **)
	let export env filter_env =
		(** The exportation exports each pair of hook and constructor to an individual function.
		 * In addition, a dispatch function is defined for each hook to dispatch a term whose
		 * constructor is not known at compile time to the right function. **)
		(** First step: reserving names for each action and procedure. **)
		let get_fresh taken name =
			let name = fresh (fun name -> not (SSet.mem name taken)) name in
			(SSet.add name taken, name) in
		let taken = SSet.of_list keywords in
		let (taken, non_det) = get_fresh taken "nonDeterministicInteger" in
		let (constr_names, taken) =
			SMap.fold (fun c csig (m, taken) ->
					let (taken, cons) = get_fresh taken (c ^ "_intro") in
					let (taken, des) = get_fresh taken (c ^ "_destruct") in
					let arity = List.length csig.cs_input_sorts in
					(SMap.add c (cons, des, arity) m, taken))
				env.env_constr_signatures (SMap.empty, taken) in
		let (filter_names, taken) =
			SMap.fold (fun f _ (m, taken) ->
				let (taken, name) = get_fresh taken f in
				(SMap.add f name m, taken)) env.env_filter_signatures (SMap.empty, taken) in
		let (hook_names, taken) =
			SMap.fold (fun h hsig (m, taken) ->
					let (taken, name) = get_fresh taken h in
					let m = PSMap.add (h, None) name m in
					SMap.fold (fun c _ (m, taken) ->
						let name = h ^ "_" ^ c in
						let (taken, name) = get_fresh taken name in
						(PSMap.add (h, Some c) name m, taken)) hsig.hs_rules (m, taken))
				env.env_hooks (PSMap.empty, taken) in
		let (proc_names, taken) =
			SMap.fold (fun p psig (m, taken) ->
					let (taken, name) = get_fresh taken p in
					(SMap.add p name m, taken))
				env.env_proc_signatures (SMap.empty, taken) in
		(** Second step: compiling each function. **)
		let exported_functions = [] in
		let exported_functions =
			SMap.fold (fun c csig l ->
				let (intro, destruct) =
					match SMap.find_opt c constr_names with
					| None -> assert false
					| Some (intro, destruct, arity) -> (intro, destruct) in
				let intro = create_constructor get_fresh intro c csig taken in
				let destruct = create_destructor get_fresh destruct c csig taken in
				intro :: destruct :: l) env.env_constr_signatures exported_functions in
		SMap.fold (fun h hsig l ->
			let new_procs =
				let (arg_names, taken) =
					List.fold_left (fun (m, taken) x ->
						let (taken, name) = get_fresh taken x.tv_name in
						(SMap.add x.tv_name name m, taken)) (SMap.empty, taken) hsig.hs_input_vars in
				let (taken, arg_term_name) = get_fresh taken "t" in
				let get_arg x =
					match SMap.find_opt x.tv_name arg_names with
					| None -> assert false
					| Some name -> name in
				let cons = SMap.fold (fun c _ l -> c :: l) hsig.hs_rules [] in
				let args = List.map get_arg hsig.hs_input_vars in
				let dispatch = {
						proc_name =
							(match PSMap.find_opt (h, None) hook_names with
							 | None -> assert false
							 | Some n -> n) ;
						proc_arguments = args @ [arg_term_name] ;
						proc_body =
							let f =
								create_dispatch constr_names hook_names h args arg_term_name cons in
							M.extract f taken
					} in
				let functions =
					SMap.fold (fun c rule l ->
						let (all_arg_subterms, ve, taken) =
							List.fold_left (fun (args, m, taken) x ->
									let (taken, name) = get_fresh taken x.tv_name in
									(name :: args, SMap.add x.tv_name name m, taken))
								([], arg_names, taken) rule.r_constructor_arguments in
						let all_arg_subterms = List.rev all_arg_subterms in
						let env_names = (constr_names, filter_names, hook_names, proc_names) in
						let f = {
								proc_name =
									(match PSMap.find_opt (h, Some c) hook_names with
									 | None -> assert false
									 | Some n -> n) ;
								proc_arguments =
									List.map get_arg hsig.hs_input_vars @ all_arg_subterms ;
								proc_body =
									let body =
										extract_skeleton env env_names filter_env non_det ve rule.r_skeleton (fun l ->
											M.return (VList (List.map (fun x -> Var x) l))) in
									M.extract body taken
							} in
						f :: l) hsig.hs_rules [] in
				dispatch :: functions in
			new_procs @ l) env.env_hooks exported_functions

	end

