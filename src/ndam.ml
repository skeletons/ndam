open List
   
open Util
open Skeleton
open Skeltypes
open Interpretation

(******* utility functions *******)

let fresh_taken taken v = fresh (fun x -> not (mem x taken)) v
   
let name_of_base = function
  | Base (n, _, _) -> n
  | _ -> failwith "name_of_base_sort"

let name_of_pconstr = function 
  | PConstr ((c, _), _, _) -> c
  | _ -> failwith "name_of_pconstr"
       
let name_of_tconstr = function 
  | TConstr ((c, _), _, _) -> c
  | _ -> failwith "name_of_tconstr"

(*makes a pattern out of a constructor and a list of typed_var*)     
let make_pattern cons l =
  let l = (map (fun v -> PVar v) l) in
  let p = (match l with
    | [p'] -> p'
    | _ -> PTuple l) in
  PConstr (cons, [], p)

(*same for a term*)     
let make_term cons l =
  let l = (map (fun v -> TVar (v,[])) l) in
  let t = (match l with
           | [p'] -> p'
           | _ -> TTuple l) in
  TConstr (cons, [], t)

let rec term_of_pattern = function
  | PVar v -> TVar (v, [])
  | PConstr (c, sl, p) -> TConstr (c, sl, term_of_pattern p)
  | PTuple l -> TTuple (map term_of_pattern l)
  | _ -> failwith "term_of_pattern"

let rec pattern_of_term = function
  | TVar (v, _) -> PVar v
  | TConstr (c, l, t) -> PConstr (c, l, pattern_of_term t)
  | TTuple l -> PTuple (map pattern_of_term l)
  | _ -> failwith "pattern_of_term"
       
(* split a pattern into the head constructor and its argument*)   
let split_pattern p =
  let rec aux = function
    | PVar v -> [v]
    | PTuple l -> concat (map aux l)
    | _ -> failwith "split_pattern2"
  in match p with
  | PConstr (c, _, p2) -> (c, aux p2)
  | _ -> failwith "split_pattern"

let rec arrow_type_of_tvlist rtype = function
  | [v] -> Arrow (v.tv_typ, rtype)
  | [] -> Arrow (Product [] , rtype)
  | h::t -> Arrow (h.tv_typ, arrow_type_of_tvlist rtype t)       

let rec arrow_type_of_typ_list rtype = function
  | [v] -> Arrow (v, rtype)
  | [] -> Arrow (Product [] , rtype)
  | h::t -> Arrow (h, arrow_type_of_typ_list rtype t)       

   
  
(******* conventions and notations *******)

(* we expect in the original sk file a term init
 * giving the entry point to the ZS, and a term
 * inctx defining the plugging of a term in a context *)   
let inctx = "inctx"
let init = "init"
   
let annot = "annot"
let avar = "_a"
let annotSet = annot^"Set"
let asvar = "_s"
let s_annot = Base (annot, false, [])
let s_annotSet = Base(annotSet, false, [])
(*annotation corresponding to the mode m*)
let make_annot m = "A"^m
let make_fannot m = (String.lowercase_ascii m)^"_to_annot"
let tv_fannot m args = { tv_typ = arrow_type_of_typ_list s_annot args;
                         tv_name = make_fannot m }
                 
let empty_annotSet = "empty_annotSet"
let t_e_aSet = TVar ({tv_typ = s_annotSet; tv_name = empty_annotSet}, [])
                 
let tv_asvar = {tv_typ = s_annotSet; tv_name = asvar}
let tv_anvar = { tv_name = avar; tv_typ = s_annot }
                 
let niaSet = "notin"^annotSet
let s_niaSet s_term = Arrow (s_annot, Arrow(s_term, Product []))
let tv_niaSet s_term = {tv_typ = s_niaSet s_term; tv_name = niaSet}

let iaSet = "in"^annotSet
let s_iaSet s_term = Arrow (s_annot, Arrow(s_term, Product []))
let tv_iaSet s_term = {tv_typ = s_iaSet s_term; tv_name = iaSet}

             
let erase = "erase"
let s_erase s_term = Arrow (s_term, s_term)
let tv_erase s_term = {tv_typ = s_erase s_term; tv_name = erase}


let inctx = "inctx"
let s_inctx s_ctx s_term = Arrow (s_ctx, Arrow (s_term, s_term))
let tv_inctx s_ctx s_term = {tv_typ = s_inctx s_ctx s_term;
                             tv_name = inctx }
                      
(*stack of rules*)
let rulen = "rule"
let s_rule = Base (rulen, true, [])                      
let stack = "rStack"
let s_stack = Base (stack, true, [])
let estack = "EmpStack"
let consrs = "Cons"
let svar = "_p"
let tv_svar = { tv_name = svar; tv_typ = s_stack }
           
let addas = "add"^annotSet
let s_addas = Arrow (s_annot, Arrow(s_annotSet, s_annotSet))
let tv_addas = {tv_typ = s_addas; tv_name = addas}
                      
let amcfg = "amcfg"
let s_amcfg = Base (amcfg, true, [])
let amvar = "_c"
let make_amcfg m = "M"^m
let make_amcfgB m = (make_amcfg m)^"B"
let mdone = "MDone"
let zsm = "zs"          
let minit =  make_amcfg zsm
         
let sk_amvar = Return (TVar( { tv_typ = s_amcfg; tv_name = amvar }, []))

(* e.g., turns "equal" into "notequal", and "notequal" into "equal" *)             
let make_opposite f = if String.sub f 0 3 = "not"
                      then String.sub f 3 (String.length f - 3)
                      else "not"^f
             
let stepf = "step"


          

(***********************************************************************************)
(***********************************************************************************)
(***********************"constant" terms and sorts**********************************)
(***********************************************************************************)
(***********************************************************************************)

          
(*boilerplate: annot, annotSet and its filters, erase,  
  In general, the parameters s_* are sorts*)
let gen_commonbp s_tcons s_econs =
  let sorts = SMap.add annotSet (s_annotSet, None) SMap.empty in
  let sorts = SMap.add annot (s_annot, None) sorts in
  let terms = SMap.add niaSet ([], s_niaSet s_tcons, None) SMap.empty in
  let terms = SMap.add iaSet ([], s_iaSet s_tcons, None) terms in
  let terms = SMap.add addas ([], s_addas, None) terms in
  let terms = SMap.add empty_annotSet ([], s_annotSet, None) terms in
  let terms = SMap.add erase ([], s_erase s_tcons, None) terms in  
  { ss_types = sorts;
    ss_aliases = SMap.empty;
    ss_terms = terms; }

(* given an input sort is, add the sort ns to it*)
let add_sort is ns = match is with
  | Product l -> Product (l @ [ns])
  | Base _ -> Product (is::ns::[])
  |  _ -> failwith "add_sort"

(*adding annotSet to the type of terms*)                        
let gen_annot env ndam tcons  =
  (*extending terms with annotSet *)
  let (_, tdef) = match SMap.find tcons env.ss_types with
    | (_, None) -> failwith "no terms definition"
    | (s, Some cl) -> (s, cl)
  in
  let newtdef = map (fun cs -> { cs with cs_input_type = add_sort
  cs.cs_input_type s_annotSet}) tdef in
  let sorts = SMap.add tcons (Base(tcons, true, []), Some newtdef) ndam.ss_types in  
  { ndam with ss_types = sorts }

(*copy the remaining potentially useful term/types in env: the types distinct from tcons,
 *mcons, and not starting with opt, and the terms that are not the lts
 *definition *)  
let copy_useful env ndam tcons mcons zs =
  (*types *)
  let env' = SMap.filter (fun k _ ->
                 (not (String.equal k tcons)) && (not (String.equal k mcons))
                 && (not (String.equal (String.sub k 0 3) "opt"))) env.ss_types in
  let sorts = SMap.union (fun _ a _ -> Some a) env' ndam.ss_types in
  (*terms*)
  let env' = SMap.filter (fun k _ -> not (String.equal k zs)
                                     && not (String.equal k init)
                                     && not (String.equal k inctx)) env.ss_terms  in
  let terms = SMap.union (fun _ a _ -> Some a) env' ndam.ss_terms in
  {
    ss_types = sorts;
    ss_aliases = SMap.empty;
    ss_terms = terms; 
  } 
    
(* create if needed the opposite side-conditions*)
(* if eqname : a1 -> a2 -> ... -> an -> unit exists but not its opposite, it
 * creates eqname : a1 -> a2 -> ... -> an -> unit *)
(* if noteqname already with not the expected type well too bad for you*)

let rec is_sc_type = function
  | Arrow (_, t) -> is_sc_type t
  | Product [] -> true
  | _ -> false
  
let gen_opposite_sc env ndam =
  let rec aux res = function
    | [] -> res
    | (fname, (l, typ, _))::t ->
       let res' = aux res t in
       let opp_fname = make_opposite fname in
       match (SMap.find_opt opp_fname env.ss_terms) with
       | None -> SMap.add opp_fname (l, typ, None) res'
       | Some (_, opt_typ, _) -> if (Skeleton.type_compare opt_typ typ) = 0 then res'
                            else failwith (opp_fname^" is expected to be the opposite of "^fname^", but their types are different")
  in 
  let env' = SMap.bindings (SMap.filter (fun k (_, tp, def) -> is_sc_type tp && def = None)
               env.ss_terms)  in
  {ndam with ss_terms = aux (ndam.ss_terms) env'}
  
  
(***********************************************************************************)
(***********************************************************************************)
(*********************generating the AM config sorts********************************)
(***********************************************************************************)
(***********************************************************************************)

let gen_stack ndam lrn =
  let sorts = SMap.add rulen
                (s_rule, Some (map (fun rn -> {
                                        cs_name = rn;
                                        cs_type_args = [];
                                        cs_input_type = Product [];
                                        cs_output_type = s_rule;
                                      }
                                 ) lrn))
                ndam.ss_types in
  let cs_estk =
    {
      cs_name = estack;
      cs_type_args = [];
      cs_input_type = Product [];
      cs_output_type = s_stack;
    } in
  let cs_cons =
    {
      cs_name = consrs;
      cs_type_args = [];
      cs_input_type = Product [s_rule; s_annotSet; s_stack];
      cs_output_type = s_stack;
    } in    
  let sorts = SMap.add stack
                (s_stack, Some ([cs_estk; cs_cons]))
                sorts in
  { ndam with ss_types = sorts }

let (add_empty_annotset_skel, add_empty_annotset_term)  =
  let interp : (term, skeleton) interpretation =
    {
      var_interp = (fun v ta -> TVar (v, ta)) ;
      constr_interp = (fun (c, out) ta t ->
        let t' = (match t with
          | TTuple l -> TTuple (l@[t_e_aSet])
          | _ -> TTuple [t; t_e_aSet])
        in TConstr ((c, out), ta, t')) ;
      tuple_interp = (fun t -> TTuple t) ;
      function_interp = (fun x ty s -> TFunc (x, ty, s)) ;
      letin_interp = (fun a t s1 s2 -> LetIn (a, t, s1, s2));
      apply_interp = (fun t1 ta t2 -> Apply (t1, ta, t2)) ;
      merge_interp = (fun s b loc -> Branching (s, b, loc)) ;
      return_interp = (fun l -> Return l)
    } in (interpret_skeleton interp, interpret_term interp)
           
(*side conditions are of the form let _ = f args in ...*)
(*during saturation, we add a rule let _ = notf args in ...*)
let is_side_condition p sk =
  match p with
  | PVar v -> if (v.tv_name = "_") then 
               (match sk with
                | Apply (f, _, args) -> [(f, args)]
                | _ -> failwith "is_side_condition"
               )
              else []
  | _ -> []
  
(* so far we assume a rule always pattern-match on the term, done by the first
 * LetIn*)
(*we are not checking that skvar is indeed the correct term variable*)
(*also generates a fresh rule name of the form modeop_k*)
(*lsc: list of side conditions*)
(*returns: rule name, root operator, variables of the pattern,
 *the updated pattern (with the annotSet varia ble), list of
 *side conditions, the first letins, the final call*)
let analyse_rule rule mode taken =
  let rec split_rule = function
    | LetIn (b, p, sk1, sk2) ->
       let sk1' = (add_empty_annotset_skel sk1) in
       let sc = is_side_condition p sk1' in
       let (lsc, sk, sk_call) = split_rule sk2 in
       (sc@lsc, (fun x -> LetIn(b, p, sk1', sk x)), sk_call)
    | Apply (f, sl, tl) -> ([], (fun x -> x), (f, sl, tl))
    | _ -> failwith "a rule should be a succession of LetIn ended by an Apply"
  in let (pterm, skvar, (lsc, f_sk, sk_call)) =
       (match rule with
        | LetIn (_, p, sk1, sk2) -> (p, sk1, split_rule sk2)
        | _ -> failwith "a rule should start with a pattern matching on the
                         source term"
       ) in
     let (c_op, lvar) = split_pattern pterm in
     let (op, _) = c_op in
     let rn = fresh_taken taken (mode^op) in
     (rn, c_op, lvar, (make_pattern c_op (lvar@[tv_asvar]), lsc),
      f_sk, sk_call)

let analyse_rules mode l =
  let (lrn, lrules, _) =
    fold_left (fun (lrn, lrules, taken) rule ->
        let res = analyse_rule rule mode taken in
        let (rn, _, _, _, _, _) = res in
        (rn::lrn, res::lrules, rn::taken))
      ([], [], []) l
  in (lrn, lrules)
    
(* no checking, pargs should be either ASome(list of variables)*)  
let analyse_pargs pargs =
  match pargs with
  | PConstr (_, _, p) ->
     begin match p with
     | PTuple [] -> []
     | PTuple l -> map (function | PVar v -> v | _ -> failwith "analyse_pargs3") l
     | PVar v -> [v]
     | _ -> failwith "analyse_pargs2"
     end
  | _ -> failwith "analyse_pargs"


(*extract the info from a mode definition in the original ZS*)        
let analyse_mode = function
  | LetIn (_, pmode, _, sk2) ->
     (match sk2 with
      | LetIn (_, pargs, _, sk4) ->
         let rules = 
           match sk4 with
           | Branching (_, l, _) -> l
           | LetIn _ -> [sk4]
           | _ -> failwith "expecting the LTS rule(s) (branch if several, or just
                            a LetIn)"
         in
         let (lrn, lrules) = analyse_rules (name_of_pconstr pmode) rules in
         (lrn, (pmode, analyse_pargs pargs, lrules))
      | _ -> failwith "expecting pattern matching on the arguments"
     )
  | _ -> failwith "expecting pattern matching on the mode"

(*extract some info from the original ZS def and split its rules per mode*)       
let split_lts env zs =
  match (SMap.find_opt zs env.ss_terms) with
  | None -> failwith ("Expecting a ZS definition in a term "^zs^": term -> mode -> optctx ->
                                                       optarg -> term")
  | Some (_, _, ltsdef) ->
     match ltsdef with
     | None -> failwith "no lts definition"
     | Some sk1 ->
        match sk1 with
        | TFunc (v_term, s_term, sk2) ->
           (match sk2 with
            | Return(TFunc (v_mode, s_mode, sk3)) ->
               (match sk3 with 
                | Return(TFunc (v_optarg, _, sk4)) ->
                   let lsk = 
                     (match sk4 with
                      | Branching (_, l, _) -> l
                      | LetIn _ -> [sk4]
                      | _ -> failwith "expecting branching describing each
                                       mode or a LetIn if there is only one
                                       mode"
                     )
                   in
                   let (lrn, lmodes) = split(map analyse_mode lsk) in
                   (concat lrn, name_of_base s_mode,
                    {tv_name = v_term; tv_typ = s_term}, lmodes)
                | _ -> failwith "expecting (a:optarg) -> ..."
               )
            | _ -> failwith "expecting (m:mode) -> ..."
           )
        | _ -> failwith "expecting (t:term) -> ..."

let gen_amcfg_sort_mode s_tcons (pmode, pargs, rules)  =
  (*pargs is a list of typed variables*)
  let nmode = name_of_pconstr pmode in
  let largs = map (fun x -> x.tv_typ) pargs in
  let fannot = arrow_type_of_tvlist s_annot pargs in
  (*forward mode: term, stack, other arguments*)
  let inputs = s_tcons::s_stack::largs in
  let cs_forward = {
      cs_name = make_amcfg nmode;
      cs_type_args = [];
      cs_input_type = Product inputs;
      cs_output_type = s_amcfg;
    } in
  (*backtracking mode: stack, term, other arguments*)
  let inputs = s_stack::s_tcons::largs
  in let cs_backward = {
         cs_name = make_amcfgB nmode;
         cs_type_args = [];
         cs_input_type = Product inputs;
         cs_output_type = s_amcfg;
       } in  ((nmode, fannot), [cs_forward; cs_backward])

                      
let gen_amcfg_sort ndam splittedlts s_tcons  s_econs =
  let cs_MDone = {
      cs_name = mdone;
      cs_type_args = [];
      cs_input_type = s_tcons;
      cs_output_type = s_amcfg;
    } in
  let cs_MInit = {
      cs_name = minit;
      cs_type_args = [];
      cs_input_type = s_tcons;
      cs_output_type = s_amcfg;
    } in
  let (lfannot, cs_modes) = split (map (gen_amcfg_sort_mode s_tcons)
                              splittedlts) in
  let sfannot = to_seq (map (fun (nmode, t) -> (make_fannot nmode, ([], t, None)))
                          lfannot) in
  let terms = SMap.add_seq sfannot ndam.ss_terms in
  let sorts = SMap.add amcfg (s_amcfg, Some ((concat cs_modes) @
  [ cs_MDone; cs_MInit])) ndam.ss_types in
  {
    ss_types = sorts;
    ss_aliases = SMap.empty;
    ss_terms = terms; 
  } 



(***********************************************************************************)
(***********************************************************************************)
(***********************************************************************************)
(******************generating the AM stepping function******************************)
(***********************************************************************************)
(***********************************************************************************)
(***********************************************************************************)

(*map sorts to a meta_variable. Some are notations, for the others, we take the
 *first letter of the sort; if it is already taken, we repeat it*)
let vars_of_sorts sorts =
  let res = SMap.add amcfg amvar (SMap.empty) in
  let res = SMap.add annot avar res in
  let res = SMap.add annotSet asvar res in
  let l = (map (fun (k, _) -> k) (SMap.bindings sorts)) in
  let rec aux2 s c res =
    if not(SMap.exists (fun _ v -> (v=s)) res)
    then s else aux2 (s^c) c res
  in 
  let rec aux res = function
    | [] -> res
    | h::t -> if (SMap.mem h res) then aux res t else
                let l = String.make 1 (Char.lowercase_ascii h.[0]) in
                aux (SMap.add h (aux2 l l res) res) t
  in (fun x -> SMap.find x (aux res l))

           
  
(***********************************************************************************)
(**************** Generating the steps in forward mode******************************)
(***********************************************************************************)
               
(* not sure if we ever add the annotset somewhere in that case*)
let analyse_targs = function
  | TConstr (_, _, t) ->
     begin match t with
     | TTuple l -> l
     | TVar _ | TConstr _ -> [t]
     | _ -> failwith ("analyse_targs2"^(Printer.string_of_term 0 t))
     end
  | _ -> failwith "analyse_targs"

              
(*generate the pattern matching on term at the beginning of a forward step *)       
let let_pattern_term c_op lvar tvterm =
  let l = lvar @ [tv_asvar] in
  let p = make_pattern c_op l in
  fun sk ->   LetIn (None, p, Return (TVar(tvterm, [])), sk)

(* In case of an inductive rule, the arguments in tl are 
   in order a term, the mode, an optarg *)
let get_args_inductive l = match l with 
  | [ tm ; mode ; optarg ] -> (tm, mode, analyse_targs optarg)
  | _ -> failwith "get_args_inductive"


(*computation of the annotation for the mode "target" *)       
let gen_annotation tanvar largs target = 
  let typargs = map term_type largs in
  (fun x -> LetIn (None, PVar (tanvar), Apply (tv_fannot target typargs,
                                                 [], largs), x))
      
(* generate the (end of the) machine step in case of an inductive rule *)
let gen_inductive s_term stk f_sk tl =
  let (tm, mode, largs) = get_args_inductive tl in
  let smode = name_of_tconstr mode in
  let mcfg = make_amcfg smode in
  let sk_anvar = gen_annotation tv_anvar largs smode
  in 
  let t = TTuple (tm::stk::largs) in
  ((sk_anvar, tm),
   sk_anvar (LetIn (None, PWild (Product []), Apply (tv_niaSet s_term, [],
                                                     [TVar(tv_anvar, []); tm]),
                    f_sk (Return (TConstr((mcfg, s_amcfg), [], t))))
  ))

(* in case of an axiom, tl are the arguments of "inctx", so context then term *)  
let gen_axiom fsorts s_term s_ctx taken lvar f_sk tl =
  let l = lvar @ [tv_asvar] in
  let (ctx, tm) =
    (match tl with
     | [ ctx ; tm ] -> (ctx, add_empty_annotset_term tm)
     | _ -> failwith "gen_axiom"
    ) in
  let taken' = taken@(map (fun tv -> tv.tv_name)
                        (l@ (concat (map free_variables_term tl))))  in
  let tm' = fresh_taken taken' (fsorts (name_of_base s_term)) in
  let tm'' = fresh_taken (tm'::taken') (fsorts (name_of_base s_term)) in
  let tv_tm' = {tv_name = tm'; tv_typ = s_term} in
  let tv_tm'' = {tv_name = tm''; tv_typ = s_term} in
  f_sk (
      LetIn (None, PVar (tv_tm'),
             Apply (tv_inctx s_ctx s_term, [], [ctx; tm]),
             LetIn (None, PVar(tv_tm''),
                   Apply (tv_erase s_term, [], [TVar(tv_tm', [])]),
                   Return (TConstr((minit, s_amcfg), [],
                                   TVar(tv_tm'', [])))
               )
        )
    )

let find_constr sort constr ndam =
  let (_, lc) = SMap.find sort (ndam.ss_types) in
  match lc with
  | None -> failwith "find_constr"
  | Some l -> find (fun c -> c.cs_name = constr) l

let rec pattern_of_sort sort taken fsorts =
  match sort with
  | Base (s, _, _) -> let v = fresh_taken taken (fsorts s)
                      in (v::taken, PVar {tv_typ = sort; tv_name = v})
  | Product l -> let (taken', l') = pattern_of_sortlist l taken fsorts in
                 (taken', PTuple l') 
  | _ -> failwith "pattern_of_sort"
and pattern_of_sortlist l taken fsorts =
  match l with
  | [] -> (taken, [])
  | h::t -> let (taken', p) = pattern_of_sort h taken fsorts in
            let (taken'', l') = pattern_of_sortlist t taken' fsorts in
            (taken'', p::l')

let pattern_of_constr cs taken fsorts =
  let (taken', p) = pattern_of_sort cs.cs_input_type taken fsorts in
  (taken', PConstr ((cs.cs_name, cs.cs_output_type), [], p))


(* replaces the annotset metavariable of t with asvar, return the updated term
 * and the former annotset variable*)       
let update_annotset t (asvar:Skeltypes.term) =
  let rec aux = function
    | [v] -> (v, [asvar])
    | h::t -> let (v, t') = aux t in (v, h::t')
    | _ -> failwith "update_annotset3"
  in 
  match t with
  | TConstr (c, sl, t') -> let (v, t'') = 
     (match t' with
     | TTuple l -> let (v, l') = aux l in (v, TTuple l')
     | TVar _ -> (t', asvar)
     | _ -> failwith "update_annotset2")
                           in (v, TConstr (c, sl, t''))
  | _ -> failwith "update_annotset"

(*generate the sequence of opposites of side-conditions *)
let gen_opposite_skel l =
  fold_left (fun f (sc, args) ->
      (fun x -> LetIn (None, PWild (Product []),
                       Apply ({tv_name = make_opposite sc.tv_name;
                               tv_typ = sc.tv_typ}, [], args), f x)))
    (fun y -> y) l

(*generate the sequence of inAnnotset for the "otherwise" branch *)
let gen_ias s_term lannottm =
  (* the "fold_left" builds the succession of 
     (definition of annotset, "let _ = inannotSet ...") with some
   * renaming *)
  let (lsk, ias, _) = 
    (fold_left (fun (f, sk, taken) (fsk, tm) ->
         let avar' = fresh_taken taken ("_"^avar) in
         let f_rename = 
           (skel_rename (fun x -> if x = avar then avar'
                                  else x)) in
         let tannot' =  TVar({tv_typ = s_annot; 
                              tv_name = avar'}, [])
         in
         ((fun y -> f (f_rename (fsk y))), 
          (fun x -> sk (LetIn (None, PWild (Product []),
                                    Apply (tv_iaSet s_term, [],
                                           [tannot'; tm]),
                                    x))),
          avar'::taken)
       )
       ((fun x -> x), (fun x -> x), []) lannottm)
  in fun x -> lsk (ias x)

      
(*3 kinds: not parsed, parsed with sc, parsed withtout sc*)
(*not parsed: new pattern, for the other, we reuse pterm*)
(*f in the added skeleton for the with and without sc cases*)
let gen_saturate ndam pmode largs tvterm fsorts taken (constr, pterm, f) =
  let smode = (name_of_pconstr pmode) in
  let mcfg = make_amcfgB smode in
  let cs_constr = find_constr (name_of_base tvterm.tv_typ) constr ndam in
  let (taken', p) = pattern_of_constr cs_constr taken fsorts in
  let p = (if pterm = PWild (Product []) then p else pterm) in
  let t = term_of_pattern p in
  let asvar' = fresh_taken taken' (fsorts annotSet) in
  let tv_asvar' = {tv_name = asvar'; tv_typ = s_annotSet} in
  let (tv_as, t') = update_annotset t (TVar (tv_asvar', [])) in
  let targs = map (fun tv -> TVar (tv, [])) largs in
  let l = TVar(tv_svar, [])::t':: targs in
  let tannot' = fresh_taken [avar] avar in
  let tv_anvar' = {tv_name = tannot'; tv_typ = s_annot} in
  let f_g_annot = gen_annotation tv_anvar' targs smode in
  LetIn (None, p, Return (TVar(tvterm, [])),
         f (f_g_annot (
                       LetIn (None, PVar (tv_asvar'),
                              Apply (tv_addas, [], [TVar (tv_anvar', []); tv_as]), 
                              Return (TConstr ((mcfg, s_amcfg), [],
                                               TTuple l)))
              )
           )
    )
  
(*for each constructor, keep the necessary infos to generate the "otherwise"
 *branch*)
(*problem if pterm is not the same between rules for the
 *same constructor*)
let rec partition_c = function
  | [] -> []
  | Some ((c, _), pterm, _, inf) :: t ->
     let res = partition_c t in
     (match (assoc_opt c res) with
     | None -> (c, (pterm, [inf]))::res
     | Some (p, l) -> if p<>pterm then failwith "the variables must be the same
                                                 when different rules
                                                 pattern-match the same
                                                 operator"
                      else (c, (p, inf::l))::(remove_assoc c res)
     )
  | _::t -> partition_c t

                     
let gen_forward ndam tvterm s_ctx fsorts zs (pmode, largs, an_rules) =
  let s_term = tvterm.tv_typ in 
  let smode = (name_of_pconstr pmode) in
  let mcfg = make_amcfg smode in
  let amargs = tv_svar::largs in 
  let ampattern = make_pattern (mcfg, s_amcfg)  (tvterm::amargs) in
  let taken = map (fun tv -> tv.tv_name) (tvterm::amargs) in
  (*forward rules*)
  let (linf, l) = split (
    map (fun (rn, c_op, lvar, (pterm, lsc), f_sk, (f, _, tl)) ->
        let (inf, fin_sk) = (
            if f.tv_name = zs then (*not an axiom*)
              (*the extended stack*)
              let stk = TConstr ((consrs, s_stack), [],
                                 TTuple (TConstr ((rn, s_rule), [], TTuple [])::
                                           map (fun v -> TVar (v, [])) [tv_asvar; tv_svar]))
              in
              let (inf, sk) = gen_inductive s_term stk f_sk tl
              in (Some (c_op, pterm, lsc, inf), sk)
            else (*axiom*) 
              (None, gen_axiom fsorts s_term s_ctx taken lvar f_sk tl)
          )
        in (inf, (let_pattern_term c_op lvar tvterm) fin_sk))
      an_rules) in
  (*saturation of forward rules*)
  let withoutsc = partition_c linf in
  let withoutsc = map (fun (c, (pterm, linf)) -> (c, pterm, gen_ias s_term linf))
                    withoutsc in 
  let parsed = map (fun (_, (c, _), _, (pterm, lsc),  _, _) -> (c, (pterm, lsc))) an_rules in
  let withsc = filter_map (fun (c, (pterm,  lsc)) -> if (lsc<>[]) then
                 Some (c, pterm, gen_opposite_skel lsc) else None) parsed in
  let all_constr = (let (_, lc) = SMap.find (name_of_base s_term) (ndam.ss_types) in
                    match lc with
                    | None -> failwith "find_all_constr (gen_forward)"
                    | Some l -> map (fun c -> (c.cs_name, PWild (Product []),
                                                           (fun x -> x))) l) in
  let not_parsed = filter (fun (x, _, _) ->
                        not (mem x (map (fun (c, _) -> c) parsed))) all_constr in
  let saturated = map
                    (gen_saturate ndam pmode largs tvterm fsorts taken) 
                    (not_parsed@withsc@withoutsc) in                           
  LetIn (None, ampattern, sk_amvar, Branching (s_amcfg, l @ saturated, None))


let gen_init (tvterm, largs) =
  let p = PConstr ((minit, s_amcfg), [], PVar(tvterm)) in
  let (tm, mode, args) = get_args_inductive largs in
  let smode = name_of_tconstr mode in
  (*init step*)
  let stk = TConstr((estack, s_stack), [], TTuple []) in
  let mcfg = make_amcfg smode in
  let t = TTuple (tm::stk::args) in
  let sk_init = LetIn (None, p, sk_amvar, Return (TConstr((mcfg, s_amcfg), [], t))) in
  (*backtracking step*)
  let mcfgB = make_amcfgB smode in
  let l = TConstr((estack, s_stack), [], TTuple [])::tm::args in
  let p = PConstr ((mcfgB, s_amcfg), [], pattern_of_term (TTuple l)) in
  let sk_back = LetIn (None, p, sk_amvar,
                       Return(TConstr((mdone, s_amcfg), [], TVar(tvterm, []))))
  in 
  [ sk_init; sk_back]
  

  
(***********************************************************************************)
(**************** Generating the steps in backward mode ****************************)
(***********************************************************************************)

  
let gen_backtrack_rule cmode argsconc fsorts (rn, c_op, lvar, _, f_sk, (f, _, tl)) =
  (* if an axiom, no corresponding backtracking step*)
  if (f.tv_name = inctx) then None  
  else
    let (tm, mode, argsprem) = get_args_inductive tl in
    let smode = (name_of_tconstr mode) in
(*    let (sk_anvar, tanvar) = chg_mode smode cmode (map (fun v -> TVar(v, []))
                                                     argsconc) in*)
    let svar' = fresh_taken [svar] svar in
    let tv_svar' = {tv_name = svar'; tv_typ = s_stack } in
    let stk = TConstr ((consrs, s_stack), [],
                       TTuple (TConstr ((rn, s_rule), [], TTuple [])::
                                 map (fun v -> TVar (v, [])) [tv_asvar; tv_svar'])) in 
    let pstack = pattern_of_term stk in
    let taken = map (fun x -> x.tv_name)
                  (lvar@argsconc@concat (map free_variables_term
                                                    argsprem)) in
    let (lsk, largs, _) = fold_right
                         (fun t (lsk, largs, taken) ->
                           match t with
                           | TVar (v, [])  -> ((fun x -> x)::lsk, v::largs,
                                               taken)
                           | TConstr ((_, ttyp), _, _) ->
                              let v = fresh_taken taken
                                        (fsorts (name_of_base ttyp)) in
                              let tv = { tv_name = v; tv_typ = ttyp
                                       } in
                              ((fun x -> LetIn (None, pattern_of_term t,
                                               Return (TVar(tv, [])),
                                               x))::lsk,
                                        tv::largs, v::taken)
                           | _ -> failwith "gen_backward"
                         )
                         (tm::argsprem) ([], [], taken) in
    let sk_args = fold_left (fun acc f -> (fun x -> f (acc x)))
                    (fun x -> x) lsk in
    let pam = make_pattern (make_amcfgB smode, s_amcfg) (tv_svar::largs) in
    let t = make_term c_op (lvar @ [tv_asvar]) in
    let l = t::TVar (tv_svar', []):: (map (fun tv -> TVar (tv, []))
                                        argsconc) in
    let end_sk = Return (TConstr ((make_amcfg cmode, s_amcfg), [], TTuple l)) in
    Some(LetIn (None, pam, sk_amvar, 
                LetIn (None, pstack, Return (TVar(tv_svar, [])),
                       sk_args((*sk_anvar*)(end_sk))
                  )
           )
      )

let gen_backtrack fsorts (pmode, argsconc, an_rules) =
  let smode = (name_of_pconstr pmode) in
  filter_map (gen_backtrack_rule smode argsconc fsorts)
    an_rules

  
(***********************************************************************************)
(**************** Generating the updated inctx    **********************************)
(***********************************************************************************)

(* to add the empty annotset to the arguments of a TConstr*)             
let add_empty_aset t = match t with
  | TConstr (c, sl, tl) ->
     (match tl with
      | TTuple l -> TConstr (c, sl, TTuple (l@[t_e_aSet]))
      | _ -> TConstr(c, sl, TTuple ([tl; t_e_aSet]))
     )
  | _ -> t

(*generates the new def for inctx,
 *with the (empty) annotations set added to terms*)
let analyse_inctx_rule = function
  | LetIn(b, p, sk1, sk2) ->
     (match p with
     (* the hole case calls the init conf*)
      | PConstr (_, _, PTuple []) ->
          LetIn(b, p, sk1, sk2)
      | PConstr (p', l1, PTuple l) ->
         (match sk2 with
          | Apply (i, l2, [e'; t']) ->
             let t'' = add_empty_aset t' in
             LetIn(b, p, sk1, Apply(i, l2, [e'; t'']))
          | _ -> failwith "analyse_inctx_rule: expecting apply inctx E' t', which
                           is maybe too restrictive" 
         )
      | _ -> failwith "analyse_inctx_rule: expecting a context constructor in the
                       pattern matching"
     )
  | _ -> failwith "analyse_inctx_rule: expecting a pattern matching on the
                   context"
      
let gen_inctx ndam s_ctx def_inctx =
  let (tvctx, tvterm, ldef) = def_inctx in 
  let ldef = map analyse_inctx_rule ldef in
  let newdef = 
    TFunc (tvctx.tv_name, tvctx.tv_typ,
           Return (TFunc (tvterm.tv_name, tvterm.tv_typ,
                          Branching (tvterm.tv_typ, ldef, None))))
  in 
  let terms = SMap.add inctx ([], s_inctx s_ctx tvterm.tv_typ,
                              Some newdef) ndam.ss_terms in
  { ndam with ss_terms = terms }


(***********************************************************************************)
(****************** The generating function itself *********************************)
(***********************************************************************************)

  
let gen_stepf ndam zs tvterm s_ctx splitted init_args =
  let fsorts = vars_of_sorts ndam.ss_types in
  let lforward = (map (gen_forward ndam tvterm s_ctx fsorts zs) splitted) in
  let lbacktrack = concat (map (gen_backtrack fsorts) splitted) in
  let skinit = gen_init init_args in
  let l = lforward @ lbacktrack @ skinit in 
  let term = TFunc (amvar, s_amcfg, Branching (s_amcfg, l, None)) in 
  let terms = SMap.add stepf ([], Arrow(s_amcfg, s_amcfg), Some term)
                ndam.ss_terms in
  { ndam with ss_terms = terms }


  
(***********************************************************************************)
(***********************************************************************************)
(***********************************************************************************)
(***************************** the alltogether *************************************)
(***********************************************************************************)
(***********************************************************************************)
(***********************************************************************************)

let analyse_inctx env =
  match (SMap.find_opt inctx env.ss_terms) with
  | None -> failwith "The ZS must contain a definition of inctx: ctx -> term ->
                      term, defining the pluging of a term in a context"
  | Some (_, _, def) -> 
     match def with
     | None -> failwith "no definition for inctx"
     | Some sk ->
        match sk with
         | TFunc (v_ctx, s_ctx, sk2) ->
            (match sk2 with
             | Return(TFunc (v_term, s_term, sk3)) ->
                (match sk3 with
                 | Branching (_, l, _) -> ( { tv_typ = s_ctx; tv_name = v_ctx },
                                           { tv_typ = s_term; tv_name = v_term
                                           }, l)
                 | _ -> failwith "inctx: expecting a branching"
                )
             | _ -> failwith "inctx: expecting fun (t:term) -> ..."
            )
         | _ -> failwith "inctx: expecting fun (e:ectx) -> ..."

(*getting the name of the ZS and the arguments of the initializing call*)              
let analyse_init env s_term =
  match (SMap.find_opt init env.ss_terms) with
  | None -> failwith "The ZS must contain a definition of init: term ->
                      term, doing the first call to the ZS"
  | Some (_, _, def) -> 
     match def with
     | None -> failwith "no definition for init"
     | Some sk ->
        match sk with
        | TFunc (v_term, s_term', sk2) ->
           if s_term' <> s_term then failwith ("type mismatch: terms between "^init^
                                       " and "^inctx)
           else
             (match sk2 with
              | Apply (tzs, _, arg) -> (tzs.tv_name, ({tv_name = v_term; tv_typ =
                                                                   s_term}, arg))
              | _ -> failwith "init: expecting apply zs ..."
             )
        | _ -> failwith "init: expecting fun (t:term) -> ..."
             
  
let gen_ndam env options =
  (*from the definition of inctx, we get the types of terms and contexts*)
  let (ve, vt, ldef) = analyse_inctx env in
  let s_tcons = vt.tv_typ in
  let s_econs = ve.tv_typ in
  let tcons = name_of_base s_tcons in
  let (zs, init_args) = analyse_init env s_tcons in
  (*the mode type can be found in the typing of the ZS*)
  let (lrn, mcons, tvterm, splittedlts) = split_lts env zs in
  (*boilerplate*)
  let ndam = gen_commonbp s_tcons s_econs in
  let ndam = gen_annot env ndam tcons in
  let ndam = copy_useful env ndam tcons mcons zs in
  let ndam = gen_opposite_sc env ndam in
  let ndam = gen_inctx ndam s_econs (ve, vt, ldef) in
  (*AM configurations*)
  let ndam = gen_stack ndam lrn in
  let ndam = gen_amcfg_sort ndam splittedlts s_tcons s_econs in
  gen_stepf ndam zs tvterm s_econs splittedlts init_args 
