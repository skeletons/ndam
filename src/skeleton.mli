(** Utility functions on skeletal semantics. *)

open Skeltypes


(** {1 Skeletal Semantics} *)

val empty_ss : skeletal_semantics
(** The empty skeletal semantics. *)

(** Compare two skeletal semantics *)
val same_ss: skeletal_semantics -> skeletal_semantics -> bool

(** Get all the constructors, separated by the type of the term they produce *)
val get_constructors_by_type : skeletal_semantics -> (necro_type * constructor_signature list) list

(** Get all the constructors *)
val get_all_constructors : skeletal_semantics -> constructor_signature list

(** Map the variables of a skeletal semantics to new terms. *)
val ss_map : (typed_var -> term) -> skeletal_semantics -> skeletal_semantics

(** Rename the variables of a skeletal semantics. *)
val ss_rename : (var -> var) -> (skeletal_semantics -> skeletal_semantics)

(** Return all the names of declared types, constructors, or terms. *)
val ss_reserved_names : skeletal_semantics -> Util.SSet.t

(** As for [ss_reserved_names], but also return the identifier used in skeletons names. *)
val ss_all_names : skeletal_semantics -> Util.SSet.t

(** [protect_ss against s] returns a semantically equivalent copy of the
 * skeleton [s] where every variable has been renamed to avoid the list of
 * identifiers [against] *)
val protect_ss: string list -> skeletal_semantics -> skeletal_semantics

(** Return all the variables mentionned in a skeletal semantics *)
val all_variables_ss : skeletal_semantics -> var -> bool
val all_variables_list_ss : skeletal_semantics -> var list
val all_variables_set_ss : skeletal_semantics -> Util.SSet.t

(** {1 Skeletons} *)

(** Return the variables declared in a skeleton (including in inner skeletons). *)
val declared_variables_skel : skeleton -> typed_var list

(** Same as {!Skeleton.declared_variables_skel} but without including inner skeletons. *)
val declared_outer_variables_skel : skeleton -> typed_var list

(** Return the variables alive in a skeleton. *)
val alive_variables_skel : skeleton -> var -> bool

(** Return all the variables mentionned in a skeleton. *)
val all_variables_skel : skeleton -> var -> bool
val all_variables_list_skel : skeleton -> var list
val all_variables_set_skel : skeleton -> Util.SSet.t

(** Return the free variables of a skeleton. *)
val free_variables_skel : skeleton -> typed_var list

(** Get the type of a skeleton *)
val skeleton_type : skeleton -> necro_type

(** Map the variables of a skeleton to new terms. *)
val skel_map : (typed_var -> term) -> skeleton -> skeleton

(** Rename the variables of a skeleton. *)
val skel_rename : (var -> var) -> (skeleton -> skeleton)

(** Given a function comparing variables, compare two terms. *)
val skeleton_equals : (typed_var -> typed_var -> bool) -> skeleton -> skeleton -> bool

(** Similar to [term_equals], but following the [compare] conventions. *)
val skeleton_compare : (typed_var -> typed_var -> int) -> skeleton -> skeleton -> int

(** [protect_skel against s] returns a semantically equivalent copy of the
 * skeleton [s] where every variable has been renamed to avoid the list of
 * identifiers [against] *)
val protect_skel: string list -> skeleton -> skeleton


(** {1 Terms} *)

(** Return the variables declared in a term (including in inner skeletons). *)
val declared_variables_term : term -> typed_var list

(** Same as {!Skeleton.declared_variables_term} but without including inner skeletons. *)
val declared_outer_variables_term : term -> typed_var list

(** Return the variables alive in a term. *)
val alive_variables_term : term -> var -> bool

(** Return all the variables mentionned in a skeleton. *)
val all_variables_term : term -> var -> bool
val all_variables_list_term : term -> var list
val all_variables_set_term : term -> Util.SSet.t

(** Return the free variables of a term. *)
val free_variables_term : term -> typed_var list

(** Get the type of a term *)
val term_type : term -> necro_type

(** Map the variables of a term to new terms. *)
val term_map : (typed_var -> term) -> term -> term

(** Rename the variables of a term. *)
val term_rename : (var -> var) -> (term -> term)

(** Given a function comparing variables, compare two terms. *)
val term_equals : (typed_var -> typed_var -> bool) -> term -> term -> bool

(** Similar to [term_equals], but following the [compare] conventions. *)
val term_compare : (typed_var -> typed_var -> int) -> term -> term -> int

(** Rename the variables of a term such that:
 * - no open variable appears twice in the term,
 * - each variables in the term are fresh within the given list of name. *)
val term_fresh : var list -> term -> term

(** Rename the variables of a term such that:
 * - variables appearing more than once in the term will be associated the same name
 *   iff they have the same type.
 * - each variables in the term are fresh within the given list of name. *)
val term_fresh_separate : var list -> term -> term

(** [protect_skel against s] returns a semantically equivalent copy of the
 * skeleton [s] where every variable has been renamed to avoid the list of
 * identifiers [against] *)
val protect_term: string list -> term -> term


(** {1 Sorts} *)

(** Get the name of a type *)
val type_name : necro_type -> type_name

(* For [s] and [s'] two types, and [x] a variable,
 * [subst_type x s s'] = s'{x<-s} *)
val subst_type : var -> necro_type -> necro_type -> necro_type

(* same as subst_type, but perform several substitutions *)
val subst_mult : var list -> necro_type list -> necro_type -> necro_type

(* Compare two types *)
val type_compare: necro_type -> necro_type -> int

(** {1 Miscellaneous} *)

(* give the type against which a pattern matches *)
val pattern_typ: pattern -> necro_type

(** Get a list of pattern that refines the two patterns given in argument. The
 * set of matched terms is the whole type. *)
val common_refinement : skeletal_semantics -> necro_type -> pattern -> pattern -> pattern list
