(** * Auxiliary functions **)

let report_error filename start_pos end_pos =
	let open Lexing in
	let start_col = start_pos.pos_cnum - start_pos.pos_bol + 1 in
	let end_col = end_pos.pos_cnum - start_pos.pos_bol + 1 in
	Format.eprintf "File %S, line %d, characters %d-%d:@." filename start_pos.pos_lnum start_col end_col

(** Wrap a function call to make the whole program fail with the right error code if an exception happens. **)
let wrap f x =
	try f x
	with e ->
		let stack = Printexc.get_backtrace () in
		let msg = Printexc.to_string e in
		Printf.eprintf "Fatal error: %s%s\n" msg stack ;
		exit 1
let wrap2 f x y = wrap (wrap f x) y
let wrap3 f x y z = wrap2 (wrap f x) y z

let parse_input invalid_input l =
	let options =
		let ret x = function l -> (x, l) in
		let one_arg f = function
			| [] -> invalid_input ()
			| x :: q -> f x, q
		in
		(* (options, args, necro_option) *)
		("--safe", 0, ret Skeltypes.SafeMode) ::
		("--no-shadow", 0, ret Skeltypes.SafeMode) ::
		("--ssa", 0, ret Skeltypes.NoShadow) ::
		("--output", 1, one_arg (function o -> Skeltypes.OutputFile o)) ::
		("-o", 1, one_arg (function o -> Skeltypes.OutputFile o)) ::
		("-t", 1, one_arg (function t -> Skeltypes.AbstractType t)) ::
		("-h", 1, one_arg (function h -> Skeltypes.AbstractHook h)) :: []
	in
	let rec aux l accu_opt accu_arg =
		match l with
		| [] ->
				accu_opt, accu_arg
		| x :: q ->
				match List.find_opt (fun (s, _, _) -> s = x) options with
				| None -> aux q accu_opt (x :: accu_arg)
				| Some (s, n, o) ->
						let x, l = o q in
						aux l (x :: accu_opt) accu_arg
	in
	let l1, l2 = aux l [] [] in
	List.rev l1, List.rev l2
    
let print_in_file options txt =
	let output_file =
		List.filter_map (function | Skeltypes.OutputFile s -> Some s | _ -> None) options in
	match output_file with
	| [] -> print_endline txt
	| f :: _ ->
			let oc = open_out f in
			let () = output_string oc txt in
			close_out oc

(** * Main functions for necro **)

(** Just parsing and typing **)
let just_parse invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	ignore (wrap2 Typer.type_program infile parsed)

(** Printing back the parsed skeletal semantics **)
let cat invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let env = wrap2 Typer.type_program infile parsed in
	print_in_file options (wrap Printer.string_of_ss env)


(** Generating a skeletal NDAM from a skeletal ZS **)
let ndam invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let env = wrap2 Typer.type_program infile parsed in
        let bla = Ndam.gen_ndam env options in
	print_in_file options (wrap Printer.string_of_ss bla)

        
(** Generating ML files **)
let mllist invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let sem = wrap2 Typer.type_program infile parsed in
		print_in_file options (wrap2 Generator.generate_interpreter_list sem options)

(** Generating ML files **)
let rand invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let sem = wrap2 Typer.type_program infile parsed in
		print_in_file options (wrap2 Generator.generate_interpreter_rand sem options)

(** Generating ML files **)
let genml invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let env = wrap2 Typer.type_program infile parsed in
	print_in_file options (wrap2 Generator.generate_interpreter env options)


(** Deleting let matching on constructors **)
let simplelet invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let env = wrap2 Typer.type_program infile parsed in
	let env = wrap Compilation.simplelet env in
	print_in_file options (wrap Printer.string_of_ss env)

(** Deleting let matching on constructors **)
let noredef invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let env = wrap2 Typer.type_program infile parsed in
	let env = wrap Compilation.remove_redef env in
	print_in_file options (wrap Printer.string_of_ss env)

(** Deleting let matching on constructors **)
let noinnerletin invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let env = wrap2 Typer.type_program infile parsed in
	let env = wrap Compilation.extract_letin env in
	print_in_file options (wrap Printer.string_of_ss env)

(** Deleting monadic binds **)
let nomonad invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let env = wrap2 Typer.type_program infile parsed in
	let env = wrap Compilation.delete_monads env in
	print_in_file options (wrap Printer.string_of_ss env)

(** Exploding skeletons **)
let explode invalid_input args options =
	let infile =
		match args with
		| x :: [] -> x
		| _ -> invalid_input ()
	in
	let parsed = wrap2 Parserutil.parse_from_file Parser.main infile in
	let env = wrap2 Typer.type_program infile parsed in
	let env = wrap Compilation.explode env in
	print_in_file options (wrap Printer.string_of_ss env)

let help actions _ =
	let describe cmd inputs desc =
		print_endline ("  " ^ Sys.argv.(0) ^ " " ^ cmd ^ " " ^ inputs) ;
		print_endline ("    " ^ desc) in
	let describe_help () =
		let commands = List.map (fun (cmd, _, _, _) -> cmd) actions in
		Printf.eprintf "Usage:\n" ;
		Printf.eprintf "  %s help command\n" Sys.argv.(0) ;
		Printf.eprintf "    where `command' is one of %s.\n" (String.concat ", " commands) in
	match Array.length Sys.argv with
	| 0 | 1 -> assert false
	| 2 ->
		print_endline "Usages:" ;
		List.iter (fun (cmd, inputs, desc, _) -> describe cmd inputs desc) actions
	| 3 ->
		let command = Sys.argv.(2) in
		(match List.find_opt (fun (cmd, _, _, _) -> cmd = command) actions with
		 | None -> describe_help ()
		 | Some (_, inputs, desc, _) ->
			 print_endline "Usage:" ;
			 describe command inputs desc)
	| _ -> describe_help ()

(** MAIN FUNCTION **)
let () =
	let rec actions =
		let help _ _ _ = help actions () in [
			("help", "command",
			 "Provides some basic help for each command.", help) ;
			("parse", "rules.sk",
			 "Parse and typecheck a skeletal file, with no output in case of success.", just_parse) ;
			("nomonad", "rules.sk",
			 "Remove monadic binding and replace them by explicit bindings", nomonad) ;
			("cat", "rules.sk",
			 "Parse, typecheck, and pretty-print a skeletal file.", cat) ;
			("ndam", "rules.sk",
			 "Generate a NDAM from a ZS", ndam) ;
			("mllist", "rules.sk",
			 "Generate an OCaml module associated to the skeletal file with collecting semantics.", mllist) ;
			("rand", "rules.sk",
			 "Generate an OCaml module associated to the skeletal file with random branch choosing.", rand) ;
			("gen", "rules.sk",
			 "Generate an OCaml module associated to the skeletal file.", genml) ;
			("explode", "rules.sk",
			 "Produce a skeletal semantics where all branches are at top level.", explode) ;
			("simplelet", "rules.sk",
			 "Delete all let in constructs matching on a constructor", simplelet) ;
			("noredef", "rules.sk",
			 "Rename variables so that no \"let in\" shadows a previously \
				defined variable", noredef) ;
			("extractletin", "rules.sk",
			 "Extract all inner \"let in\" from all skeletons", noinnerletin) ;
		] in
	let available_options =
		(["--safe"], "adds a \"C_\", \"f_\", \"h_\" and \"s_\" at the beginning \
			of each constructor, filter, hook, and type, respectively") ::
		 (["--no-shadowing"; "--ssa"],
				"Forbids any redeclaration of variable on any line of the flow graph") ::
		 (["--output FILE"; "-o FILE"],
				"Print result in FILE instead of stdout "
		 ) :: []
	in
	let invalid_input action =
		match List.find_opt (fun (cmd, _, _, _) -> cmd = action) actions with
		| None ->
			Printf.eprintf "Usages:\n" ;
			(List.iter (fun (cmd, inputs, _, _) ->
				Printf.eprintf "  %s %s [options] %s\n" Sys.argv.(0) cmd inputs) actions) ;
			exit 1
		| Some (cmd, inputs, _, _) ->
			Printf.eprintf "Usage:\n" ;
			Printf.eprintf "  %s %s [options] %s\n" Sys.argv.(0) cmd inputs;
			Printf.eprintf "  Available options:\n";
			let rec print_list ?(sep="\n  ") print = function
				| [] -> ""
				| a :: [] -> print a
				| a :: q -> print a ^ sep ^ (print_list ~sep print q)
			in
			List.iter (fun (l,s) ->
				Printf.eprintf "  %s\n    %s\n" (print_list (fun s -> s) l) s) available_options;
			exit 1 in
	if Array.length Sys.argv < 2 then invalid_input "" ;
	let action = Sys.argv.(1) in
	let f =
		match List.find_opt (fun (cmd, _, _, _) -> cmd = action) actions with
		| None -> invalid_input ""
		| Some (action, _, _, f) -> f in
	(* given the number of arguments that are not options, returns the list of selected options *)
	let options, args = parse_input (fun _ -> invalid_input "") (List.tl (List.tl (Array.to_list Sys.argv))) in
	wrap3 f (fun _ -> invalid_input action) args options

