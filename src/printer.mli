(** This module provides functions to print the types defined in the Skeltypes module. **)

(** * Main functions **)

(** Converting a type to a string. **)
val string_of_type : Skeltypes.necro_type -> string

(** Converting a term to a string.
 * The output contains line breaks.
 * The argument [indent] indicates the current indentation level.
 * The output string tries to minimise the amount of parentheses. **)
val string_of_term : indent:int -> Skeltypes.term -> string

(** Converting a pattern to a string.
 * The output string has no line breaks and tries to minimise the amount of parentheses. **)
val string_of_pattern : Skeltypes.pattern -> string

(** Converts a skeleton to a string.
 * The output contains line breaks.
 * The argument [indent] indicates the current indentation level. **)
val string_of_skeleton : indent:int -> Skeltypes.skeleton -> string

(** Prints a whole skeletal semantics.
 * The output contains line breaks. *)
val string_of_ss : Skeltypes.skeletal_semantics -> string

(** * Miscellaneous **)

(** Add parentheses around the string if needed. **)
val add_parentheses : string -> string

 
                                 
