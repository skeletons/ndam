(** [getlines ic] takes all the lines available in the input channel and puts
	 them in a string. **)
let getlines ic =
	let rec f acc =
		try f ((input_line ic) :: acc) with End_of_file -> String.concat "\n" (List.rev acc)
	in f []

let report_error filename start_pos end_pos =
	let open Lexing in
	let start_col = start_pos.pos_cnum - start_pos.pos_bol + 1 in
	let end_col = end_pos.pos_cnum - start_pos.pos_bol + 1 in
	Format.eprintf "File %S, line %d, characters %d-%d:@." filename start_pos.pos_lnum start_col end_col

let parse_from_file parsefunc filename =
	let oc = if filename = "-" then stdin else open_in filename in
	let lexbuf = Lexing.from_channel oc in
	let r = try
			parsefunc Lexer.token lexbuf
		with
			(Parser.Error | Parsing.Parse_error | Lexer.Lexing_error _) as e ->
			let errs =
				match e with
				| Lexer.Lexing_error s -> s
				| Parser.Error | Parsing.Parse_error -> "Syntax error."
				| _ -> assert false in
			report_error filename (Lexing.lexeme_start_p lexbuf) (Lexing.lexeme_end_p lexbuf);
			Format.eprintf "%s@." errs; exit 1
	in
	close_in oc;
	r

