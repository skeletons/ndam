open Skeltypes

(** Delete let matching on constructor and replace them by calls to a
 * non-specified term acting as destructor *)
val simplelet: skeletal_semantics -> skeletal_semantics

(** Delete all monadic [let%bind] construct, and replace them by explicit
 * application of [bind] *)
val delete_monads: skeletal_semantics -> skeletal_semantics

(** Explode all branchings, so that each specified term contains at most one
 * branching, at the head *)
val explode: skeletal_semantics -> skeletal_semantics

(** Rename inner variables of terms so that there is no shadowing **)
val remove_redef: skeletal_semantics -> skeletal_semantics

(** Extract inner letin, giving fresh name if need be **)
val extract_letin: skeletal_semantics -> skeletal_semantics
