(* HOpi Abstract Machine *)

open HopiAM

module Flow = struct
  type ident = string
  type name = string

  type side =
  | Right
  | Left
  and rule =
  | PrlmPrl_2
  | PrlmPrl_1
  | PrlmPrl_0
  | PrlmPrl
  | PrlmNew
  | OutPrl_0
  | OutPrl
  | OutNew
  | OutEmit
  | InpRcv_0
  | InpRcv
  | InpPrl_0
  | InpPrl
  | InpNew
  and rStack =
  | EmpStack
  | Cons of (rule * annotSet * rStack)
  and proc =
  | Zero of annotSet
  | Var of (ident * annotSet)
  | Rcv of (name * ident * proc * annotSet)
  | Prl of (proc * proc * annotSet)
  | New of (name * proc * annotSet)
  | Emit of (name * proc * annotSet)
  and ectx =
  | EPrlr of (proc * ectx)
  | EPrll of (ectx * proc)
  | ENew of (name * ectx)
  | EHole
  and amcfg =
  | Mzs of proc
  | MPrlmB of (rStack * proc * ectx)
  | MPrlm of (proc * rStack * ectx)
  | MOutB of (rStack * proc * ectx * ectx * side * proc * ectx)
  | MOut of (proc * rStack * ectx * ectx * side * proc * ectx)
  | MInpB of (rStack * proc * ectx * side * proc * name * ectx * ectx * ectx)
  | MInp of (proc * rStack * ectx * side * proc * name * ectx * ectx * ectx)
  | MDone of proc
  and annot =
    | APrlm
    | AOut of (proc * ectx)
    | AInp of name
  and annotSet = annot list
                       
           
  let rec freevars = function
    | Zero _ -> []
    | Var (x, _) -> [x]
    | Rcv (_, x, p, _) -> List.filter (fun v -> v <> x) (freevars p)
    | Prl (p, q, _) -> (freevars p) @ (freevars q)
    | New (_, p, _) -> (freevars p)
    | Emit(_, p, _) -> freevars p
                     
  let rec freenames = function
    | Zero _ -> []
    | Var (x, _) -> []
    | Rcv (a, x, p, _) -> a::(freenames p)
    | Prl (p, q, _) -> (freenames p) @ (freenames q)
    | New (a, p, _) -> List.filter (fun x -> a <> x) (freenames p)
    | Emit(a, p, _) -> a::freenames p

  let freshvar fv b =
    let rec f name = if List.mem name fv then f (name ^ "'") else name in
    f (b ^ "'")
    
  let freshname fn b =
    let rec f name = if List.mem name fn then f (name ^ "'") else name in
    f (b ^ "'")

  let rec rename b f t = match t with
    | Rcv (n, x, p, a) -> Rcv (n, x, rename b f p, a)
    | Emit (n, p, a) -> Emit (n, rename b f p, a)
    | Prl (p, q, a) -> Prl (rename b f p, rename b f q, a)
    | New (n, p, a) ->
       if n = b then t
       else if n = f then
         let fresh = freshname (b::(freenames p)) n in
         New (fresh, rename b f (rename n fresh p), a)
       else
         New (n, rename n f p, a)
    | _ -> t
         
  let rec subst_aux b t1 t2 =
    match t1 with
    | Zero _ -> t1
    | Var (x, a) -> if x = b then t2 else t1
    | Rcv (n, c, p, a) -> 
       if c = b then t1 else
         let fv2 = freevars t2 in
         if List.mem c fv2 then
           let fv1 = freevars t1 in
           let fresh = freshvar (b :: (fv1 @ fv2)) c in
           Rcv (n, fresh, subst_aux b (subst_aux c p (Var (fresh, []))) t2, a)
         else Rcv (n, c, subst_aux b p t2, a)
    | New (n, p, a) ->
       let fn2 = freenames t2 in
       if List.mem n fn2 then
         let fn1 = freenames t1 in
         let fresh = freshname (fn1 @ fn2) n in
         New (fresh, subst_aux b (rename n fresh t1) t2, a)
       else New (n, subst_aux b p t2, a)
    | Prl (p, q, a) -> Prl (subst_aux b p t2, subst_aux b q t2, a)
    | Emit(n, p, a) -> Emit(n, subst_aux b p t2, a)
                     
  let subst p x q = subst_aux x p q

  let annotSet_of_term = function
    | Zero a -> a
    | Var (x, a) -> a
    | Rcv (_, _, _, a) -> a
    | Prl (_, _, a) -> a
    | Emit(_, _, a) -> a
    | New (_,_, a) -> a

  let rec nb = function
    | EHole -> []
    | EPrlr (_, e) -> nb e
    | EPrll (e, _) -> nb e
    | ENew (a, e) -> a :: nb e

  let assert_necro b =
    if not b then raise (Branch_fail "Necro Assertion Error")
                   
  let notinnb a e = assert_necro (not(List.mem a (nb e)))
                  
  let innb a e = assert_necro (List.mem a (nb e))
                      
  let eqname a b = assert_necro (a = b)

  let noteqname a b = assert_necro (a <> b)
                 
  let notinannotSet a t = assert_necro(not (List.mem a (annotSet_of_term t)))
      
  let inannotSet a t = assert_necro(List.mem a (annotSet_of_term t))

  let addannotSet a l = a::l
                      
  let empty_annotSet = []
                      
  let rec erase = function
    | Zero _ -> Zero []
    | Var (x, a) -> Var (x, [])
    | Rcv (n, x, p, _) -> Rcv (n, x, erase p, [])
    | Prl (p, q, _) -> Prl (erase p, erase q, [])
    | Emit(n, p, _) -> Emit(n, erase p, [])
    | New (n, p, _) -> New (n, erase p, [])

  let inp_to_annot _ _ p a e f _ = AInp a
  let out_to_annot _ f2 _ r e = AOut (erase r, f2)
  let prlm_to_annot _ = APrlm

                    
end

module InterpLambda : (INTERPRETER with type ident = string
                                    and type proc = Flow.proc
                                    and type ectx = Flow.ectx
                                    and type annot = Flow.annot
                                    and type annotSet = Flow.annotSet
                                    and type amcfg = Flow.amcfg) = MakeInterpreter (Flow)

open Flow
open InterpLambda


   
let rec string_of_term = function
  | Zero a -> "0"
  | Var (x, a) -> x
  | Rcv (n, x, p, a) -> n^"("^x^").("^(string_of_term p)^")"
  | Prl (p, q, a) -> "("^(string_of_term p)^" | "^(string_of_term q)^")"
  | Emit(n, p, a) -> n^"<"^(string_of_term p)^">"
  | New (n, p, a) -> "nu "^n^"."^(string_of_term p)



let string_of_side = function
  | Left -> "L"
  | Right -> "R"
          
let rec string_of_ectx = function
  | EHole -> "[]"
  | EPrlr (p, e) ->
     Printf.sprintf "%s | %s"
       (string_of_term p) (string_of_ectx e)
  | EPrll (e, p) ->
     Printf.sprintf "%s | %s"
       (string_of_ectx e) (string_of_term p)
  | ENew (n, e) -> "nu "^n^"."^(string_of_ectx e)

let string_of_annot = function
  | AOut (p, e) -> "#o("^(string_of_term p)^","^(string_of_ectx e)^")"
  | APrlm -> "#p"
  | AInp a-> "#i("^a^")"

let rec string_of_annotSet = function
  | [] -> ""
  | t::q -> (string_of_annot t)^","^(string_of_annotSet q)


let rec string_of_mcfg = function
  | MPrlm (p, _, e) -> 
     Printf.sprintf "< %s * %s >_p" (string_of_term p) (string_of_ectx e)
  | MPrlmB (_, p, e) ->
     Printf.sprintf "< %s * %s >_bp" (string_of_ectx e) (string_of_term p)
  | MOut (p, _, f1, f2, s, q, e) -> 
     Printf.sprintf "< %s *  %s %s, %s, %s, %s >_o"
       (string_of_term p) (string_of_ectx f1) (string_of_ectx f2)
       (string_of_side s) (string_of_term q) (string_of_ectx e) 
  | MOutB (_, p, f1, f2, s, q, e) -> 
     Printf.sprintf "< %s *  %s, %s, %s, %s, %s >_bo"
       (string_of_term p) (string_of_ectx f1) (string_of_ectx f2)
       (string_of_side s) (string_of_term q) (string_of_ectx e)
  | MInp (p, _, g, s, r, n, e, f1, f2) ->
     Printf.sprintf "< %s *  %s, %s, %s, %s, %s, %s, %s >_i"
       (string_of_term p) (string_of_ectx g) (string_of_side s)
       (string_of_term r) n (string_of_ectx e) (string_of_ectx f1) (string_of_ectx f2)
  | MInpB (_, p, g, s, r, n, e, f1, f2) ->
     Printf.sprintf "< %s *  %s, %s, %s, %s, %s, %s, %s >_bi"
       (string_of_term p) (string_of_ectx g)  (string_of_side s)
       (string_of_term r) n (string_of_ectx e) (string_of_ectx f1) (string_of_ectx f2)
  | MDone (t) -> "Done :"^(string_of_term t) 
  | Mzs (t) -> "Start :"^(string_of_term t) 

                   

let i name ident proc = Rcv (name, ident, proc, [])
let o name proc = Emit (name, proc, [])
let v ident = Var (ident, [])
let ( $ ) p q = Prl (p, q, [])
let nu name proc = New (name, proc, [])
let z = Zero []

let id a = i a "x" (v "x")

let rec multi_step cfg =
  Printf.printf "%s \n" (string_of_mcfg cfg); 
  match cfg with
  | MDone(_) -> ()
  | _ -> multi_step (step cfg)

let p = v "x" $ z
let pstuck = i "a" "x" (id "a" $ o "a" z)
let p2 = id "b" $ (id "a" $ o "a" z)
let p3 = (i "a" "x" z) $ (id "a" $ o "a" z)
let p4 = (i "b" "x" z) $ (id "a" $ o "a" z) $ o "b" z
let p5 = (i "b" "x" z) $ i "a" "x" (id "b") $ (o "a" z $ o "b" (id "c"))
let p6 = (id "b" $ id "a") $ o "b" z
let p7 = (id "a" $ o "b" z) $ i "b" "y" (o "a" z)
       
let p8 = (o "a" z) $ nu "a" (o "a" z $ id "a")
let p9 = (id "a") $ nu "a" (o "a" z $ id "a")
let p10 = (id "c") $ nu "a" (nu "b" (o "c" z $ id "a"))
let p11 = (i "b" "x" z) $ i "a" "x" (id "b") $ (nu "c" (o "a" z $ o "b" (id "c")))

let () =
  let cfg = Flow.Mzs (p11) in
  multi_step cfg
