(* Test file for the lambda abstract machine *)

open LambdaAM

module Flow = struct


  type ident = string

  type annot =
    | ALam
    | AApp

  type annotSet = annot list

  type rule =
  | LammLam
  | AppmLam
  | AppmApp_1
  | AppmApp_0
  | AppmApp
  and rStack =
  | EmpStack
  | Cons of (rule * annotSet * rStack)
  and lterm =
  | Var of (ident * annotSet)
  | Lam of (ident * lterm * annotSet)
  | App of (lterm * lterm * annotSet)
  and ectx =
  | ELam of (ident * ectx)
  | EHole
  | EAppr of (lterm * ectx)
  | EAppl of (ectx * lterm)
  and amcfg =
  | Mzs of lterm
  | MLammB of (rStack * lterm * lterm * ectx)
  | MLamm of (lterm * rStack * lterm * ectx)
  | MDone of lterm
  | MAppmB of (rStack * lterm * ectx)
  | MAppm of (lterm * rStack * ectx)
           
  let rec freenames = function
    | Var (x, a) -> [x]
    | Lam (b, t, a) -> List.filter (fun n -> n <> b) (freenames t)
    | App (t1, t2, a) -> (freenames t1) @ (freenames t2)
                  
  let freshname fn b =
    let rec f name = if List.mem name fn then f (name ^ "'") else name in
    f (b ^ "'")

  let rec subst_aux b t1 t2 =
    match t1 with
    | Var (x, a) -> if x = b then t2 else t1
    | Lam (c, t, a) ->
       if c = b then t1 else
         let fn2 = freenames t2 in
         if List.mem c fn2 then
           let fn1 = freenames t1 in
           let fresh = freshname (b :: (fn1 @ fn2)) c in
           Lam (fresh, subst_aux b (subst_aux c t (Var (fresh, []))) t2, a)
         else Lam (c, subst_aux b t t2, a)
    | App (t1', t2', a) -> App (subst_aux b t1' t2, subst_aux b t2' t2, a)
         
  let subst t y s = subst_aux y t s

  let annotSet_of_term = function
    | Var (_, a) -> a
    | Lam (_, _, a) -> a
    | App (_, _, a) -> a

  let assert_necro b =
    if not b then raise (Branch_fail "Necro Assertion Error")
                      
  let notinannotSet a t = assert_necro(not (List.mem a (annotSet_of_term t)))
      
  let inannotSet a t = assert_necro(List.mem a (annotSet_of_term t))

  let addannotSet a l = a::l
                         
  let empty_annotSet = []

  let rec erase = function
    | Var (x, _) -> Var(x, [])
    | Lam (c, t, _) -> Lam (c, erase t, [])
    | App (t1, t2, _) -> App(erase t1, erase t2, [])
                     
  let appm_to_annot _ = AApp
  let lamm_to_annot _ _ = ALam
                           
                       
end

module InterpLambda : (INTERPRETER with type ident = string
                                    and type lterm = Flow.lterm
                                    and type ectx = Flow.ectx
                                    and type annot = Flow.annot
                                    and type annotSet = Flow.annotSet
                                    and type amcfg = Flow.amcfg) = MakeInterpreter (Flow)

open Flow
open InterpLambda

let string_of_annot = function
  | AApp -> "@"
  | ALam -> "λ"

let rec string_of_annotSet = function
  | [] -> ""
  | t::q -> (string_of_annot t)^","^(string_of_annotSet q)

   
let rec string_of_lterm = function
  | Lam (id, lt, _) -> Printf.sprintf "λ%s.%s" id (string_of_lterm lt)
  | Var (id , a)-> id (*^":"^(string_of_annotSet a)*)
  | App (Var (id1, _), Var (id2, _), _) -> id1 ^ " " ^ id2
  | App (Var (id1, _), lt2, a) ->
     Printf.sprintf "%s (%s)"
       id1
       (*(string_of_annotSet a)*)
       (string_of_lterm lt2)
  | App (lt1, Var (id2, _), a) -> 
     Printf.sprintf "%s (%s)"
       (string_of_lterm lt1)
       (*       (string_of_annotSet a) *)
       id2
  | App (lt1, lt2, _) ->
    Printf.sprintf "(%s) (%s)" (string_of_lterm lt1) (string_of_lterm lt2)

    
let rec string_of_ectx = function
  | EHole -> "[]"
  | ELam (x, e) -> Printf.sprintf "λ%s.%s" x (string_of_ectx e)
  | EAppl (e, t) -> 
     Printf.sprintf "(%s) (%s) " (string_of_ectx e)
       (string_of_lterm t)
  | EAppr (t, e) -> 
     Printf.sprintf "(%s) (%s)" (string_of_lterm t) 
       (string_of_ectx e)

let rec string_of_mcfg = function
  | MAppm (t, _, e) ->
     Printf.sprintf "< %s, %s >_@" (string_of_lterm t) (string_of_ectx e)
  | MAppmB (_, t, e) ->
     Printf.sprintf "< %s, %s >_b@" (string_of_ectx e) (string_of_lterm t) 
  | MLamm (t, _, s, e) ->
     Printf.sprintf "< %s * %s, %s >_l"
       (string_of_lterm t) (string_of_lterm s) (string_of_ectx e)
  | MLammB (_, t, s, e) ->
     Printf.sprintf "< %s * %s, %s >_bl"
       (string_of_lterm t) (string_of_lterm s) (string_of_ectx e) 
  | MDone (t) -> "Done :"^(string_of_lterm t)
  | Mzs (t) -> "Start :"^(string_of_lterm t)



    
let l ident lterm : lterm = Lam (ident, lterm, [])
let v ident = Var (ident, [])
let ( $ ) lt1 lt2 = App (lt1, lt2, [])

let pow f lt n =
  let rec w acc i = if i = n then f $ acc else w (f $ acc) (i+1) in
  w lt 1

let mkint i = l "s" @@ l "z" @@ (pow (v "s") (v "z") i)

let id = l "x" @@
  v "x"


let rec multi_step cfg =
  Printf.printf "%s \n" (string_of_mcfg cfg); 
  match cfg with
  | MDone(_) -> ()
  | _ -> multi_step (step cfg)

let lt = v "x" $ v "y"
let lt2 = v "x" $ (id $ v "y")
let lt3 = (id $ v "y") $ v "x" 
let lt4 = l "z" @@ (v "x" $ (id $ v "y"))
let lt5 = l "z" @@ (v "x" $ (id $ v "y") $ ((id $ v "y") $ v "x" ))

let () =
  let cfg = Flow.Mzs (lt5) in
  multi_step cfg
