SHELL=bash
red=`tput setaf 1``tput setab 7`
reset=`tput sgr0`

SOURCES = $(addprefix src/,$(shell ls src))

.PHONY: all main clean

all: necro

%.native: $(SOURCES)
	rm -f $@
	ocamlbuild -I src -pkg str,easy-format $@

necro: main.native
	ln -fs $^ $@
	chmod +x $@


clean:
	rm -rf _build || true
	rm -f *.native || true
	rm -rf ndam/_build || true
	rm -f ndam/*.native || true
	rm necro || true
