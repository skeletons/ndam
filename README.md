Install the following dependencies using opam https://opam.ocaml.org/

* OCaml (4.09 or greater)
* ocamlfind
* ocamlbuild
* easy-format
* menhir

After `make`, do

`./necro ndam ndam/lambdaZS.sk -o ndam/lambdaAM.sk`

to transform the zipper semantics into the NDAM. Then

`./necro rand ndam/lambdaAM.sk -o ndam/lambdaAM.ml`

to generate the OCaml interpreter for the NDAM, which must be instantiated, see,
e.g., `test_lambdaAM.ml` in the `ndam` folder. To generate the binary, type

`ocamlbuild test_lambdaAM.ml test_lambdaAM.native`

in the `ndam` folder.

Compared to companion paper, we do not generate the `erase` function and
the annotation for a given mode automatically. Look in
`ndam/test_lambdaAM.ml` for an example of implementation of the `erase`
function and the function computing the annotations (`appm_to_annot` and `lamm_to_annot`)



